-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mku_system.tbl_equipment
DROP TABLE IF EXISTS `tbl_equipment`;
CREATE TABLE IF NOT EXISTS `tbl_equipment` (
  `equipment_id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_name` varchar(100) NOT NULL DEFAULT '0',
  `equipment_rate_day` varchar(30) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`equipment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_equipment: ~8 rows (approximately)
/*!40000 ALTER TABLE `tbl_equipment` DISABLE KEYS */;
REPLACE INTO `tbl_equipment` (`equipment_id`, `equipment_name`, `equipment_rate_day`, `status`) VALUES
	(1, 'Road grader', '789.65', 1),
	(2, 'Road Roller', '789.65', 1),
	(3, 'Water tank', '659.76', 1),
	(4, 'Backhoe', '367.9', 1),
	(5, 'Payloader', '439.87', 1),
	(6, 'Transit mixer', '985.56', 1),
	(7, 'Concrete vibrator', '367.98', 1),
	(8, 'Concrete cutter', '327.78', 1);
/*!40000 ALTER TABLE `tbl_equipment` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_estimated_task
DROP TABLE IF EXISTS `tbl_estimated_task`;
CREATE TABLE IF NOT EXISTS `tbl_estimated_task` (
  `et_id` int(11) NOT NULL AUTO_INCREMENT,
  `et_name` varchar(50) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`et_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_estimated_task: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_estimated_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_estimated_task` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_generated_fixed_cost
DROP TABLE IF EXISTS `tbl_generated_fixed_cost`;
CREATE TABLE IF NOT EXISTS `tbl_generated_fixed_cost` (
  `gc_id` int(11) NOT NULL AUTO_INCREMENT,
  `gc_quantity` int(11) NOT NULL DEFAULT '0',
  `gc_total` double(10,2) NOT NULL DEFAULT '0.00',
  `material_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_generated_fixed_cost: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_generated_fixed_cost` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_generated_fixed_cost` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_manpower
DROP TABLE IF EXISTS `tbl_manpower`;
CREATE TABLE IF NOT EXISTS `tbl_manpower` (
  `manpower_id` int(11) NOT NULL AUTO_INCREMENT,
  `manpower_name` varchar(100) NOT NULL DEFAULT '0',
  `manpower_rate_day` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manpower_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_manpower: ~5 rows (approximately)
/*!40000 ALTER TABLE `tbl_manpower` DISABLE KEYS */;
REPLACE INTO `tbl_manpower` (`manpower_id`, `manpower_name`, `manpower_rate_day`, `status`) VALUES
	(1, 'Foreman', 450.6, 1),
	(2, 'Operator', 370.76, 1),
	(3, 'Driver', 370.56, 1),
	(4, 'Helper', 298.5, 1),
	(5, 'Mason', 290.76, 1);
/*!40000 ALTER TABLE `tbl_manpower` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_material
DROP TABLE IF EXISTS `tbl_material`;
CREATE TABLE IF NOT EXISTS `tbl_material` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(100) NOT NULL DEFAULT '0',
  `material_unit` varchar(30) NOT NULL DEFAULT '0',
  `material_cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_material: ~14 rows (approximately)
/*!40000 ALTER TABLE `tbl_material` DISABLE KEYS */;
REPLACE INTO `tbl_material` (`material_id`, `material_name`, `material_unit`, `material_cost`, `status`) VALUES
	(1, 'Cement', 'bag', 235.00, 1),
	(2, 'Gravel', '1 cubic meter', 760.00, 1),
	(3, 'Sand', '1 cubic meter', 550.00, 1),
	(4, 'Deformed Steel Bar', '6.0 meters (1 piece)', 34.00, 1),
	(5, 'Plain Bar', '6.0 meter (1 piece)', 60.00, 1),
	(6, 'Fuel', '1 Liter', 46.89, 1),
	(7, 'Curing compound', '1 Liter/15 m2', 58.78, 1),
	(8, 'Asphalt sealant', '1 Liter/80m', 67.56, 1),
	(9, 'Diesel', '1 Liter', 35.76, 1),
	(10, 'Gasoline', '1 Liter', 46.78, 1),
	(11, 'Item104', '1 cubic meter', 37.98, 1),
	(12, 'Item105', '1 m2', 39.78, 1),
	(13, 'Item200', '1 cubic meter', 41.87, 1),
	(14, 'Item201', '1 cubic meter', 29.78, 1);
/*!40000 ALTER TABLE `tbl_material` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project
DROP TABLE IF EXISTS `tbl_project`;
CREATE TABLE IF NOT EXISTS `tbl_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL,
  `project_location` varchar(100) NOT NULL,
  `project_start_date` date NOT NULL DEFAULT '0000-00-00',
  `project_end_date` date NOT NULL DEFAULT '0000-00-00',
  `project_dim_length` double NOT NULL,
  `project_dim_height` double NOT NULL,
  `project_dim_width` double NOT NULL,
  `project_dim_lanes` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_project` DISABLE KEYS */;
REPLACE INTO `tbl_project` (`project_id`, `project_name`, `project_location`, `project_start_date`, `project_end_date`, `project_dim_length`, `project_dim_height`, `project_dim_width`, `project_dim_lanes`, `status`) VALUES
	(1, 'Project01', 'Sta. Anna', '2016-10-31', '2016-12-31', 1500, 0.5, 6.9, 2, 0);
/*!40000 ALTER TABLE `tbl_project` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project_client
DROP TABLE IF EXISTS `tbl_project_client`;
CREATE TABLE IF NOT EXISTS `tbl_project_client` (
  `pc_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pc_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project_client: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_project_client` DISABLE KEYS */;
REPLACE INTO `tbl_project_client` (`pc_id`, `user_id`, `project_id`, `status`) VALUES
	(1, 10, 1, 0);
/*!40000 ALTER TABLE `tbl_project_client` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project_engineer
DROP TABLE IF EXISTS `tbl_project_engineer`;
CREATE TABLE IF NOT EXISTS `tbl_project_engineer` (
  `pe_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`pe_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project_engineer: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_project_engineer` DISABLE KEYS */;
REPLACE INTO `tbl_project_engineer` (`pe_id`, `user_id`, `project_id`, `status`) VALUES
	(1, 6, 1, 0);
/*!40000 ALTER TABLE `tbl_project_engineer` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task
DROP TABLE IF EXISTS `tbl_task`;
CREATE TABLE IF NOT EXISTS `tbl_task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_description` varchar(100) NOT NULL DEFAULT '0',
  `task_duration` int(11) NOT NULL DEFAULT '0',
  `task_start_date` date NOT NULL DEFAULT '0000-00-00',
  `task_end_date` date NOT NULL DEFAULT '0000-00-00',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_task` DISABLE KEYS */;
REPLACE INTO `tbl_task` (`task_id`, `task_description`, `task_duration`, `task_start_date`, `task_end_date`, `project_id`, `status`) VALUES
	(1, 'Embankment', 0, '0000-00-00', '0000-00-00', 1, 1),
	(2, 'Sub-grade', 0, '0000-00-00', '0000-00-00', 1, 1),
	(3, 'Sub-base', 0, '0000-00-00', '0000-00-00', 1, 1),
	(4, 'Base course', 0, '0000-00-00', '0000-00-00', 1, 1),
	(5, 'PCCP', 0, '0000-00-00', '0000-00-00', 1, 1),
	(6, 'Embarkment', 0, '2016-11-20', '2016-11-30', 1, 0);
/*!40000 ALTER TABLE `tbl_task` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_equipment
DROP TABLE IF EXISTS `tbl_task_equipment`;
CREATE TABLE IF NOT EXISTS `tbl_task_equipment` (
  `te_id` int(11) NOT NULL AUTO_INCREMENT,
  `te_quantity` int(11) NOT NULL DEFAULT '0',
  `te_days` int(11) NOT NULL DEFAULT '0',
  `te_total` double(10,2) NOT NULL DEFAULT '0.00',
  `equipment_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`te_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_equipment: ~20 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_equipment` DISABLE KEYS */;
REPLACE INTO `tbl_task_equipment` (`te_id`, `te_quantity`, `te_days`, `te_total`, `equipment_id`, `task_id`, `status`) VALUES
	(1, 1, 15, 11844.75, 1, 1, 1),
	(2, 1, 15, 11844.75, 2, 1, 1),
	(3, 1, 4, 2639.04, 3, 1, 1),
	(4, 1, 5, 3948.25, 1, 2, 1),
	(5, 1, 5, 3948.25, 2, 2, 1),
	(6, 1, 2, 1319.52, 3, 2, 1),
	(7, 1, 19, 15003.35, 1, 3, 1),
	(8, 1, 19, 15003.35, 2, 3, 1),
	(9, 1, 5, 3298.80, 3, 3, 1),
	(10, 1, 15, 11844.75, 1, 4, 1),
	(11, 1, 15, 11844.75, 2, 4, 1),
	(12, 1, 4, 2639.04, 3, 4, 1),
	(13, 1, 9, 7106.85, 1, 6, 0),
	(14, 1, 9, 7106.85, 2, 6, 0),
	(15, 1, 9, 5937.84, 3, 6, 0),
	(16, 2, 7, 5150.60, 4, 6, 0),
	(17, 0, 0, 0.00, 5, 6, 0),
	(18, 0, 0, 0.00, 6, 6, 0),
	(19, 0, 0, 0.00, 7, 6, 0),
	(20, 0, 0, 0.00, 8, 6, 0);
/*!40000 ALTER TABLE `tbl_task_equipment` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_manpower
DROP TABLE IF EXISTS `tbl_task_manpower`;
CREATE TABLE IF NOT EXISTS `tbl_task_manpower` (
  `tmp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tmp_quantity` int(11) NOT NULL DEFAULT '0',
  `tmp_days` int(11) NOT NULL DEFAULT '0',
  `tmp_total` double(10,2) NOT NULL DEFAULT '0.00',
  `manpower_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tmp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=131 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_manpower: ~21 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_manpower` DISABLE KEYS */;
REPLACE INTO `tbl_task_manpower` (`tmp_id`, `tmp_quantity`, `tmp_days`, `tmp_total`, `manpower_id`, `task_id`, `status`) VALUES
	(110, 1, 15, 6759.00, 1, 1, 1),
	(111, 2, 15, 11122.80, 2, 1, 1),
	(112, 1, 4, 1482.24, 3, 1, 1),
	(113, 4, 15, 17910.00, 4, 1, 1),
	(114, 1, 5, 2253.00, 1, 2, 1),
	(115, 2, 5, 3707.60, 2, 2, 1),
	(116, 1, 2, 741.12, 3, 2, 1),
	(117, 4, 5, 5970.00, 4, 2, 1),
	(118, 1, 19, 8561.40, 1, 3, 1),
	(119, 2, 19, 14088.88, 2, 3, 1),
	(120, 1, 5, 1852.80, 3, 3, 1),
	(121, 4, 19, 22686.00, 4, 3, 1),
	(122, 1, 15, 6759.00, 1, 4, 1),
	(123, 2, 15, 11122.80, 2, 4, 1),
	(124, 1, 4, 1482.24, 3, 4, 1),
	(125, 4, 15, 17910.00, 4, 4, 1),
	(126, 1, 9, 4055.40, 1, 6, 0),
	(127, 2, 9, 6673.68, 2, 6, 0),
	(128, 1, 9, 3335.04, 3, 6, 0),
	(129, 15, 9, 40297.50, 4, 6, 0),
	(130, 0, 0, 0.00, 5, 6, 0);
/*!40000 ALTER TABLE `tbl_task_manpower` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_material
DROP TABLE IF EXISTS `tbl_task_material`;
CREATE TABLE IF NOT EXISTS `tbl_task_material` (
  `tm_id` int(11) NOT NULL AUTO_INCREMENT,
  `tm_quantity` int(11) NOT NULL DEFAULT '0',
  `tm_total` double(10,2) NOT NULL DEFAULT '0.00',
  `material_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_material: ~22 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_material` DISABLE KEYS */;
REPLACE INTO `tbl_task_material` (`tm_id`, `tm_quantity`, `tm_total`, `material_id`, `task_id`, `status`) VALUES
	(1, 5951, 226028.48, 11, 1, 1),
	(2, 368, 17255.52, 6, 1, 1),
	(3, 10350, 411723.00, 12, 2, 1),
	(4, 127, 5955.03, 6, 2, 1),
	(5, 5951, 249178.84, 13, 3, 1),
	(6, 466, 21850.74, 6, 3, 1),
	(7, 5951, 177228.23, 14, 4, 1),
	(8, 368, 17255.52, 6, 4, 1),
	(9, 0, 0.00, 1, 6, 0),
	(10, 0, 0.00, 2, 6, 0),
	(11, 0, 0.00, 3, 6, 0),
	(12, 0, 0.00, 4, 6, 0),
	(13, 0, 0.00, 5, 6, 0),
	(14, 168, 7877.52, 6, 6, 0),
	(15, 0, 0.00, 7, 6, 0),
	(16, 0, 0.00, 8, 6, 0),
	(17, 0, 0.00, 9, 6, 0),
	(18, 0, 0.00, 10, 6, 0),
	(19, 0, 0.00, 11, 6, 0),
	(20, 0, 0.00, 12, 6, 0),
	(21, 0, 0.00, 13, 6, 0),
	(22, 0, 0.00, 14, 6, 0);
/*!40000 ALTER TABLE `tbl_task_material` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_user
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(50) NOT NULL DEFAULT '0',
  `user_lastname` varchar(50) NOT NULL DEFAULT '0',
  `user_username` varchar(50) NOT NULL DEFAULT '0',
  `user_password_text` varchar(50) NOT NULL DEFAULT '0',
  `user_password_hash` varchar(50) NOT NULL DEFAULT '0',
  `user_age` int(11) NOT NULL DEFAULT '0',
  `user_gender` varchar(50) NOT NULL DEFAULT '0',
  `user_rate` double NOT NULL DEFAULT '0',
  `user_contact_number` varchar(50) NOT NULL DEFAULT '0',
  `user_address` varchar(100) NOT NULL DEFAULT '0',
  `user_type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_user: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
REPLACE INTO `tbl_user` (`user_id`, `user_firstname`, `user_lastname`, `user_username`, `user_password_text`, `user_password_hash`, `user_age`, `user_gender`, `user_rate`, `user_contact_number`, `user_address`, `user_type`, `status`) VALUES
	(1, 'admin', 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, 'Female', 0, '0', '0', 1, 0),
	(6, 'engr', 'engr', 'engr', 'engr', 'b1a506e2c9c65fbec86acf6a9e8145f4', 22, 'Female', 0, '09745273856', 'Amazon Sreet, Sta Anna', 2, 0),
	(8, 'Claire', 'Simons', 'clairesimons', 'FinKsN3P', '9b149e29787d7f2a8cea304e843e3b6a', 22, 'Female', 739.6, '09322756483', 'Sander Street, Kentucky', 2, 0),
	(9, 'Alvin', 'Stuart', 'alvinStuart', 'goJaDjW1', 'c5f868ecb6f7bc7cf67c2a325183579a', 45, 'Male', 830.68, '094276946374', 'Rancho Street, North Carolina', 2, 0),
	(10, 'Client', 'Client', 'client', 'client', '62608e08adc29a8d6dbc9754e659f125', 23, 'Female', 0, '0974627482', 'Brgy. Tangub, Bacolod City', 3, 0),
	(11, 'Aldrin', 'Cruz', 'aldrinCruz', 'cCO5DL3E', '04dfcaa1db889214d190626a66bdebb2', 34, 'Male', 0, '0978674536', 'Brgy. Estifania Bacolod City', 3, 0);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
