-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table mku_system.tbl_equipment
CREATE TABLE IF NOT EXISTS `tbl_equipment` (
  `equipment_id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_name` varchar(100) NOT NULL DEFAULT '0',
  `equipment_unit` varchar(30) NOT NULL DEFAULT '0',
  `equipment_cost` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`equipment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_equipment: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_equipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_equipment` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_manpower
CREATE TABLE IF NOT EXISTS `tbl_manpower` (
  `manpower_id` int(11) NOT NULL AUTO_INCREMENT,
  `manpower_name` varchar(100) NOT NULL DEFAULT '0',
  `manpower_rate_per_day` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manpower_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_manpower: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_manpower` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_manpower` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_material
CREATE TABLE IF NOT EXISTS `tbl_material` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(100) NOT NULL DEFAULT '0',
  `material_unit` varchar(30) NOT NULL DEFAULT '0',
  `material_cost` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_material: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_material` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_material` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project
CREATE TABLE IF NOT EXISTS `tbl_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL DEFAULT '0',
  `project_location` varchar(100) NOT NULL DEFAULT '0',
  `project_start_date` date NOT NULL DEFAULT '0000-00-00',
  `project_end_date` date NOT NULL DEFAULT '0000-00-00',
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_project` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_project` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project_client
CREATE TABLE IF NOT EXISTS `tbl_project_client` (
  `pc_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project_client: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_project_client` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_project_client` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project_engineer
CREATE TABLE IF NOT EXISTS `tbl_project_engineer` (
  `pe_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pe_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project_engineer: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_project_engineer` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_project_engineer` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task
CREATE TABLE IF NOT EXISTS `tbl_task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_description` varchar(100) NOT NULL DEFAULT '0',
  `tast_duration` int(11) NOT NULL DEFAULT '0',
  `task_start_date` date NOT NULL DEFAULT '0000-00-00',
  `task_end_date` date NOT NULL DEFAULT '0000-00-00',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_task` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_manpower
CREATE TABLE IF NOT EXISTS `tbl_task_manpower` (
  `tmp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tmp_days` int(11) NOT NULL DEFAULT '0',
  `tmp_total` double NOT NULL DEFAULT '0',
  `manpower_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tmp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_manpower: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_manpower` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_task_manpower` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_material
CREATE TABLE IF NOT EXISTS `tbl_task_material` (
  `tm_id` int(11) NOT NULL AUTO_INCREMENT,
  `tm_quantity` int(11) NOT NULL DEFAULT '0',
  `tm_total` double NOT NULL DEFAULT '0',
  `material_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_material: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_material` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_task_material` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_tasl_equipment
CREATE TABLE IF NOT EXISTS `tbl_tasl_equipment` (
  `te_id` int(11) NOT NULL AUTO_INCREMENT,
  `te_day` int(11) NOT NULL DEFAULT '0',
  `te_total` double NOT NULL DEFAULT '0',
  `equipment_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`te_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_tasl_equipment: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_tasl_equipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tasl_equipment` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(50) NOT NULL DEFAULT '0',
  `user_lastname` varchar(50) NOT NULL DEFAULT '0',
  `user_username` varchar(50) NOT NULL DEFAULT '0',
  `user_password_text` varchar(50) NOT NULL DEFAULT '0',
  `user_password_hash` varchar(50) NOT NULL DEFAULT '0',
  `user_age` int(11) NOT NULL DEFAULT '0',
  `user_gender` varchar(50) NOT NULL DEFAULT '0',
  `user_rate` double NOT NULL DEFAULT '0',
  `user_contact_number` varchar(50) NOT NULL DEFAULT '0',
  `user_address` varchar(100) NOT NULL DEFAULT '0',
  `user_type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_user: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`user_id`, `user_firstname`, `user_lastname`, `user_username`, `user_password_text`, `user_password_hash`, `user_age`, `user_gender`, `user_rate`, `user_contact_number`, `user_address`, `user_type`, `status`) VALUES
	(1, 'admin', 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, '0', 0, '0', '0', 1, 0),
	(6, 'engr', 'engr', 'engr', 'engr', 'b1a506e2c9c65fbec86acf6a9e8145f4', 22, 'Female', 0, '09745273856', 'Amazon Sreet, Sta Anna', 2, 0),
	(8, 'Claire', 'Simons', 'clairesimons', 'FinKsN3P', '9b149e29787d7f2a8cea304e843e3b6a', 22, 'Female', 739.6, '09322756483', 'Sander Street, Kentucky', 2, 0),
	(9, 'Alvin', 'Stuart', 'alvinStuart', 'goJaDjW1', 'c5f868ecb6f7bc7cf67c2a325183579a', 45, 'Male', 830.68, '094276946374', 'Rancho Street, North Carolina', 2, 0);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
