-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for procedure mku_system.GetProjectById
DROP PROCEDURE IF EXISTS `GetProjectById`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetProjectById`(IN id INT)
BEGIN
SELECT p.project_id, p.project_name, p.project_location,
						p.project_start_date, p.project_end_date,
						p.project_dim_length, p.project_dim_height,
						p.project_dim_width, p.project_dim_lanes,
						pe.user_id as project_engineer, 
						pc.user_id as project_client,
						CASE 
							WHEN p.status = 0	THEN 'Approved'
							WHEN p.status = 1 THEN 'Completed'
							WHEN p.status = 2 THEN 'On going'
						END as status
				FROM tbl_project as p
				INNER JOIN tbl_project_engineer as pe
				INNER JOIN tbl_project_client as pc
				ON
					p.project_id = pe.project_id
				AND
					p.project_id = pc.project_id
				WHERE
					p.project_id = id;
END//
DELIMITER ;


-- Dumping structure for procedure mku_system.GetProjectByUserId
DROP PROCEDURE IF EXISTS `GetProjectByUserId`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetProjectByUserId`(IN id INT)
BEGIN
SELECT p.project_id, p.project_name, p.project_location, 
				p.project_start_date, p.project_end_date,
				CONCAT(u1.user_firstname,' ',u1.user_lastname) as engineer_name,
				CONCAT(u2.user_firstname,' ',u2.user_lastname) as client_name,
				CASE 
					WHEN p.status = 0	THEN 'Approved'
					WHEN p.status = 1 THEN 'Completed'
					WHEN p.status = 2 THEN 'On going'
				END as status
				FROM tbl_project AS p
				INNER JOIN tbl_project_engineer as pe
				INNER JOIN tbl_project_client as pc
						ON
							p.project_id = pe.project_id
						AND
							p.project_id = pc.project_id
				INNER JOIN tbl_user as u1
				INNER JOIN tbl_user as u2
				ON
					pe.user_id = u1.user_id
				WHERE
					pc.user_id = u2.user_id
				AND
					IF((SELECT count(*) FROM tbl_project_engineer WHERE user_id = id), 
						pe.user_id = id, pc.user_id = id);
END//
DELIMITER ;


-- Dumping structure for table mku_system.tbl_equipment
DROP TABLE IF EXISTS `tbl_equipment`;
CREATE TABLE IF NOT EXISTS `tbl_equipment` (
  `equipment_id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_name` varchar(100) NOT NULL DEFAULT '0',
  `equipment_rate_day` varchar(30) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`equipment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_equipment: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_equipment` DISABLE KEYS */;
INSERT INTO `tbl_equipment` (`equipment_id`, `equipment_name`, `equipment_rate_day`, `status`) VALUES
	(3, 'Transit Mixer', '2585.76', 0),
	(4, 'Pay Loader', '534.90', 0);
/*!40000 ALTER TABLE `tbl_equipment` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_manpower
DROP TABLE IF EXISTS `tbl_manpower`;
CREATE TABLE IF NOT EXISTS `tbl_manpower` (
  `manpower_id` int(11) NOT NULL AUTO_INCREMENT,
  `manpower_name` varchar(100) NOT NULL DEFAULT '0',
  `manpower_rate_day` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manpower_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_manpower: ~3 rows (approximately)
/*!40000 ALTER TABLE `tbl_manpower` DISABLE KEYS */;
INSERT INTO `tbl_manpower` (`manpower_id`, `manpower_name`, `manpower_rate_day`, `status`) VALUES
	(2, 'Foreman', 450.45, 0),
	(3, 'Operator', 350, 0);
/*!40000 ALTER TABLE `tbl_manpower` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_material
DROP TABLE IF EXISTS `tbl_material`;
CREATE TABLE IF NOT EXISTS `tbl_material` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(100) NOT NULL DEFAULT '0',
  `material_unit` varchar(30) NOT NULL DEFAULT '0',
  `material_cost` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_material: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_material` DISABLE KEYS */;
INSERT INTO `tbl_material` (`material_id`, `material_name`, `material_unit`, `material_cost`, `status`) VALUES
	(1, 'Cement', 'bag', 478.54, 0),
	(2, 'Asphalt', 'gram/cubic meter', 224.37, 0);
/*!40000 ALTER TABLE `tbl_material` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project
DROP TABLE IF EXISTS `tbl_project`;
CREATE TABLE IF NOT EXISTS `tbl_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL,
  `project_location` varchar(100) NOT NULL,
  `project_start_date` date NOT NULL DEFAULT '0000-00-00',
  `project_end_date` date NOT NULL DEFAULT '0000-00-00',
  `project_dim_length` double NOT NULL,
  `project_dim_height` double NOT NULL,
  `project_dim_width` double NOT NULL,
  `project_dim_lanes` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_project` DISABLE KEYS */;
INSERT INTO `tbl_project` (`project_id`, `project_name`, `project_location`, `project_start_date`, `project_end_date`, `project_dim_length`, `project_dim_height`, `project_dim_width`, `project_dim_lanes`, `status`) VALUES
	(3, 'Carmela Road', 'Guanzon Avenue Ph2', '2016-06-20', '2016-07-13', 810, 0.78, 34.98, 4, 0),
	(7, 'qq', 'qq', '2017-01-01', '2018-12-01', 1, 1, 1, 1, 0),
	(8, 'www', 'www', '2016-12-31', '2016-01-01', 1, 1, 1, 1, 0);
/*!40000 ALTER TABLE `tbl_project` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project_client
DROP TABLE IF EXISTS `tbl_project_client`;
CREATE TABLE IF NOT EXISTS `tbl_project_client` (
  `pc_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pc_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project_client: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_project_client` DISABLE KEYS */;
INSERT INTO `tbl_project_client` (`pc_id`, `user_id`, `project_id`, `status`) VALUES
	(2, 10, 3, 0),
	(6, 11, 7, 0),
	(7, 10, 8, 0);
/*!40000 ALTER TABLE `tbl_project_client` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project_engineer
DROP TABLE IF EXISTS `tbl_project_engineer`;
CREATE TABLE IF NOT EXISTS `tbl_project_engineer` (
  `pe_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`pe_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project_engineer: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_project_engineer` DISABLE KEYS */;
INSERT INTO `tbl_project_engineer` (`pe_id`, `user_id`, `project_id`, `status`) VALUES
	(1, 9, 3, 0),
	(6, 6, 7, 0),
	(7, 9, 8, 0);
/*!40000 ALTER TABLE `tbl_project_engineer` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task
DROP TABLE IF EXISTS `tbl_task`;
CREATE TABLE IF NOT EXISTS `tbl_task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_description` varchar(100) NOT NULL DEFAULT '0',
  `tast_duration` int(11) NOT NULL DEFAULT '0',
  `task_start_date` date NOT NULL DEFAULT '0000-00-00',
  `task_end_date` date NOT NULL DEFAULT '0000-00-00',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_task` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_manpower
DROP TABLE IF EXISTS `tbl_task_manpower`;
CREATE TABLE IF NOT EXISTS `tbl_task_manpower` (
  `tmp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tmp_days` int(11) NOT NULL DEFAULT '0',
  `tmp_total` double NOT NULL DEFAULT '0',
  `manpower_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tmp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_manpower: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_manpower` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_task_manpower` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_material
DROP TABLE IF EXISTS `tbl_task_material`;
CREATE TABLE IF NOT EXISTS `tbl_task_material` (
  `tm_id` int(11) NOT NULL AUTO_INCREMENT,
  `tm_quantity` int(11) NOT NULL DEFAULT '0',
  `tm_total` double NOT NULL DEFAULT '0',
  `material_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tm_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_material: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_material` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_task_material` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_tasl_equipment
DROP TABLE IF EXISTS `tbl_tasl_equipment`;
CREATE TABLE IF NOT EXISTS `tbl_tasl_equipment` (
  `te_id` int(11) NOT NULL AUTO_INCREMENT,
  `te_day` int(11) NOT NULL DEFAULT '0',
  `te_total` double NOT NULL DEFAULT '0',
  `equipment_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`te_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_tasl_equipment: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_tasl_equipment` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_tasl_equipment` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_user
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(50) NOT NULL DEFAULT '0',
  `user_lastname` varchar(50) NOT NULL DEFAULT '0',
  `user_username` varchar(50) NOT NULL DEFAULT '0',
  `user_password_text` varchar(50) NOT NULL DEFAULT '0',
  `user_password_hash` varchar(50) NOT NULL DEFAULT '0',
  `user_age` int(11) NOT NULL DEFAULT '0',
  `user_gender` varchar(50) NOT NULL DEFAULT '0',
  `user_rate` double NOT NULL DEFAULT '0',
  `user_contact_number` varchar(50) NOT NULL DEFAULT '0',
  `user_address` varchar(100) NOT NULL DEFAULT '0',
  `user_type` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_user: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`user_id`, `user_firstname`, `user_lastname`, `user_username`, `user_password_text`, `user_password_hash`, `user_age`, `user_gender`, `user_rate`, `user_contact_number`, `user_address`, `user_type`, `status`) VALUES
	(1, 'admin', 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, '0', 0, '0', '0', 1, 0),
	(6, 'engr', 'engr', 'engr', 'engr', 'b1a506e2c9c65fbec86acf6a9e8145f4', 22, 'Female', 0, '09745273856', 'Amazon Sreet, Sta Anna', 2, 0),
	(8, 'Claire', 'Simons', 'clairesimons', 'FinKsN3P', '9b149e29787d7f2a8cea304e843e3b6a', 22, 'Female', 739.6, '09322756483', 'Sander Street, Kentucky', 2, 0),
	(9, 'Alvin', 'Stuart', 'alvinStuart', 'goJaDjW1', 'c5f868ecb6f7bc7cf67c2a325183579a', 45, 'Male', 830.68, '094276946374', 'Rancho Street, North Carolina', 2, 0),
	(10, 'Client', 'Client', 'client', 'client', '62608e08adc29a8d6dbc9754e659f125', 23, 'Female', 0, '0974627482', 'Brgy. Tangub, Bacolod City', 3, 0),
	(11, 'Aldrin', 'Cruz', 'aldrinCruz', 'cCO5DL3E', '04dfcaa1db889214d190626a66bdebb2', 34, 'Male', 0, '0978674536', 'Brgy. Estifania Bacolod City', 3, 0);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;


-- Dumping structure for view mku_system.view_project
DROP VIEW IF EXISTS `view_project`;
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view_project` (
	`project_id` INT(11) NOT NULL,
	`project_name` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`project_location` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`project_start_date` DATE NOT NULL,
	`project_end_date` DATE NOT NULL,
	`engineer_name` VARCHAR(101) NOT NULL COLLATE 'latin1_swedish_ci',
	`client_name` VARCHAR(101) NOT NULL COLLATE 'latin1_swedish_ci',
	`status` VARCHAR(9) NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;


-- Dumping structure for view mku_system.view_project
DROP VIEW IF EXISTS `view_project`;
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view_project`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_project` AS SELECT p.project_id, p.project_name, p.project_location,
						p.project_start_date, p.project_end_date,
						CONCAT(u1.user_firstname,' ',u1.user_lastname) as engineer_name, 
						CONCAT(u2.user_firstname,' ',u2.user_lastname) as client_name, 
						CASE 
							WHEN p.status = 0	THEN 'Approved'
							WHEN p.status = 1 THEN 'Completed'
							WHEN p.status = 2 THEN 'On going'
						END as status
				FROM tbl_project as p
				INNER JOIN tbl_project_engineer as pe
				INNER JOIN tbl_project_client as pc
				ON
					p.project_id = pe.project_id
				AND
					p.project_id = pc.project_id
				INNER JOIN tbl_user as u1
				INNER JOIN tbl_user as u2
				ON
					pe.user_id = u1.user_id
				AND
					pc.user_id = u2.user_id ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
