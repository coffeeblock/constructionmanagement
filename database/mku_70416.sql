-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for rcyo_system
DROP DATABASE IF EXISTS `rcyo_system`;
CREATE DATABASE IF NOT EXISTS `rcyo_system` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rcyo_system`;


-- Dumping structure for procedure rcyo_system.proc_appointment
DROP PROCEDURE IF EXISTS `proc_appointment`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_appointment`(IN `id` INT)
BEGIN 
SELECT a.appointment_id, a.appointment_subject, a.appointment_date,
		a.appointment_time, CONCAT(u.user_firstname,' ',u.user_lastname) as name,
		a.status
FROM tbl_appointment as a
INNER JOIN tbl_user as u
	ON
		a.user_id = u.user_id
WHERE
	a.appointment_id = id
	GROUP BY a.appointment_id;
END//
DELIMITER ;


-- Dumping structure for procedure rcyo_system.proc_client_notif
DROP PROCEDURE IF EXISTS `proc_client_notif`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_client_notif`(IN id INT)
BEGIN
SELECT 
					'RC Young Optical' as name ,
					n.notification_id,u.user_id,
					IF(n.appointment_id IS 	NULL, o.order_id, a.appointment_id) as id,
					IF(n.appointment_id IS NULL, 'Customization', 'Appointment') as type,
					IF(n.appointment_id IS NULL, o.order_date, a.appointment_date) as date,
					IF(n.status = 1, 'Approved', 'Declined') as status
				
				FROM tbl_notification as n
				INNER JOIN tbl_appointment as a
				INNER JOIN tbl_order as o
				INNER JOIN tbl_user as u
				ON
				(	
					n.appointment_id = a.appointment_id
					AND
					a.user_id = u.user_id
				)
				OR
				(
					n.order_id = o.order_id
					AND	
					o.user_id = u.user_id
				)
				WHERE
					u.user_id = id
				AND
					(n.status = 1 OR n.status = 2)

				GROUP BY n.notification_id;
END//
DELIMITER ;


-- Dumping structure for procedure rcyo_system.proc_order
DROP PROCEDURE IF EXISTS `proc_order`;
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `proc_order`(IN `id` INT)
BEGIN
SELECT o.order_id, f.frame_id, s.shape_name, c.color_name,
			b.brand_name, l.lens_name, o.status
FROM tbl_order as o
INNER JOIN tbl_frame as f
INNER JOIN tbl_lens as l
	ON
		o.frame_id = f.frame_id
	AND
		o.lens_id = l.lens_id
INNER JOIN tbl_color as c
INNER JOIN tbl_shape as s
INNER JOIN tbl_brand as b
ON
	f.shape_id = s.shape_id
AND
	f.color_id = c.color_id
AND
	f.brand_id = b.brand_id
AND 
	o.order_id = id;
END//
DELIMITER ;


-- Dumping structure for table rcyo_system.tbl_appointment
DROP TABLE IF EXISTS `tbl_appointment`;
CREATE TABLE IF NOT EXISTS `tbl_appointment` (
  `appointment_id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_subject` varchar(100) NOT NULL DEFAULT '0',
  `appointment_date` date NOT NULL DEFAULT '0000-00-00',
  `appointment_time` time NOT NULL DEFAULT '00:00:00',
  `appointment_request_date` date NOT NULL DEFAULT '0000-00-00',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`appointment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_appointment: ~4 rows (approximately)
DELETE FROM `tbl_appointment`;
/*!40000 ALTER TABLE `tbl_appointment` DISABLE KEYS */;
INSERT INTO `tbl_appointment` (`appointment_id`, `appointment_subject`, `appointment_date`, `appointment_time`, `appointment_request_date`, `user_id`, `status`) VALUES
	(10, 'Consultation', '2016-07-08', '09:00:00', '2016-07-08', 19, 1),
	(11, 'Inquiry', '2016-07-09', '08:00:00', '2016-07-08', 22, 2),
	(13, 'Inquiry', '2016-07-08', '10:00:00', '2016-07-06', 22, 1),
	(14, 'Consultation', '2016-07-13', '13:30:00', '2016-07-01', 19, 1);
/*!40000 ALTER TABLE `tbl_appointment` ENABLE KEYS */;


-- Dumping structure for table rcyo_system.tbl_brand
DROP TABLE IF EXISTS `tbl_brand`;
CREATE TABLE IF NOT EXISTS `tbl_brand` (
  `brand_id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_name` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`brand_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_brand: ~2 rows (approximately)
DELETE FROM `tbl_brand`;
/*!40000 ALTER TABLE `tbl_brand` DISABLE KEYS */;
INSERT INTO `tbl_brand` (`brand_id`, `brand_name`, `status`) VALUES
	(1, 'Ray-Ban', 0),
	(2, 'Oakley', 0),
	(3, 'D&G', 0);
/*!40000 ALTER TABLE `tbl_brand` ENABLE KEYS */;


-- Dumping structure for table rcyo_system.tbl_color
DROP TABLE IF EXISTS `tbl_color`;
CREATE TABLE IF NOT EXISTS `tbl_color` (
  `color_id` int(11) NOT NULL AUTO_INCREMENT,
  `color_name` varchar(50) DEFAULT '0',
  `status` varchar(50) DEFAULT '0',
  PRIMARY KEY (`color_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_color: ~4 rows (approximately)
DELETE FROM `tbl_color`;
/*!40000 ALTER TABLE `tbl_color` DISABLE KEYS */;
INSERT INTO `tbl_color` (`color_id`, `color_name`, `status`) VALUES
	(1, 'black', '0'),
	(2, 'grey', '0'),
	(3, 'white', '0'),
	(4, 'brown', '0');
/*!40000 ALTER TABLE `tbl_color` ENABLE KEYS */;


-- Dumping structure for table rcyo_system.tbl_frame
DROP TABLE IF EXISTS `tbl_frame`;
CREATE TABLE IF NOT EXISTS `tbl_frame` (
  `frame_id` int(11) NOT NULL AUTO_INCREMENT,
  `shape_id` int(11) NOT NULL DEFAULT '0',
  `color_id` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`frame_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_frame: ~0 rows (approximately)
DELETE FROM `tbl_frame`;
/*!40000 ALTER TABLE `tbl_frame` DISABLE KEYS */;
INSERT INTO `tbl_frame` (`frame_id`, `shape_id`, `color_id`, `brand_id`, `status`) VALUES
	(1, 1, 1, 1, 0);
/*!40000 ALTER TABLE `tbl_frame` ENABLE KEYS */;


-- Dumping structure for table rcyo_system.tbl_glasses
DROP TABLE IF EXISTS `tbl_glasses`;
CREATE TABLE IF NOT EXISTS `tbl_glasses` (
  `glasses_id` int(11) NOT NULL AUTO_INCREMENT,
  `glasses_code` varchar(50) NOT NULL DEFAULT '0',
  `glasses_name` varchar(50) NOT NULL DEFAULT '0',
  `glasses_img` varchar(100) NOT NULL DEFAULT '0',
  `glasses_gender_pref` int(11) NOT NULL DEFAULT '0',
  `glasses_type` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `shape_id` int(11) NOT NULL DEFAULT '0',
  `color_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`glasses_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_glasses: ~1 rows (approximately)
DELETE FROM `tbl_glasses`;
/*!40000 ALTER TABLE `tbl_glasses` DISABLE KEYS */;
INSERT INTO `tbl_glasses` (`glasses_id`, `glasses_code`, `glasses_name`, `glasses_img`, `glasses_gender_pref`, `glasses_type`, `brand_id`, `shape_id`, `color_id`, `status`) VALUES
	(1, '0', 'Mainliknk PRIZM', 'images/glasses/glasses1.png', 2, 2, 2, 1, 1, 0),
	(2, '0', 'Oakley Titaniun', 'images/glasses/2.jpg', 1, 1, 2, 2, 2, 0);
/*!40000 ALTER TABLE `tbl_glasses` ENABLE KEYS */;


-- Dumping structure for table rcyo_system.tbl_lens
DROP TABLE IF EXISTS `tbl_lens`;
CREATE TABLE IF NOT EXISTS `tbl_lens` (
  `lens_id` int(11) NOT NULL AUTO_INCREMENT,
  `lens_name` varchar(50) NOT NULL DEFAULT '0',
  `lens_grade` double NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lens_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_lens: ~0 rows (approximately)
DELETE FROM `tbl_lens`;
/*!40000 ALTER TABLE `tbl_lens` DISABLE KEYS */;
INSERT INTO `tbl_lens` (`lens_id`, `lens_name`, `lens_grade`, `brand_id`, `status`) VALUES
	(1, 'Biofinity', 0, 1, 0);
/*!40000 ALTER TABLE `tbl_lens` ENABLE KEYS */;


-- Dumping structure for table rcyo_system.tbl_notification
DROP TABLE IF EXISTS `tbl_notification`;
CREATE TABLE IF NOT EXISTS `tbl_notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `appointment_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_notification: ~2 rows (approximately)
DELETE FROM `tbl_notification`;
/*!40000 ALTER TABLE `tbl_notification` DISABLE KEYS */;
INSERT INTO `tbl_notification` (`notification_id`, `appointment_id`, `order_id`, `status`) VALUES
	(9, 10, 0, 1),
	(10, 11, 0, 2),
	(12, 13, 0, 1),
	(13, 14, 0, 1);
/*!40000 ALTER TABLE `tbl_notification` ENABLE KEYS */;


-- Dumping structure for table rcyo_system.tbl_order
DROP TABLE IF EXISTS `tbl_order`;
CREATE TABLE IF NOT EXISTS `tbl_order` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `order_date` date NOT NULL DEFAULT '0000-00-00',
  `frame_id` int(11) NOT NULL DEFAULT '0',
  `lens_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_order: ~1 rows (approximately)
DELETE FROM `tbl_order`;
/*!40000 ALTER TABLE `tbl_order` DISABLE KEYS */;
INSERT INTO `tbl_order` (`order_id`, `order_date`, `frame_id`, `lens_id`, `user_id`, `status`) VALUES
	(1, '2016-07-07', 1, 1, 19, 0);
/*!40000 ALTER TABLE `tbl_order` ENABLE KEYS */;


-- Dumping structure for table rcyo_system.tbl_product
DROP TABLE IF EXISTS `tbl_product`;
CREATE TABLE IF NOT EXISTS `tbl_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_code` varchar(50) NOT NULL DEFAULT '0',
  `product_name` varchar(50) NOT NULL DEFAULT '0',
  `product_img` varchar(100) NOT NULL DEFAULT '0',
  `product_gender_pref` int(11) NOT NULL DEFAULT '0',
  `product_type` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) NOT NULL DEFAULT '0',
  `shape_id` int(11) NOT NULL DEFAULT '0',
  `color_id` int(11) NOT NULL DEFAULT '0',
  `grade` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_product: ~8 rows (approximately)
DELETE FROM `tbl_product`;
/*!40000 ALTER TABLE `tbl_product` DISABLE KEYS */;
INSERT INTO `tbl_product` (`product_id`, `product_code`, `product_name`, `product_img`, `product_gender_pref`, `product_type`, `brand_id`, `shape_id`, `color_id`, `grade`, `status`) VALUES
	(1, 'OKL0001', 'MainLink PRIZM', 'glasses1.png', 2, 2, 2, 2, 2, 0, 0),
	(2, 'OKL0002', 'Titanium', '2.jpg', 1, 1, 1, 1, 1, 0, 0),
	(4, '2191016', 'Silver', '0 (1).jpg', 1, 2, 1, 1, 2, 0, 0),
	(9, '118121643', 'Crimsol LX', '125415_lg.jpg', 1, 1, 2, 1, 4, 0, 0),
	(10, '118121638', 'Classic XE', 'eyeglasses.jpg', 1, 1, 3, 1, 3, 0, 0),
	(11, '118121635', 'Snipe FL', 'FAM13A.jpg', 2, 1, 1, 3, 4, 0, 0),
	(12, '118121646', 'Denver CL01', 'arden_a3204_c1_front.jpg', 2, 1, 2, 2, 4, 0, 0),
	(13, '118121636', 'Classic FG20', '7.jpg', 1, 1, 1, 1, 4, 0, 0);
/*!40000 ALTER TABLE `tbl_product` ENABLE KEYS */;


-- Dumping structure for table rcyo_system.tbl_shape
DROP TABLE IF EXISTS `tbl_shape`;
CREATE TABLE IF NOT EXISTS `tbl_shape` (
  `shape_id` int(11) NOT NULL AUTO_INCREMENT,
  `shape_name` varchar(50) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`shape_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_shape: ~2 rows (approximately)
DELETE FROM `tbl_shape`;
/*!40000 ALTER TABLE `tbl_shape` DISABLE KEYS */;
INSERT INTO `tbl_shape` (`shape_id`, `shape_name`, `status`) VALUES
	(1, 'Oval', 0),
	(2, 'Round', 0),
	(3, 'Square', 0);
/*!40000 ALTER TABLE `tbl_shape` ENABLE KEYS */;


-- Dumping structure for table rcyo_system.tbl_user
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(50) NOT NULL DEFAULT '0',
  `user_lastname` varchar(50) NOT NULL DEFAULT '0',
  `user_middlename` varchar(50) NOT NULL DEFAULT '0',
  `user_username` varchar(50) NOT NULL DEFAULT '0',
  `user_password_text` varchar(50) NOT NULL DEFAULT '0',
  `user_password_hash` varchar(50) NOT NULL DEFAULT '0',
  `user_gender` varchar(50) NOT NULL DEFAULT '0',
  `user_age` int(11) NOT NULL DEFAULT '0',
  `user_contact_number` varchar(50) NOT NULL DEFAULT '0',
  `user_email_address` varchar(50) NOT NULL DEFAULT '0',
  `user_address` varchar(50) NOT NULL DEFAULT '0',
  `user_type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- Dumping data for table rcyo_system.tbl_user: ~5 rows (approximately)
DELETE FROM `tbl_user`;
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
INSERT INTO `tbl_user` (`user_id`, `user_firstname`, `user_lastname`, `user_middlename`, `user_username`, `user_password_text`, `user_password_hash`, `user_gender`, `user_age`, `user_contact_number`, `user_email_address`, `user_address`, `user_type`) VALUES
	(1, 'admin', 'admin', 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', '0', 0, '0', '0', '0', 1),
	(19, 'client', 'client', 'client', 'client', 'client', '62608e08adc29a8d6dbc9754e659f125', 'M', 21, '0998877665', 'client@gmail.com', 'Ignacio Street', 3),
	(22, 'a', 'a', 'a', 'a', 'a', '0cc175b9c0f1b6a831c399e269772661', 'F', 32, '1', '1', '1', 3),
	(23, 'doctor', 'doctor', 'doctor', 'doctor', 'doctor', 'f9f16d97c90d8c6f2cab37bb6d1f1992', 'F', 25, '09575800000', 'doctor@gmail.com', 'Tangub', 2);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
