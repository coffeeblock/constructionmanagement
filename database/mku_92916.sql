-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.6.21 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.1.0.4867
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for mku_system
CREATE DATABASE IF NOT EXISTS `mku_system` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `mku_system`;


-- Dumping structure for procedure mku_system.GetProjectById
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetProjectById`(IN id INT)
BEGIN
SELECT p.project_id, p.project_name, p.project_location,
						p.project_start_date, p.project_end_date,
						p.project_dim_length, p.project_dim_height,
						p.project_dim_width, p.project_dim_lanes,
						pe.user_id as project_engineer, 
						pc.user_id as project_client,
						CASE 
							WHEN p.status = 0	THEN 'Approved'
							WHEN p.status = 1 THEN 'Completed'
							WHEN p.status = 2 THEN 'On going'
						END as status
				FROM tbl_project as p
				INNER JOIN tbl_project_engineer as pe
				INNER JOIN tbl_project_client as pc
				ON
					p.project_id = pe.project_id
				AND
					p.project_id = pc.project_id
				WHERE
					p.project_id = id;
END//
DELIMITER ;


-- Dumping structure for procedure mku_system.GetProjectByUserId
DELIMITER //
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetProjectByUserId`(IN `id` INT)
BEGIN
SELECT p.project_id, p.project_name, p.project_location, 
				p.project_start_date, p.project_end_date,
				CONCAT(u1.user_firstname,' ',u1.user_lastname) as engineer_name,
				CONCAT(u2.user_firstname,' ',u2.user_lastname) as client_name,
				CASE 
					WHEN p.status = 0	THEN 'Approved'
					WHEN p.status = 1 THEN 'Completed'
					WHEN p.status = 2 THEN 'On going'
				END as status
				FROM tbl_project AS p
				INNER JOIN tbl_project_engineer as pe
					ON
							p.project_id = pe.project_id
				INNER JOIN tbl_project_client as pc
					
						ON
							p.project_id = pc.project_id
				INNER JOIN tbl_user as u1
				ON
					pe.user_id = u1.user_id
				INNER JOIN tbl_user as u2
				
				ON
					pc.user_id = u2.user_id
				WHERE
					IF((SELECT count(*) FROM tbl_project_engineer WHERE user_id = id), 
						pe.user_id = id, pc.user_id = id)
				GROUP BY p.project_id;
END//
DELIMITER ;


-- Dumping structure for table mku_system.tbl_attendance
CREATE TABLE IF NOT EXISTS `tbl_attendance` (
  `a_id` int(11) NOT NULL AUTO_INCREMENT,
  `a_am` int(11) NOT NULL DEFAULT '0',
  `a_pm` int(11) NOT NULL DEFAULT '0',
  `a_date` date NOT NULL DEFAULT '0000-00-00',
  `tw_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`a_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_attendance: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_attendance` DISABLE KEYS */;
REPLACE INTO `tbl_attendance` (`a_id`, `a_am`, `a_pm`, `a_date`, `tw_id`, `task_id`) VALUES
	(1, 1, 1, '2016-09-26', 1, 7),
	(2, 1, 0, '2016-09-26', 2, 7);
/*!40000 ALTER TABLE `tbl_attendance` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_equipment
CREATE TABLE IF NOT EXISTS `tbl_equipment` (
  `equipment_id` int(11) NOT NULL AUTO_INCREMENT,
  `equipment_name` varchar(100) NOT NULL DEFAULT '0',
  `equipment_rate_day` varchar(30) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`equipment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_equipment: ~12 rows (approximately)
/*!40000 ALTER TABLE `tbl_equipment` DISABLE KEYS */;
REPLACE INTO `tbl_equipment` (`equipment_id`, `equipment_name`, `equipment_rate_day`, `status`) VALUES
	(1, 'Road grader', '789.65', 1),
	(2, 'Road Roller', '789.65', 1),
	(3, 'Water tank', '659.76', 1),
	(4, 'Backhoe', '367.9', 1),
	(5, 'Payloader', '439.87', 1),
	(6, 'Transit mixer', '985.56', 1),
	(7, 'Concrete vibrator', '367.98', 1),
	(8, 'Concrete cutter', '327.78', 1),
	(9, 'Bulldozer', '689.79', 1),
	(10, 'Payloader', '584.77', 1),
	(11, 'Dump track', '546.87', 1),
	(12, 'sample', '248.32', 0);
/*!40000 ALTER TABLE `tbl_equipment` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_equipment_log
CREATE TABLE IF NOT EXISTS `tbl_equipment_log` (
  `el_id` int(11) NOT NULL AUTO_INCREMENT,
  `el_date_sent` date NOT NULL DEFAULT '0000-00-00',
  `el_date_returned` date NOT NULL DEFAULT '0000-00-00',
  `el_date_due` date NOT NULL DEFAULT '0000-00-00',
  `equipment_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`el_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_equipment_log: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_equipment_log` DISABLE KEYS */;
REPLACE INTO `tbl_equipment_log` (`el_id`, `el_date_sent`, `el_date_returned`, `el_date_due`, `equipment_id`, `project_id`, `user_id`, `status`) VALUES
	(1, '2016-09-28', '2016-10-02', '2016-09-30', 2, 1, 12, 0),
	(2, '2016-09-26', '2016-10-04', '2016-09-30', 1, 1, 13, 0);
/*!40000 ALTER TABLE `tbl_equipment_log` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_estimated_task
CREATE TABLE IF NOT EXISTS `tbl_estimated_task` (
  `et_id` int(11) NOT NULL AUTO_INCREMENT,
  `et_name` varchar(50) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`et_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_estimated_task: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_estimated_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_estimated_task` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_generated_fixed_cost
CREATE TABLE IF NOT EXISTS `tbl_generated_fixed_cost` (
  `gc_id` int(11) NOT NULL AUTO_INCREMENT,
  `gc_quantity` int(11) NOT NULL DEFAULT '0',
  `gc_total` double(10,2) NOT NULL DEFAULT '0.00',
  `material_id` int(11) NOT NULL DEFAULT '0',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`gc_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_generated_fixed_cost: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_generated_fixed_cost` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbl_generated_fixed_cost` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_inventory
CREATE TABLE IF NOT EXISTS `tbl_inventory` (
  `inventory_id` int(11) NOT NULL AUTO_INCREMENT,
  `inventory_quantity` int(11) NOT NULL DEFAULT '0',
  `material_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`inventory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_inventory: ~15 rows (approximately)
/*!40000 ALTER TABLE `tbl_inventory` DISABLE KEYS */;
REPLACE INTO `tbl_inventory` (`inventory_id`, `inventory_quantity`, `material_id`, `status`) VALUES
	(1, 927721, 1, 0),
	(2, 101524, 2, 0),
	(3, 61486, 3, 0),
	(4, 3536, 4, 0),
	(5, 8986, 5, 0),
	(6, 114986, 6, 0),
	(7, 8486, 7, 0),
	(8, 2386, 8, 0),
	(9, 40686, 9, 0),
	(10, 4136, 10, 0),
	(11, 41042, 11, 0),
	(12, 22986, 12, 0),
	(13, 62986, 13, 0),
	(14, 72486, 14, 0),
	(15, 15064, 15, 0),
	(16, -14, 16, 0);
/*!40000 ALTER TABLE `tbl_inventory` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_manpower
CREATE TABLE IF NOT EXISTS `tbl_manpower` (
  `manpower_id` int(11) NOT NULL AUTO_INCREMENT,
  `manpower_name` varchar(100) NOT NULL DEFAULT '0',
  `manpower_rate_day` double NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`manpower_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_manpower: ~4 rows (approximately)
/*!40000 ALTER TABLE `tbl_manpower` DISABLE KEYS */;
REPLACE INTO `tbl_manpower` (`manpower_id`, `manpower_name`, `manpower_rate_day`, `status`) VALUES
	(1, 'Foreman', 450.6, 1),
	(2, 'Operator', 370.76, 1),
	(3, 'Driver', 370.56, 1),
	(4, 'Helper', 298.5, 1),
	(5, 'Mason', 290.76, 1);
/*!40000 ALTER TABLE `tbl_manpower` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_material
CREATE TABLE IF NOT EXISTS `tbl_material` (
  `material_id` int(11) NOT NULL AUTO_INCREMENT,
  `material_name` varchar(100) NOT NULL DEFAULT '0',
  `material_unit` varchar(30) NOT NULL DEFAULT '0',
  `material_cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`material_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_material: ~14 rows (approximately)
/*!40000 ALTER TABLE `tbl_material` DISABLE KEYS */;
REPLACE INTO `tbl_material` (`material_id`, `material_name`, `material_unit`, `material_cost`, `status`) VALUES
	(1, 'Cement', 'bag', 235.00, 1),
	(2, 'Gravel', 'm3', 760.00, 1),
	(3, 'Sand', 'm3', 550.00, 1),
	(4, 'Reinforcing Steel Bar', '6.0 meters (1 piece)', 34.00, 1),
	(5, 'Steel Forms', '6.0 meter (1 piece)', 60.00, 1),
	(6, 'Fuel', 'Liter', 46.89, 1),
	(7, 'Curing compound', '1 Liter/15 m2', 58.78, 1),
	(8, 'Asphalt sealant', '1 Liter/80m', 67.56, 1),
	(9, 'Diesel', 'Liter', 35.76, 1),
	(10, 'Gasoline', 'Liter', 46.78, 1),
	(11, 'Item104', 'm3', 37.98, 1),
	(12, 'Item105', 'm2', 39.78, 1),
	(13, 'Item200', 'm3', 41.87, 1),
	(14, 'Item201', 'm3', 29.78, 1),
	(15, 'Item102', 'm3', 57.54, 1),
	(16, 'sample1', 'm', 293.10, 0);
/*!40000 ALTER TABLE `tbl_material` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project
CREATE TABLE IF NOT EXISTS `tbl_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(100) NOT NULL,
  `project_location` varchar(100) NOT NULL,
  `project_start_date` date NOT NULL DEFAULT '0000-00-00',
  `project_end_date` date NOT NULL DEFAULT '0000-00-00',
  `project_dim_length` double NOT NULL,
  `project_dim_height` double NOT NULL,
  `project_dim_width` double NOT NULL,
  `project_dim_lanes` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_project` DISABLE KEYS */;
REPLACE INTO `tbl_project` (`project_id`, `project_name`, `project_location`, `project_start_date`, `project_end_date`, `project_dim_length`, `project_dim_height`, `project_dim_width`, `project_dim_lanes`, `status`) VALUES
	(1, 'project 001', 'Bacolod', '2016-09-01', '2016-09-07', 1000, 0.7, 6.8, 2, 0);
/*!40000 ALTER TABLE `tbl_project` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project_client
CREATE TABLE IF NOT EXISTS `tbl_project_client` (
  `pc_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pc_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project_client: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_project_client` DISABLE KEYS */;
REPLACE INTO `tbl_project_client` (`pc_id`, `user_id`, `project_id`, `status`) VALUES
	(1, 10, 1, 0);
/*!40000 ALTER TABLE `tbl_project_client` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_project_engineer
CREATE TABLE IF NOT EXISTS `tbl_project_engineer` (
  `pe_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  PRIMARY KEY (`pe_id`),
  KEY `project_id` (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_project_engineer: ~0 rows (approximately)
/*!40000 ALTER TABLE `tbl_project_engineer` DISABLE KEYS */;
REPLACE INTO `tbl_project_engineer` (`pe_id`, `user_id`, `project_id`, `status`) VALUES
	(1, 6, 1, 0);
/*!40000 ALTER TABLE `tbl_project_engineer` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_returned_items
CREATE TABLE IF NOT EXISTS `tbl_returned_items` (
  `ri_id` int(11) NOT NULL AUTO_INCREMENT,
  `ri_quantity` int(11) NOT NULL DEFAULT '0',
  `material_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ri_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_returned_items: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_returned_items` DISABLE KEYS */;
REPLACE INTO `tbl_returned_items` (`ri_id`, `ri_quantity`, `material_id`, `task_id`, `status`) VALUES
	(4, 38, 2, 7, 0),
	(5, 1000, 8, 7, 0);
/*!40000 ALTER TABLE `tbl_returned_items` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task
CREATE TABLE IF NOT EXISTS `tbl_task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_description` varchar(100) NOT NULL DEFAULT '0',
  `task_duration` int(11) NOT NULL DEFAULT '0',
  `task_start_date` date NOT NULL DEFAULT '0000-00-00',
  `task_end_date` date NOT NULL DEFAULT '0000-00-00',
  `project_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task: ~6 rows (approximately)
/*!40000 ALTER TABLE `tbl_task` DISABLE KEYS */;
REPLACE INTO `tbl_task` (`task_id`, `task_description`, `task_duration`, `task_start_date`, `task_end_date`, `project_id`, `status`) VALUES
	(1, 'Excavation', 0, '0000-00-00', '0000-00-00', 1, 1),
	(2, 'Embankment', 0, '0000-00-00', '0000-00-00', 1, 1),
	(3, 'Sub-grade', 0, '0000-00-00', '0000-00-00', 1, 1),
	(4, 'Sub-base', 0, '0000-00-00', '0000-00-00', 1, 1),
	(5, 'Base course', 0, '2016-09-01', '2016-09-10', 1, 1),
	(6, 'PCCP', 0, '0000-00-00', '0000-00-00', 1, 1),
	(7, 'Task01_proj001', 0, '2016-09-01', '2016-09-07', 1, 2);
/*!40000 ALTER TABLE `tbl_task` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_equipment
CREATE TABLE IF NOT EXISTS `tbl_task_equipment` (
  `te_id` int(11) NOT NULL AUTO_INCREMENT,
  `te_quantity` int(11) NOT NULL DEFAULT '0',
  `te_days` int(11) NOT NULL DEFAULT '0',
  `te_total` double(10,2) NOT NULL DEFAULT '0.00',
  `equipment_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`te_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_equipment: ~34 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_equipment` DISABLE KEYS */;
REPLACE INTO `tbl_task_equipment` (`te_id`, `te_quantity`, `te_days`, `te_total`, `equipment_id`, `task_id`, `status`) VALUES
	(1, 1, 3, 2069.37, 9, 1, 1),
	(2, 1, 3, 1754.31, 10, 1, 1),
	(3, 1, 3, 2069.37, 9, 1, 1),
	(4, 2, 5, 5468.70, 11, 1, 1),
	(5, 1, 2, 1579.30, 1, 2, 1),
	(6, 1, 2, 1579.30, 2, 2, 1),
	(7, 1, 1, 659.76, 3, 2, 1),
	(8, 1, 1, 789.65, 1, 3, 1),
	(9, 1, 1, 789.65, 2, 3, 1),
	(10, 1, 1, 659.76, 3, 3, 1),
	(11, 1, 2, 1579.30, 1, 4, 1),
	(12, 1, 2, 1579.30, 2, 4, 1),
	(13, 1, 1, 659.76, 3, 4, 1),
	(14, 2, 2, 3158.60, 1, 5, 1),
	(15, 1, 2, 1579.30, 2, 5, 1),
	(16, 1, 1, 659.76, 3, 5, 1),
	(17, 1, 13, 4905.33, 4, 6, 1),
	(18, 1, 13, 5864.93, 5, 6, 1),
	(19, 4, 13, 52563.20, 6, 6, 1),
	(20, 1, 13, 8796.80, 3, 6, 1),
	(21, 2, 13, 9812.80, 7, 6, 1),
	(22, 1, 13, 4370.40, 8, 6, 1),
	(23, 10, 9, 71068.50, 1, 7, 0),
	(24, 3, 9, 21320.55, 2, 7, 0),
	(25, 2, 9, 11875.68, 3, 7, 0),
	(26, 0, 0, 0.00, 4, 7, 0),
	(27, 0, 0, 0.00, 5, 7, 0),
	(28, 0, 0, 0.00, 6, 7, 0),
	(29, 0, 0, 0.00, 7, 7, 0),
	(30, 0, 0, 0.00, 8, 7, 0),
	(31, 0, 0, 0.00, 9, 7, 0),
	(32, 0, 0, 0.00, 10, 7, 0),
	(33, 2, 9, 9843.66, 11, 7, 0),
	(34, 0, 0, 0.00, 12, 7, 0);
/*!40000 ALTER TABLE `tbl_task_equipment` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_manpower
CREATE TABLE IF NOT EXISTS `tbl_task_manpower` (
  `tmp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tmp_quantity` int(11) NOT NULL DEFAULT '0',
  `tmp_days` int(11) NOT NULL DEFAULT '0',
  `tmp_total` double(10,2) NOT NULL DEFAULT '0.00',
  `manpower_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tmp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_manpower: ~32 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_manpower` DISABLE KEYS */;
REPLACE INTO `tbl_task_manpower` (`tmp_id`, `tmp_quantity`, `tmp_days`, `tmp_total`, `manpower_id`, `task_id`, `status`) VALUES
	(1, 1, 3, 1351.80, 1, 1, 1),
	(2, 1, 3, 1112.28, 2, 1, 1),
	(3, 1, 3, 1112.28, 2, 1, 1),
	(4, 1, 3, 1112.28, 2, 1, 1),
	(5, 2, 5, 3705.60, 3, 1, 1),
	(6, 4, 3, 3582.00, 4, 1, 1),
	(7, 1, 2, 901.20, 1, 2, 1),
	(8, 2, 2, 1483.04, 2, 2, 1),
	(9, 1, 1, 370.56, 3, 2, 1),
	(10, 4, 2, 2388.00, 4, 2, 1),
	(11, 1, 1, 450.60, 1, 3, 1),
	(12, 2, 1, 741.52, 2, 3, 1),
	(13, 1, 1, 370.56, 3, 3, 1),
	(14, 4, 1, 1194.00, 4, 3, 1),
	(15, 1, 2, 901.20, 1, 4, 1),
	(16, 2, 2, 1483.04, 2, 4, 1),
	(17, 1, 1, 370.56, 3, 4, 1),
	(18, 4, 2, 2388.00, 4, 4, 1),
	(19, 1, 2, 901.20, 1, 5, 1),
	(20, 2, 2, 1483.04, 2, 5, 1),
	(21, 1, 1, 370.56, 3, 5, 1),
	(22, 4, 2, 2388.00, 4, 5, 1),
	(23, 1, 13, 6008.00, 1, 6, 1),
	(24, 2, 13, 9886.93, 2, 6, 1),
	(25, 5, 13, 24704.00, 3, 6, 1),
	(26, 4, 13, 15507.20, 5, 6, 1),
	(27, 15, 13, 59700.00, 4, 6, 1),
	(28, 1, 9, 4055.40, 1, 7, 0),
	(29, 3, 9, 10010.52, 2, 7, 0),
	(30, 1, 9, 3335.04, 3, 7, 0),
	(31, 14, 9, 37611.00, 4, 7, 0),
	(32, 0, 0, 0.00, 5, 7, 0);
/*!40000 ALTER TABLE `tbl_task_manpower` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_material
CREATE TABLE IF NOT EXISTS `tbl_task_material` (
  `tm_id` int(11) NOT NULL AUTO_INCREMENT,
  `tm_quantity` double(10,2) NOT NULL DEFAULT '0.00',
  `tm_total` double(10,2) NOT NULL DEFAULT '0.00',
  `material_id` int(11) NOT NULL DEFAULT '0',
  `task_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tm_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_material: ~35 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_material` DISABLE KEYS */;
REPLACE INTO `tbl_task_material` (`tm_id`, `tm_quantity`, `tm_total`, `material_id`, `task_id`, `status`) VALUES
	(1, 977.50, 56245.35, 15, 1, 1),
	(2, 1012.00, 47452.68, 6, 1, 1),
	(3, 575.00, 21838.50, 11, 2, 1),
	(4, 414.00, 19412.46, 6, 2, 1),
	(5, 500.00, 19890.00, 12, 3, 1),
	(6, 230.00, 10784.70, 6, 3, 1),
	(7, 575.00, 24075.25, 13, 4, 1),
	(8, 414.00, 19412.46, 6, 4, 1),
	(9, 1500.00, 44670.00, 14, 5, 1),
	(10, 414.00, 19412.46, 6, 5, 1),
	(11, 47600.00, 11186000.00, 1, 6, 1),
	(12, 4760.00, 3617600.00, 2, 6, 1),
	(13, 2380.00, 1309000.00, 3, 6, 1),
	(14, 167.00, 5678.00, 4, 6, 1),
	(15, 454.00, 26686.12, 7, 6, 1),
	(16, 13.00, 878.28, 8, 6, 1),
	(17, 416.67, 25000.00, 5, 6, 1),
	(18, 4002.00, 143111.52, 9, 6, 1),
	(19, 401.00, 18758.78, 10, 6, 1),
	(20, 100.00, 23500.00, 1, 7, 0),
	(21, 300.00, 228000.00, 2, 7, 0),
	(22, 250.00, 137500.00, 3, 7, 0),
	(23, 340.00, 11560.00, 4, 7, 0),
	(24, 280.00, 16800.00, 5, 7, 0),
	(25, 439.00, 20584.71, 6, 7, 0),
	(26, 0.00, 0.00, 7, 7, 0),
	(27, 1387.00, 93705.72, 8, 7, 0),
	(28, 0.00, 0.00, 9, 7, 0),
	(29, 20.00, 935.60, 10, 7, 0),
	(30, 500.00, 18990.00, 11, 7, 0),
	(31, 0.00, 0.00, 12, 7, 0),
	(32, 0.00, 0.00, 13, 7, 0),
	(33, 0.00, 0.00, 14, 7, 0),
	(34, 0.00, 0.00, 15, 7, 0),
	(35, 0.00, 0.00, 16, 7, 0);
/*!40000 ALTER TABLE `tbl_task_material` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_task_worker
CREATE TABLE IF NOT EXISTS `tbl_task_worker` (
  `tw_id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`tw_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_task_worker: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_task_worker` DISABLE KEYS */;
REPLACE INTO `tbl_task_worker` (`tw_id`, `task_id`, `user_id`, `status`) VALUES
	(2, 7, 13, 0),
	(4, 7, 12, 0);
/*!40000 ALTER TABLE `tbl_task_worker` ENABLE KEYS */;


-- Dumping structure for table mku_system.tbl_user
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(50) NOT NULL DEFAULT '0',
  `user_lastname` varchar(50) NOT NULL DEFAULT '0',
  `user_username` varchar(50) NOT NULL DEFAULT '0',
  `user_password_text` varchar(50) NOT NULL DEFAULT '0',
  `user_password_hash` varchar(50) NOT NULL DEFAULT '0',
  `user_age` int(11) NOT NULL DEFAULT '0',
  `user_gender` varchar(50) NOT NULL DEFAULT '0',
  `user_rate` double NOT NULL DEFAULT '0',
  `user_contact_number` varchar(50) NOT NULL DEFAULT '0',
  `user_address` varchar(100) NOT NULL DEFAULT '0',
  `user_type` int(11) NOT NULL DEFAULT '0',
  `manpower_id` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table mku_system.tbl_user: ~8 rows (approximately)
/*!40000 ALTER TABLE `tbl_user` DISABLE KEYS */;
REPLACE INTO `tbl_user` (`user_id`, `user_firstname`, `user_lastname`, `user_username`, `user_password_text`, `user_password_hash`, `user_age`, `user_gender`, `user_rate`, `user_contact_number`, `user_address`, `user_type`, `manpower_id`, `status`) VALUES
	(1, 'admin1', 'admin', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 0, 'Female', 0, '0', '0', 1, 0, 0),
	(6, 'engr', 'engr', 'engr', 'engr', 'b1a506e2c9c65fbec86acf6a9e8145f4', 22, 'Female', 0, '09745273856', 'Amazon Sreet, Sta Anna', 2, 0, 0),
	(8, 'Claire', 'Simons', 'clairesimons', 'FinKsN3P', '9b149e29787d7f2a8cea304e843e3b6a', 22, 'Female', 739.6, '09322756483', 'Sander Street, Kentucky', 2, 0, 0),
	(9, 'Alvin', 'Stuart', 'alvinStuart', 'goJaDjW1', 'c5f868ecb6f7bc7cf67c2a325183579a', 45, 'Male', 830.68, '094276946374', 'Rancho Street, North Carolina', 2, 0, 0),
	(10, 'Client', 'Client', 'client', 'client', '62608e08adc29a8d6dbc9754e659f125', 23, 'Female', 0, '0974627482', 'Brgy. Tangub, Bacolod City', 3, 0, 0),
	(11, 'Aldrin', 'Cruz', 'aldrinCruz', 'cCO5DL3E', '04dfcaa1db889214d190626a66bdebb2', 34, 'Male', 0, '0978674536', 'Brgy. Estifania Bacolod City', 3, 0, 0),
	(12, 'worker', 'worker', 'worker', 'worker', 'b61822e8357dcaff77eaaccf348d9134', 32, 'Male', 356.89, '09736342517', 'Brgy. Granada Bacolod City', 5, 1, 1),
	(13, 'a', 'a', 'a', 'a', '0cc175b9c0f1b6a831c399e269772661', 5, 'Female', 55, '5', 'a', 5, 2, 1);
/*!40000 ALTER TABLE `tbl_user` ENABLE KEYS */;


-- Dumping structure for view mku_system.view_inventory
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view_inventory` (
	`material_name` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`material_unit` VARCHAR(30) NOT NULL COLLATE 'latin1_swedish_ci',
	`inventory_quantity` INT(11) NOT NULL,
	`sub` DOUBLE(19,2) NULL,
	`material_id` INT(11) NOT NULL,
	`total` DOUBLE(19,2) NULL
) ENGINE=MyISAM;


-- Dumping structure for view mku_system.view_project
-- Creating temporary table to overcome VIEW dependency errors
CREATE TABLE `view_project` (
	`project_id` INT(11) NOT NULL,
	`project_name` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`project_location` VARCHAR(100) NOT NULL COLLATE 'latin1_swedish_ci',
	`project_start_date` DATE NOT NULL,
	`project_end_date` DATE NOT NULL,
	`engineer_name` VARCHAR(101) NOT NULL COLLATE 'latin1_swedish_ci',
	`client_name` VARCHAR(101) NOT NULL COLLATE 'latin1_swedish_ci',
	`status` VARCHAR(9) NULL COLLATE 'utf8mb4_general_ci'
) ENGINE=MyISAM;


-- Dumping structure for view mku_system.view_inventory
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view_inventory`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_inventory` AS SELECT m.material_name, m.material_unit,i.inventory_quantity,
(SELECT sum(tm_quantity) FROM tbl_task_material
 WHERE material_id = m.material_id) as sub, m.material_id,(i.inventory_quantity -
(SELECT sum(tm_quantity) FROM tbl_task_material
 WHERE material_id = m.material_id)) as total
FROM tbl_inventory as i
INNER JOIN
tbl_material as m
ON 
	i.material_id = m.material_id ;


-- Dumping structure for view mku_system.view_project
-- Removing temporary table and create final VIEW structure
DROP TABLE IF EXISTS `view_project`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` VIEW `view_project` AS SELECT p.project_id, p.project_name, p.project_location,
						p.project_start_date, p.project_end_date,
						CONCAT(u1.user_firstname,' ',u1.user_lastname) as engineer_name, 
						CONCAT(u2.user_firstname,' ',u2.user_lastname) as client_name, 
						CASE 
							WHEN p.status = 0	THEN 'Approved'
							WHEN p.status = 1 THEN 'Completed'
							WHEN p.status = 2 THEN 'On going'
						END as status
				FROM tbl_project as p
				INNER JOIN tbl_project_engineer as pe
				ON
					p.project_id = pe.project_id
				INNER JOIN tbl_project_client as pc
				ON
					p.project_id = pc.project_id
				INNER JOIN tbl_user as u1
				ON
					pe.user_id = u1.user_id
				INNER JOIN tbl_user as u2
				ON
					pc.user_id = u2.user_id
				GROUP BY p.project_id ;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
