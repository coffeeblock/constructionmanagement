<?php

include_once 'dompdf/dompdf_config.inc.php';
require_once 'connect.php';


/*	
Hi laine,

you need to create folder named "pdf_file" same directory

use MYSQLI not MYSQL
EX.

$sql = 'SELECT * FROM table'
$run = mysqli_query($con , $sql); $con is from connect.php file declared variable

search also for other MYSQLI function if needed
please PING me for further questions.

	Truly Yours
	KIER :D
*/

$id = $_GET['id'];

$sql = "SELECT project_name FROM tbl_project
				WHERE project_id = '$id'";
$run = mysqli_query($con , $sql);

$row = mysqli_fetch_assoc($run);
$name = $row['project_name'];

$sql = "SELECT * FROM tbl_task
				WHERE project_id = '".$id."'
				AND status = 1";
$run = mysqli_query($con , $sql);

$data = '';

while($row = mysqli_fetch_assoc($run))
{
	$data .= '<tr>
				<td width="35%">'.$row['task_description'].'</td>
				<td>';

	$materials_sql = "SELECT 
				m.material_id, m.material_name, m.material_unit,
				tm.tm_quantity, tm.tm_total,tm.tm_id,tm.task_id,
				m.material_cost
				FROM tbl_material as m
				INNER JOIN
					tbl_task_material as tm
				ON
					m.material_id = tm.material_id
				WHERE
					tm.task_id = '".$row['task_id']."'
				AND
					tm.status = 1
				AND 
					(tm.tm_quantity != 0
					OR
					tm.tm_total !=0)";

	$run1 = mysqli_query($con , $materials_sql);

	$data .= '
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Materials</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Quantity</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Unit</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Price</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Amount</b></div><br>';

	while($materials = mysqli_fetch_assoc($run1))
	{
		$data .= '
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;">'.$materials['material_name'].'</div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;">'.$materials['tm_quantity'].'</div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;">'.$materials['material_unit'].'</div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;">'.$materials['material_cost'].'</div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;">'.$materials['tm_total'].'</div><br>
					';
	}

	$data .= '
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Equipments</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Quantity</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b># of Days</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Rate/Day</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Amount</b></div><br>';

	$equipments_sql = "SELECT 
					e.equipment_id,e.equipment_name, te.te_quantity,
					te.te_days, te.te_id, te.te_total,te.te_id, te.task_id,
					e.equipment_rate_day
					FROM tbl_equipment as e
					INNER JOIN
						tbl_task_equipment as te
					ON
						e.equipment_id = te.equipment_id
					WHERE
						te.task_id = '".$row['task_id']."'
					AND
						te.status = 1
					AND
						(te.te_quantity != 0
						OR
						te.te_total != 0)";

	$run2 = mysqli_query($con , $equipments_sql);

	while($equipments = mysqli_fetch_assoc($run2))
	{
		$data .= '
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;">'.$equipments['equipment_name'].'</div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;">'.$equipments['te_quantity'].'</div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;">'.$equipments['te_days'].'</div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;">'.$equipments['equipment_rate_day'].'</div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;">'.$equipments['te_total'].'</div><br>
					';
	}

	$data .= '
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Manpower</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Quantity</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b># of Days</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Rate/Day</b></div>
						<div style="display:inline-block;width:65px;padding:0px;text-align:left;"><b>Amount</b></div><br>';

	$manpower_sql = "SELECT 
				m.manpower_id,m.manpower_name, tmp.tmp_days, m.manpower_rate_day,
				tmp.tmp_quantity, tmp.tmp_total, tmp.tmp_id, tmp.task_id
				FROM tbl_manpower as m
				INNER JOIN
					tbl_task_manpower as tmp
				ON
					m.manpower_id = tmp.manpower_id
				WHERE
					tmp.task_id = '".$row['task_id']."'
				AND
					tmp.status = 1
				AND
					(tmp.tmp_quantity != 0
					OR
					tmp.tmp_total != 0)";

	$run3 = mysqli_query($con , $manpower_sql);

	while($manpower = mysqli_fetch_assoc($run3))
	{
		$data .= '
						<div style="display:inline-block;width:70px;padding:0px;text-align:left;">'.$manpower['manpower_name'].'</div>
						<div style="display:inline-block;width:70px;padding:0px;text-align:left;">'.$manpower['tmp_quantity'].'</div>
						<div style="display:inline-block;width:70px;padding:0px;text-align:left;">'.$manpower['tmp_days'].'</div>
						<div style="display:inline-block;width:70px;padding:0px;text-align:left;">'.$manpower['manpower_rate_day'].'</div>
						<span style="display:inline-block;width:30px;padding:0px;text-align:left;">'.$manpower['tmp_total'].'</span>
					';
	}


	$data .= '</td></tr>';
}







#WRITE YOUR HTML HERE
$html ='<html>
		<head>
		<link rel="stylesheet" type="text/css" href="css/pdf.css"/>
		<style>
			#print_table tbody td{
				border: 1px solid #222;
				margin: 0px;
			}

			.break::before {
			  content: "\A";
			}

			.break::before {
			  content: "\A";
			  white-space: pre;
			}
		</style>
		</head>
		<body style="padding:3%;">
			<h1 style="color:#af0303;"><i>MKU Construction</i></h1>
			<div style="width:100%;text-align:right;">'.date("Y/m/d").'</div>
			<hr>
			<span><b>'.$name.' | ESTIMATES</b></span>
			<table cellspacing="-1" id="print_table" style="width:100%;margin-top:10px;">
				<thead style="background-color: #af0303;color:#fff;">
					<tr>
						<td>Task</td>
						<td>Sub-cost</td>
					</tr>
				</thead>
				<tbody>
					'.$data.'
				<tbody>
			</table>
		</body>
		</html>
		';
		

		//echo $html;
		//die();
$file_to_save = 'pdf_file/test.pdf';
$p = new DOMPDF();
$p->load_html($html);

/*
You can also set your customize bond paper size
@: dompdf/include/cpdf_adapter.cls.php
	starting line 67
*/
$p->set_paper('legal', 'portrait'); // BONDPAPER FORMAT
$p->render();
file_put_contents($file_to_save, $p->output());


header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="file.pdf"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file_to_save));
header('Accept-Ranges: bytes');
readfile($file_to_save);


?>