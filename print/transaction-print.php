<?php

include_once 'dompdf/dompdf_config.inc.php';
require_once 'connect.php';
extract($_REQUEST);

$sql = "SELECT *
                FROM
	               tbl_transaction as t
	             INNER JOIN
	             	view_transaction as vt
	            ON
	            	t.sef_id = vt.sef_id
				INNER JOIN
	               tbl_transaction_header as th
	            ON
	            	th.th_id = t.th_id
				INNER JOIN
	            	view_transaction_summary vts
	            ON
	            	vts.sef_id = t.sef_id
              WHERE
                t.th_id = '".$th_id."'";
$run = mysqli_query($con , $sql);

$transaction_list = "";
$sei_id = 0; 
$date = "";
while($row_t = mysqli_fetch_assoc($run))
{
	$sei_id = $row_t['sei_id'];
	$date = date('F d,Y',strtotime($row_t['th_date']));
	
	$transaction_list .= "
		<tr>
			<td>".$row_t['fee_description']."</td>
			<td>".$row_t['transaction_amount']."</td>
			<td>".$row_t['sef_amount']."</td>
			<td>".$row_t['paid']."</td>
			<td>".$row_t['remaining']."</td>
		</tr>
	";
}


//echo "<table>$transaction_list</table>";
//die();
$sql_sei = "SELECT * FROM tbl_student_enrolment_info WHERE sei_id = '$sei_id' ";
$run_sei = mysqli_query($con,$sql_sei);
$row_sei = mysqli_fetch_assoc($run_sei);

$student_id_number = $row_sei['student_id_number'];

$sql_student = "SELECT * FROM tbl_student WHERE student_id_number = '$student_id_number' ";
$run_student = mysqli_query($con,$sql_student);
$row_student = mysqli_fetch_assoc($run_student);


//DATA
$transaction_number = date("ymd",strtotime($row['th_date'])).str_pad($row['th_id'],6,"0",STR_PAD_LEFT);

$name = $row_student['student_lastname'].', '.$row_student['student_firstname'];
//WRITE YOUR HTML HERE
//
/*
$sql = "SELECT * FROM tbl_student";
$run = mysqli_query($con , $sql);

while($row = mysqli_fetch_assoc($run))
{
	echo $row['student_firstname']."<br>";
}
*/
//die();
$html ='<html>
		<head>
		<link rel="stylesheet" type="text/css" href="css/pdf.css"/>
		<style>
			
		</style>
		</head>
		<body>
			<div style="padding:5px;margin-left:5px;" >
				<h3>RECEIPT</h3>
				<hr>
				<table style="margin-top:0px;font-size:8px;" width="100%">
					<tr>
						<td>
							<b>Transaction No. : </b>
							'.$transaction_number.'
						</td>
						<td>
							<b>Date : </b>
						    '.$date.'
						</td>
					</tr>
					<tr>
						<td>
							<b>ID No.:</b>
							'.$student_id_number.'
						</td>
						<td>
						</td>
					</tr>
					<tr>
						<td>
							<b>Name : </b>
							'.$name.'
						</td>
					</tr>
				</table>
				<hr>
				<table style="width:100%;font-size:8px;">
					<tr>
						<td><b>Fees</b></td>
						<td>T-Amount</td>
						<td><b>Total</b></td>
						<td><b>Paid</b></td>
						<td><b>Remaining</b></td>
					</tr>
					'.$transaction_list.'
				</table>
			</div>
		</body>
		</html>
		';
		

$file_to_save = 'pdf_file/test.pdf';
$p = new DOMPDF();
$p->load_html($html);
$p->set_paper('kier', 'landscape'); // BONDPAPER FORMAT
$p->render();
file_put_contents($file_to_save, $p->output());

header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="file.pdf"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file_to_save));
header('Accept-Ranges: bytes');
readfile($file_to_save);
?>