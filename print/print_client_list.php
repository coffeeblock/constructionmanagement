<?php

include_once 'dompdf/dompdf_config.inc.php';
require_once 'connect.php';


/*	
Hi laine,

you need to create folder named "pdf_file" same directory

use MYSQLI not MYSQL
EX.

$sql = 'SELECT * FROM table'
$run = mysqli_query($con , $sql); $con is from connect.php file declared variable

search also for other MYSQLI function if needed
please PING me for further questions.

	Truly Yours
	KIER :D
*/

$sql = 'SELECT * FROM tbl_user WHERE user_type = 3';
$run = mysqli_query($con , $sql);

$data = '';

while($row = mysqli_fetch_assoc($run))
{
	$data .= '<tr>
				<td>'.$row['user_firstname'].'</td>
				<td>'.$row['user_lastname'].'</td>
				<td>'.$row['user_middlename'].'</td>
				<td>'.$row['user_gender'].'</td>
				<td>'.$row['user_age'].'</td>
				<td>'.$row['user_contact_number'].'</td>
				<td>'.$row['user_email_address'].'</td>
				<td>'.$row['user_address'].'</td>
			</tr>';
}




#WRITE YOUR HTML HERE
$html ='<html>
		<head>
		<link rel="stylesheet" type="text/css" href="css/pdf.css"/>
		<style>
			#print_table tbody td{
				border: 1px solid #222;
				margin: 0px;
			}
		</style>
		</head>
		<body style="padding:3%;">
			<h1 style="color:#af0303;"><i>MKU Construction</i></h1>
			<div style="width:100%;text-align:right;">'.date("Y/m/d").'</div>
			<hr>
			<span><b>CLIENTS</b></span>
			<table cellspacing="-1" id="print_table" style="width:100%;margin-top:10px;">
				<thead style="background-color: #af0303;color:#fff;">
					<tr>
						<td>Firstname</td>
						<td>Lastname</td>
						<td>Middlename</td>
						<td>Gender</td>
						<td>Age</td>
						<td>Contact number</td>
						<td>Email</td>
						<td>Address</td>
					</tr>
				</thead>
				<tbody>
					'.$data.'
				<tbody>
			</table>
		</body>
		</html>
		';
		

		//echo $html;
		//die();
$file_to_save = 'pdf_file/test.pdf';
$p = new DOMPDF();
$p->load_html($html);

/*
You can also set your customize bond paper size
@: dompdf/include/cpdf_adapter.cls.php
	starting line 67
*/
$p->set_paper('legal', 'landscape'); // BONDPAPER FORMAT
$p->render();
file_put_contents($file_to_save, $p->output());


header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="file.pdf"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file_to_save));
header('Accept-Ranges: bytes');
readfile($file_to_save);


?>