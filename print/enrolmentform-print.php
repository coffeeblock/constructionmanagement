<?php

include_once 'dompdf/dompdf_config.inc.php';
include 'Enum.php';
require_once 'connect.php';


$sei_id = $_GET['sei_id'];

$sql = "SELECT
						*
				FROM
						view_sei as sei
				INNER JOIN
						tbl_class as c
				ON
						sei.class_id = c.class_id
				WHERE
						sei.sei_id = ".$sei_id." ";

$query = mysqli_query($con,$sql);

while($row = mysqli_fetch_assoc($query))
{
	$student_id_number = $row['student_id_number'];
	$date_enrolled = $row['sei_date_enrolled'];
	$year_level = $row['year_level_description'];
	$school_year = $row['school_year_description'];
	$class_section = $row['class_description'];
	$category = $row['sei_category'];
	$payment_method = $row['payment_type_id'];
	$payment_mode = $row['payment_mode_id'];
	$discount = $row['sei_discount'];
}

switch($category)
{
	case 1:
		$c = "Regular";
		break;
	case 2:
		$c = "Scholar";
		break;
	case 3:
		$c = "Varsity";
		break;
}

$category = $c;

switch($payment_method)
{
	case 1:
		$me = "Cash";
		break;
	case 2:
		$me = "Credit Card";
		break;
	case 3:
		$me = "PayPal";
		break;
	case 4:
		$me = "Cheque";
		break;
	case 5:
		$me = "Bank Transfer";
		break;
}
$payment_method = $me;

switch($payment_mode)
{
	case 1:
		$mo = "Installment";
		break;
	case 2:
		$mo = "Full Payment";
		break;
}

$payment_mode = $mo;

$sql = "SELECT *
						FROM
						 tbl_sef as sef
						 INNER JOIN
						 view_fee as vf
						 ON
						 	sef.fsy_id = vf.fsy_id
						WHERE
							sef.sei_id = ".$sei_id." ";

$fees = "";
$subtotal="";
$tota="";
$query = mysqli_query($con,$sql);
while($row = mysqli_fetch_assoc($query))
{
	$fd = $row['fee_description'];
	$fa = $row['sef_amount'];

	$fees .= '<table width="50%;">
							<tr>
								<td>
									<b>'.$fd.'</b>
								</td>
								<td style="text-align:right;">
									'.$fa.'
								</td>
							</tr>
						</table>';

	$t = (int)$fa;
	$subtotal += $t;

}
$discounted = ($discount/100);
$total = $subtotal * $discounted;



//WRITE YOUR HTML HERE
//

$html ='<html>
		<head>
		<link rel="stylesheet" type="text/css" href="css/pdf.css"/>
		</head>
		<body>
			<div style="padding:50px;margin-left:30px;" >
				<h2>ENROLLMENT FORM</h2>
				<hr>
				<table style="margin-top:30px;" width="100%">
					<tr>
						<td>
							<b>Student ID Number: </b>
							'.$student_id_number.'
						</td>
						<td>
							<b>Year Level: </b>
							'.$year_level.'
						</td>
					</tr>
					<tr>
						<td>
							<b>Date Enrolled</b>
							'.$date_enrolled.'
						</td>
						<td>
							<b>School Year: </b>
							'.$school_year.'
						</td>
					</tr>
					<tr>
						<td>
							<b>Category: </b>
							'.$category.'
						</td>
						<td>
							<b>Class Section: </b>
							'.$class_section.'
						</td>
					</tr>
					<tr>
						<td style="padding-top:70px;padding-bottom:20px;text-align:left;"	>
							<b>FEES</b>
						</td>
						<td style="padding-top:70px;padding-bottom:20px;text-align:left;"	>
							<b>PAYMENTS</b>
						</td>
					</tr>
					<tr>
						<td>	'.$fees.'</td>
						<td>
							<table width="70%">
								<tr>
									<td width="100px"><b>Payment Method: </b></td>
									<td style="text-align:left;width:100px;">'.$payment_method.'	</td>
								</tr>
								<tr>
									<td><b>Payment Mode: </b></td>
									<td style="text-align:left;">'.$payment_mode.'</td>
								</tr>
								<tr>
									<td><b>Discount: </b></td>
									<td>'.$discount.'%</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<table style="margin-top:50px;" width="45%">
				<tr>
						<td><b>Subtotal: </b></td>
						<td>'.$subtotal.'</td>
				</tr>
				<tr>
						<td><b>Discount</b></td>
						<td>'.$discounted.'</td>
				</tr>
				<tr>
						<td>
						<b>Total:</b>
						</td>
						<td style="text-align:left;">'.$total.'</td>
				</tr>
				</table>
			</div>
		</body>
		</html>
		';


$file_to_save = 'pdf_file/test.pdf';
$p = new DOMPDF();
$p->load_html($html);
$p->set_paper('letter', 'landscape'); // BONDPAPER FORMAT
$p->render();
file_put_contents($file_to_save, $p->output());


header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="file.pdf"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file_to_save));
header('Accept-Ranges: bytes');
readfile($file_to_save);


?>
