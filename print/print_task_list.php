<?php

include_once 'dompdf/dompdf_config.inc.php';
require_once 'connect.php';


/*	
Hi laine,

you need to create folder named "pdf_file" same directory

use MYSQLI not MYSQL
EX.

$sql = 'SELECT * FROM table'
$run = mysqli_query($con , $sql); $con is from connect.php file declared variable

search also for other MYSQLI function if needed
please PING me for further questions.

	Truly Yours
	KIER :D
*/
$id = $_GET['id'];

$sql = "SELECT * FROM tbl_task
				WHERE project_id = '$id'
				AND (status = 0 OR status = 2 OR status = 3)";
$run = mysqli_query($con , $sql);

$data = '';

while($row = mysqli_fetch_assoc($run))
{
	$data .= '<tr>
				<td>'.$row['task_description'].'</td>
				<td>'.$row['task_start_date'].'</td>
				<td>'.$row['task_end_date'].'</td>
			</tr>';
}

$sql = "SELECT project_name FROM tbl_project
				WHERE project_id = '$id'";
$run = mysqli_query($con , $sql);

$row = mysqli_fetch_assoc($run);
$name = $row['project_name'];


#WRITE YOUR HTML HERE
$html ='<html>
		<head>
		<link rel="stylesheet" type="text/css" href="css/pdf.css"/>
		<style>
			#print_table tbody td{
				border: 1px solid #222;
				margin: 0px;
			}
		</style>
		</head>
		<body style="padding:3%;">
			<h1 style="color:#af0303;"><i>MKU Construction</i></h1>
			<div style="width:100%;text-align:right;">'.date("Y/m/d").'</div>
			<hr>
			<span><b>PROJECT NAME: '.$name.'</b></span>
			<table cellspacing="-1" id="print_table" style="width:100%;margin-top:10px;">
				<thead style="background-color: #af0303;color:#fff;">
					<tr>
						<td>Name</td>
						<td>Start date</td>
						<td>End date</td>
					</tr>
				</thead>
				<tbody>
					'.$data.'
				<tbody>
			</table>
		</body>
		</html>
		';
		

		//echo $html;
		//die();
$file_to_save = 'pdf_file/test.pdf';
$p = new DOMPDF();
$p->load_html($html);

/*
You can also set your customize bond paper size
@: dompdf/include/cpdf_adapter.cls.php
	starting line 67
*/
$p->set_paper('legal', 'landscape'); // BONDPAPER FORMAT
$p->render();
file_put_contents($file_to_save, $p->output());


header('Content-type: application/pdf');
header('Content-Disposition: inline; filename="file.pdf"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file_to_save));
header('Accept-Ranges: bytes');
readfile($file_to_save);


?>