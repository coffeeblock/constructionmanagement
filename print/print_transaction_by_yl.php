<?php
extract($_GET);
include 'pdf_transaction_yl.php';
require_once 'connect.php';


class TransactionYL
{
	public $year_level_description;
	public $amount;
}

 $start = "1970-01-01";
 if(isset($start_date) && !empty($start_date))
 {
	 $start = $start_date;
 }
 
 $end = date('Y-m-d');
 if(isset($end_date) && !empty($end_date))
 {
	 $end = $end_date;
 }

$sql = "SELECT 
	yl.*,
	IFNULL((
		SELECT SUM(vtr.amount) 
			FROM view_transaction_report vtr
		WHERE
			vtr.year_level_id = yl.year_level_id
		AND
			(vtr.th_date BETWEEN $start AND $end)
	),0) as amount
FROM
	tbl_year_level yl";
	
	
$run = mysqli_query($con , $sql);
$data = array();

while($row = mysqli_fetch_assoc($run))
{
	$transaction = new TransactionYL();
	$transaction->year_level_description = $row['year_level_description'];
	$transaction->amount = $row['amount'];
		
	array_push($data,$transaction);
}

$pdf = new PDFTransactionYL('P','in','Legal');
		$pdf->dt = "";
			$pdf->payrollName = "";
			$pdf->total = "";
			$pdf->start_date = $start;
			$pdf->end_date = $end;
			$pdf->AddPage();
			$pdf->Table($data);
			$pdf->Output();
?>
