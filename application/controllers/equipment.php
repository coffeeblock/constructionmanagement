<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipment extends CI_Controller {
	private $model;
	private $model_name = "EquipmentData";
	private $view_name = "equipment";

	function __construct()
	{
		parent::__construct();

		//SET MODEL
		$this->load->model($this->model_name);
		$this->model = $this->EquipmentData;

		if ( ! $this->session->userdata('loggedin'))
		{
			redirect(base_url());
		}
	}

	function index()
	{
		$this->load->view($this->view_name);
	}

	function Initialize()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;

		$data = $model->GetDataById($id);
		$result->data = $data;

		echo json_encode($result);

	}

	function InitializeLog()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;

		$data = $model->GetLogDataById($id);
		$result->data = $data;

		echo json_encode($result);

	}

	function Edit()
	{
		$result = new DataHandler();
		$model = $this->model;
		$data = $model->ProcessFields($_POST);

		if($model->Update($data))
		{
			$result->error = false;
			$result->message = "Successfully updated!";
		}
		else
		{
			$result->error = true;
			$result->message = "Update failed!";
		}

		echo json_encode($result);

	}

	function Remove()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->Delete($id))
		{
			$result->error = false;
			$result->message = "Successfully deleted!";
		}
		else
		{
			$result->error = true;
			$result->error = "Deletion failed!";
		}

		echo json_encode($result);
	}

	function RemoveLog()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->DeleteLog($id))
		{
			$result->error = false;
			$result->message = "Successfully deleted!";
		}
		else
		{
			$result->error = true;
			$result->error = "Deletion failed!";
		}

		echo json_encode($result);
	}

	function Save()
	{
		$model = $this->model;
		$result = new DataHandler();

		$data = $model->ProcessFields($_POST);

		if($model->Add($data))
		{
			$result->error = false;
			$result->message = "A new Equipment is successfully added!";
			$result->tag = $model->return_id;
		}
		else
		{
			$result->error = false;
			$result->message = "Failed!";
		}

		echo json_encode($result);
	}

	function SaveLogs()
	{
		$model = $this->model;
		$result = new DataHandler();

		$data = $model->ProcessFieldsLogs($_POST);

		if($data['el_id'] == "")
		{
			if($model->AddLog($data))
			{
				$result->error = false;
				$result->message = "A new Equipment log is successfully added!";
				$result->tag = $model->return_id;
			}
			else
			{
				$result->error = false;
				$result->message = "Failed!";
			}
		}
		else
		{
			if($model->UpdateLog($data))
			{
				$result->error = false;
				$result->message = "Successfully Updated!";
			}
			else
			{
				$result->error = false;
				$result->message = "Failed!";
			}
		}

		echo json_encode($result);
	}

	function ListView()
	{
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAll();
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function ListByProject()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAllByProject($id);
		$data = array();

		$default = array("key" => "", "value" => "--- Select Equipment ---");
		array_push($data, $default);

		foreach($row['rows'] as $key)
		{
			$r = array();
			$r["key"] = $key->equipment_id;
			$r["value"] = $key->equipment_name;

			array_push($data, $r);
		}

		$result->data = $data;
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function ListViewLogs()
	{
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAllLogs();
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function ListEquipmentDue()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAllEquipmentDue($id);
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function OptionList()
	{
		$result = new DataHandler();
		$model = $this->model;

		$row = $model->GetAll();
		$data = array();

		$default = array("key" => "", "value" => "--- Select Equipment ---");
		array_push($data, $default);

		foreach($row['rows'] as $key)
		{
			$r = array();
			$r["key"] = $key->equipment_id;
			$r["value"] = $key->equipment_name;

			array_push($data, $r);
		}

		$result->data = $data;
		$result->tag = $row['total'];

		echo json_encode($result);
	}
}

?>