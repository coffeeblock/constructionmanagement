<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Material extends CI_Controller {
	private $model;
	private $model_name = "MaterialData";
	private $view_name = "material";

	function __construct()
	{
		parent::__construct();

		//SET MODEL
		$this->load->model($this->model_name);
		$this->model = $this->MaterialData;

		if ( ! $this->session->userdata('loggedin'))
		{
			redirect(base_url());
		}
	}

	function index()
	{
		$this->load->view($this->view_name);
	}

	function Initialize()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;

		$data = $model->GetDataById($id);
		$result->data = $data;

		echo json_encode($result);

	}

	function Edit()
	{
		$result = new DataHandler();
		$model = $this->model;
		$data = $model->ProcessFields($_POST);

		if($model->Update($data))
		{
			$result->error = false;
			$result->message = "Successfully updated!";
		}
		else
		{
			$result->error = true;
			$result->message = "Update failed!";
		}

		echo json_encode($result);

	}

	function Remove()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->Delete($id))
		{
			$result->error = false;
			$result->message = "Successfully deleted!";
		}
		else
		{
			$result->error = true;
			$result->error = "Deletion failed!";
		}

		echo json_encode($result);
	}

	function RemoveReturned()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->DeleteReturned($id, $quantity))
		{
			$result->error = false;
			$result->message = "Successfully deleted!";
		}
		else
		{
			$result->error = true;
			$result->error = "Deletion failed!";
		}

		echo json_encode($result);
	}

	function Save()
	{
		$model = $this->model;
		$result = new DataHandler();

		$data = $model->ProcessFields($_POST);

		if($model->Add($data))
		{
			$result->error = false;
			$result->message = "A new Material is successfully added!";
			$result->tag = $model->return_id;
		}
		else
		{
			$result->error = false;
			$result->message = "Failed!";
		}

		echo json_encode($result);
	}

	function SaveReturnedItems()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->AddReturnedItems($quantity, $material_id, $task_id))
		{
			$result->error = false;
			$result->message = "Successfully added!";
			$result->tag = $model->return_id;
		}
		else
		{
			$result->error = false;
			$result->message = "Failed!";
		}

		echo json_encode($result);
	}

	function ListView()
	{
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAll();
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function ListViewReturnedItems()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAllReturnedItems($id);
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function ListViewByTask()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAllByTask($id);
		$data = array();

		$default = array("key" => "", "value" => "--- Select Material ---");
		array_push($data, $default);

		foreach($row['rows'] as $key)
		{
			$r = array();
			$r["key"] = $key->material_id;
			$r["value"] = $key->material_name;

			array_push($data, $r);
		}

		$result->data = $data;
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function OptionList()
	{
		$result = new DataHandler();
		$model = $this->model;

		$row = $model->GetAll();
		$data = array();

		$default = array("key" => "", "value" => "--- Select Material ---");
		array_push($data, $default);

		foreach($row['rows'] as $key)
		{
			$r = array();
			$r["key"] = $key->material_id;
			$r["value"] = $key->material_name;

			array_push($data, $r);
		}

		$result->data = $data;
		$result->tag = $row['total'];

		echo json_encode($result);
	}
}

?>