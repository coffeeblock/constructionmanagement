<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventory extends CI_Controller {
	private $model;
	private $model_name = "InventoryData";
	private $view_name = "inventory";

	function __construct()
	{
		parent::__construct();

		//SET MODEL
		$this->load->model($this->model_name);
		$this->model = $this->InventoryData;

		if ( ! $this->session->userdata('loggedin'))
		{
			redirect(base_url());
		}
	}

	function index()
	{
		$this->load->view($this->view_name);
	}

	function Initialize()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;

		$data = $model->GetDataById($id);
		mysqli_next_result( $this->db->conn_id );
		$estimate = $model->GetEstimateItems($id);
		$result->data = $data;
		$result->items = $estimate['rows'];

		echo json_encode($result);

	}

	function TotalCost()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;


		$row = $model->GetTotalCostById($id, $status);
		$result->data = $row['rows'];

		echo json_encode($result);
	}

	function Edit()
	{
		$result = new DataHandler();
		$model = $this->model;
		$engineer_id = $_POST['project_engineer_id'];
		$client_id = $_POST['project_client_id'];

		unset($_POST['project_engineer_id']);
		unset($_POST['project_client_id']);

		$data = $model->ProcessFields($_POST);

		//$data['user_password_hash'] = md5($data['user_password_text']);
		if($model->Update($data, $engineer_id, $client_id))
		{
			$result->error = false;
			$result->message = "Successfully updated!";
		}
		else
		{
			$result->error = true;
			$result->message = "Update failed!";
		}

		$length = $_POST['project_dim_length'];
		$height = $_POST['project_dim_height'];
		$width = $_POST['project_dim_width'];
		extract($_POST);
		
		if(isset($excavation))
			$ex = $this->Excavation($excavation, $length, $height, $width, $project_id, 1);
		if(isset($embankment))
			$em = $this->Embankment($embankment, $length, $height, $width, $project_id, 1);
		if(isset($sub_grade))
			$subg = $this->SubGrade($sub_grade, $length, $height, $width, $project_id, 1);
		if(isset($sub_base))
			$subb = $this->SubBase($sub_base, $length, $height, $width, $project_id, 1);
		if(isset($base_course))
			$base = $this->BaseCourse($base_course, $length, $height, $width, $project_id, 1);
		//$this->Pccp($length, $height, $width, $project_id, 1);

		$result->excavation = @$ex;
		$result->embankment = @$em;
		$result->subgrade = @$subg;
		$result->subbase = @$subb;
		$result->basecourse = @$base;

		echo json_encode($result);

	}

	function Remove()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->Delete($id))
		{
			$result->error = false;
			$result->message = "Successfully deleted!";
		}
		else
		{
			$result->error = true;
			$result->error = "Deletion failed!";
		}

		echo json_encode($result);
	}



	function Save()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->Add($material_id, $inventory_quantity))
		{
			$result->error = false;
			$result->message = "Successfully added!";
			$result->tag = $model->return_id;
		}
		else
		{
			$result->error = false;
			$result->message = "Failed!";
		}

		echo json_encode($result);
	}


	function ListView()
	{
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAll();
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);
	}


	/* ListTaskById()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$data = $model->GetProjectTasks($id);
		$result->data = $data['rows'];
		$result->tag = $data['total'];

		echo json_encode($result);
	}*/

	function ListById()
	{
		$model = $this->model;
		$result = new DataHandler();
		$id = $this->session->userdata('id');
		$result = new DataHandler();

		$data = $model->GetByUserId($id);
		$result->data = $data['rows'];
		$result->tag = $data['total'];

		echo json_encode($result);
	}
}

?>