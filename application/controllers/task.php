<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class Task extends CI_Controller {
	private $model;
	private $model_name = "TaskData";
	private $view_name = "task";

	function __construct()
	{
		parent::__construct();

		//SET MODEL
		$this->load->model($this->model_name);
		$this->model = $this->TaskData;

		/*if ( ! $this->session->userdata('loggedin'))
		{
			redirect(base_url());
		}*/
	}

	function index()
	{
		$this->load->view($this->view_name);
	}

	function estimate()
	{
		$this->load->view('estimates');
	}

	function SaveTaskMaterials()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;
		$batch = array();

		$arr = explode(":", $data);
		foreach($arr as $key)
		{
			$r = explode(',',$key);
			foreach($r as $k)
			{
				$f = explode('=',$k);
					$p = array();
					@$g[$f[0]] = $f[1];
			}
			array_push($batch, $g);
		}
		$row = $model->AddTaskMaterials($batch);

		$result->data = $row;
		echo json_encode($result);
	}


	function Edit()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->Update($task_id, $task_name, $task_start_date, 
									$task_end_date, 
									$strMat, $strEquip, $strMan))
		{
			$result->error = false;
			$result->message = "A new task is successfully updated!";
			$result->tag = $model->return_id;
		}
		else
		{
			$result->error = false;
			$result->message = "Insufficient Supply!";
		}

		echo json_encode($result);

	}

	function Done()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->SetTaskDone($id))
		{
			$result->error = false;
			$result->message = "Task completed!";
		}
		else
		{
			$result->error = false;
			$result->message = "Failed!";
		}

		echo json_encode($result);
	}

	function Start()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->SetTaskStart($id))
		{
			$result->error = false;
			$result->message = "Task started!";
		}
		else
		{
			$result->error = false;
			$result->message = "Failed!";
		}

		echo json_encode($result);
	}

	function Remove()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->Delete($id))
		{
			$result->error = false;
			$result->message = "Successfully deleted!";
		}
		else
		{
			$result->error = true;
			$result->error = "Deletion failed!";
		}

		echo json_encode($result);
	}

	function Save()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

	if($model->DateAvailability($task_start_date, $task_end_date, $project_id))
	{	
		if($model->Add($task_name, $task_start_date, 
									$task_end_date, $project_id, 
									$strMat, $strEquip, $strMan))
		{
			$result->error = false;
			$result->message = "A new task is successfully added!";
			$result->tag = $model->return_id;
		}
		else
		{
			$result->error = true;
			$result->message = "Insufficient Supply!";
		}
	}
	else
	{
		$result->error = true;
		$result->message = "Date is conflict to other task!";
	}

		echo json_encode($result);
	}



	function Initialize()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetTask($id);

		$result->data = $row;

		echo json_encode($result);
	}

	function GetTaskModules()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$materials = $model->GetMaterialByTask($id, $status);
		$equipments = $model->GetEquipmentByTask($id, $status);
		$manpower = $model->GetManpowerByTask($id, $status);
		//$materials = json_decode(json_encode($materials), true);

		$result->materials = $materials['rows'];
		$result->equipments = $equipments['rows'];
		$result->manpower = $manpower['rows'];
		$result->id = $id;

		echo json_encode($result);
	}

	function GetTotalModuleSync()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$materials = $model->GetMaterialByTaskSync($id, $status);
		$equipments = $model->GetEquipmentByTask($id, $status);
		$manpower = $model->GetManpowerByTask($id, $status);
		//$materials = json_decode(json_encode($materials), true);

		$result->materials = $materials['rows'];
		$result->equipments = $equipments['rows'];
		$result->manpower = $manpower['rows'];
		$result->id = $id;

		echo json_encode($result);
	}

	function GeneratedByProject()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$data = $model->GetGenerated($id);
		$result->data = $data['rows'];
		$result->tag = $data['total'];

		echo json_encode($result);
	}

	function ListView()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$data = $model->GetAll($id);
		$result->data = $data['rows'];
		$result->tag = $data['total'];
		$result->project_name = $data['project_name']->project_name;

		echo json_encode($result);
	}

	function ListViewEstimates()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$data = $model->GetAllEstimates($id);
		$result->data = $data['rows'];
		$result->tag = $data['total'];
		$result->project_name = $data['project_name']->project_name;

		echo json_encode($result);
	}

	function ListById()
	{
		$model = $this->model;
		$result = new DataHandler();
		$id = $this->session->userdata('id');
		extract($_POST);
		$result = new DataHandler();

		$data = $model->GetByUserId($id);
		$result->data = $data['rows'];
		$result->tag = $data['total'];

		echo json_encode($result);
	}
}

?>