<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller{
	private $model;
	private $model_name = "UserData";

	function __construct()
	{
		parent::__construct();

		//SET MODEL
		$this->load->model($this->model_name);
		$this->model = $this->UserData;
	}

	function index()
	{
		$this->load->view('profile');
	}

	function loginProcess()
	{
		$username = $_POST['username'];
		$password = $_POST['password'];

		$result = new DataHandler();

		if($this->model->login($username, $password))
		{

			$user_data = array(
			"id" => $this->session->userdata("id"),
			"username" => $this->session->userdata("username"),
			"user_type" => $this->session->userdata("user_type"),
			"firstname" => $this->session->userdata("firstname"),
			"lastname" => $this->session->userdata("lastname"),
			"middlename" => $this->session->userdata("middlename"),
			"gender" => $this->session->userdata("gender"),
			"contact" => $this->session->userdata("number"),
			"email" => $this->session->userdata("email")
			);

			$result->error = false;
			$result->message = "Login successful!";
			$result->data = $user_data;
		}
		else
		{
			$result->error = true;
			$result->message = "Invalid Username or Password.";
		}

		echo json_encode($result);
		//echo json_encode($result);
	}

	function Edit()
	{
		$result = new DataHandler();
		$model = $this->model;
		$data = $model->ProcessFields($_POST);

		$data['user_password_hash'] = md5($data['user_password_text']);
		if($model->Update($data))
		{
			$result->error = false;
			$result->message = "Successfully updated!";
		}
		else
		{
			$result->error = true;
			$result->message = "Update failed!";
		}

		echo json_encode($result);

	}

	function Initialize()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;

		$data = $model->GetDataById($id);
		$result->data = $data;

		echo json_encode($result);

	}


	function GeneratePassword()
	{
		$length = 8;
		$result = new DataHandler();
		$result->error = true;
		$this->load->model("userdata");
		$char = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";

		while($result->error == true)
		{
			$password = substr(str_shuffle($char), 0, $length);

			$data = $this->userdata->CheckPasswordAvailaility($password);

			if($data)
			{
				$result->data = array("password" => $password);
				$result->error = false;
			}
			else
			{
				$result->error = true;
			}

		}

		echo json_encode($result);
	}

	function logout()
	{
		$this->model->unset_session();
		redirect(base_url().'login');
	}

}

?>