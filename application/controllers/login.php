<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	private $view_name = "login";

	function index()
	{
		$this->load->model('userdata');
		$loggedin = $this->userdata->verifyLogin();

		if($loggedin)
		{
			redirect(base_url().'homepage');
		}
		else
			$this->load->view($this->view_name);
	}

	
}

?>