<?php
//defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {
	private $model;
	private $model_name = "ProjectData";
	private $view_name = "project";
	private $duration;

	function __construct()
	{
		parent::__construct();

		//SET MODEL
		$this->load->model($this->model_name);
		$this->model = $this->ProjectData;

		if ( ! $this->session->userdata('loggedin'))
		{
			//redirect(base_url());
		}
	}

	function index()
	{
		$this->load->view($this->view_name);
	}

	/*function SaveTaskMaterials()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;
		$batch = array();

		$arr = explode(":", $data);
		foreach($arr as $key)
		{
			$r = explode(',',$key);
			foreach($r as $k)
			{
				$f = explode('=',$k);
					$p = array();
					@$g[$f[0]] = $f[1];
			}
			array_push($batch, $g);
		}
		$row = $model->AddTaskMaterials($batch);

		$result->data = $row;
		echo json_encode($result);
	}
	*/

	function Initialize()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;

		$data = $model->GetDataById($id);
		mysqli_next_result( $this->db->conn_id );
		$estimate = $model->GetEstimateItems($id);
		$result->data = $data;
		$result->items = $estimate['rows'];

		echo json_encode($result);

	}

	function TotalCost()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;


		$row = $model->GetTotalCostById($id, $status);
		$result->data = $row['rows'];

		echo json_encode($result);
	}

	function Progress()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;
		$done=0;
		$days=0;


		$row = $model->GetTotalCostById($id, 0);
		//$result->data = $row['rows'];
		foreach($row['rows'] as $key)
		{
			if($key->status == 2)
			{
				$done = $done + $key->total_days;
			}
			$days = $days + $key->total_days;
		}

		$total = ($done/$days)*100;
		$result->progress = number_format($total,2);
		echo json_encode($result);
	}

	function Edit()
	{
		$result = new DataHandler();
		$model = $this->model;
		$engineer_id = $_POST['project_engineer_id'];
		$client_id = $_POST['project_client_id'];
		$id = $_POST['project_id'];

		unset($_POST['project_engineer_id']);
		unset($_POST['project_client_id']);

		$data = $model->ProcessFields($_POST);

		//$data['user_password_hash'] = md5($data['user_password_text']);
		if($model->Update($data, $engineer_id, $client_id))
		{
			$duration = $model->UpdateProjectDuration($id);
			$result->error = false;
			$result->message = "Successfully updated!";
		}
		else
		{
			$result->error = true;
			$result->message = "Update failed!";
		}

		$length = $_POST['project_dim_length'];
		$height = $_POST['project_dim_height'];
		$width = $_POST['project_dim_width'];
		extract($_POST);
		
		if(isset($excavation))
			$ex = $this->Excavation($excavation, $length, $height, $width, $project_id, 1);
		if(isset($embankment))
			$em = $this->Embankment($embankment, $length, $height, $width, $project_id, 1);
		if(isset($sub_grade))
			$subg = $this->SubGrade($sub_grade, $length, $height, $width, $project_id, 1);
		if(isset($sub_base))
			$subb = $this->SubBase($sub_base, $length, $height, $width, $project_id, 1);
		if(isset($base_course))
			$base = $this->BaseCourse($base_course, $length, $height, $width, $project_id, 1);
		$this->Pccp($length, $height, $width, $project_id, 1);

		$result->excavation = @$ex;
		$result->embankment = @$em;
		$result->subgrade = @$subg;
		$result->subbase = @$subb;
		$result->basecourse = @$base;

		echo json_encode($result);

	}

	function Remove()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->Delete($id))
		{
			$result->error = false;
			$result->message = "Successfully deleted!";
		}
		else
		{
			$result->error = true;
			$result->error = "Deletion failed!";
		}

		echo json_encode($result);
	}

	/*function SaveTask()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->AddProjectTask($task_name, $task_start_date, 
									$task_end_date, $project_id, 
									$strMat, $strEquip, $strMan))
		{
			$result->error = false;
			$result->message = "A new task is successfully added!";
			$result->tag = $model->return_id;
		}
		else
		{
			$result->error = false;
			$result->message = "Failed!";
		}

		echo json_encode($result);
	}*/

	/*function ViewTask()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetTask($id);

		$result->data = $row;

		echo json_encode($result);
	}*/

	/*function GetTaskModules()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$materials = $model->GetMaterialByTask($id);
		$equipments = $model->GetEquipmentByTask($id);
		$manpower = $model->GetManpowerByTask($id);
		//$materials = json_decode(json_encode($materials), true);

		$result->materials = $materials['rows'];
		$result->equipments = $equipments['rows'];
		$result->manpower = $manpower['rows'];

		echo json_encode($result);
	}*/

	function Save()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$engineer_id = $_POST['engineer_id'];
		$client_id = $_POST['client_id'];

		unset($_POST['engineer_id']);
		unset($_POST['client_id']);
		
		$data = $model->ProcessFields($_POST);
		if($model->Add($data, $engineer_id, $client_id))
		{
			$result->error = false;
			$result->message = "A new project is successfully added!";
			$result->tag = $model->return_id;
			$project_id = $model->return_id;
			//Estimated quantity and cost
			$length = $_POST['project_dim_length'];
			$height = $_POST['project_dim_height'];
			$width = $_POST['project_dim_width'];
			$start_date = $_POST['project_start_date'];
			//$end_date = $_POST['project_end_date'];
			
			if(!empty($excavation))
				$this->Excavation($excavation, $length, $height, $width, $project_id, 0);
			if(!empty($embankment))
				$this->Embankment($embankment, $length, $height, $width, $project_id, 0);
			if(!empty($sub_grade))
				$this->SubGrade($sub_grade, $length, $height, $width, $project_id, 0);
			if(!empty($sub_base))
				$this->SubBase($sub_base, $length, $height, $width, $project_id, 0);
			if(!empty($base_course))
				$basecourse = $this->BaseCourse($base_course, $length, $height, $width, $project_id, 0);
			$this->Pccp($length, $height, $width, $project_id, 0);
			$duration = $model->UpdateProjectDuration($project_id);

			//$duration = $model->UpdateProjectDuration($project_id);

			$result->embankment = $embankment;
			$result->subgrade = $sub_grade;
			$result->subbase = $sub_base;
			$result->basecourse = $base_course;
			//$result->duration = $duration;
		}
		else
		{
			$result->error = false;
			$result->message = "Failed!";
		}

		echo json_encode($result);
	}

	function Excavation($excavation, $length, $height, $width, $project_id, $mode)
	{
		$model = $this->model;
		$result = new DataHandler();
		$volume = $excavation;

		$task = array(
			'task_description' => 'Excavation',
			'project_id' => $project_id,
			'status' => 1
			);

		$task_actual = array(
			'task_description' => 'Excavation',
			'project_id' => $project_id,
			'status' => 0
			);

		if($mode == 0)
		{
			$task_id = $model->AddEstimateTask($task);
			//$model->AddEstimateTask($task_actual);
		}
		else
			$task_id = $model->GetTaskId($project_id, 'Excavation');


		//*** Embankment *** #1
		$item102 = $volume*1.15;  //cubic meter

		//equipments no. of days
		$bulldozer = ceil(ceil($item102/60)/8);  //60m3/hr
		$payloader1 = $bulldozer;				 //60m3/hr
		$payloader2 = ceil(ceil($item102/240)/8);//240m3/hr
		$dumptruck = ceil(ceil($item102/30)/8);  //30m3/hr  (2)

		//Fuel in Liters
		$sub_fuel = ($bulldozer*8*15) + ($payloader1*8*10) + ($payloader2*8*10) + ($dumptruck*8*5);
		$fuel = ceil($sub_fuel*1.15);

		//manpower hours
		$foreman = $bulldozer; //1 Foreman
		$operator = $bulldozer; //1 Operator for bulldozer
		$operator1 = $payloader1; //1 Operator for payloader1
		$operator2 = $payloader2; //1 Operator for payloader2
		$driver = $dumptruck; // 2 Driver
		$helper = $bulldozer; //4 Helpers
		//** end excavation

		//insert data
		$materials = array(
			array(
				'tm_quantity' => $item102,
				'material_id' => 15,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $fuel,
				'material_id' => 6,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$equipments = array(
			array(
				'te_days' => $bulldozer,
				'te_quantity' => 1,
				'equipment_id' => 9,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $payloader1,
				'te_quantity' => 1,
				'equipment_id' => 10,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $payloader2,
				'te_quantity' => 1,
				'equipment_id' => 9,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $dumptruck,
				'te_quantity' => 2,
				'equipment_id' => 11,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$manpower = array(
			array(
				'tmp_days' => $foreman,
				'tmp_quantity' => 1,
				'manpower_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $operator,
				'tmp_quantity' => 1,
				'manpower_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $operator1,
				'tmp_quantity' => 1,
				'manpower_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $operator2,
				'tmp_quantity' => 1,
				'manpower_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $driver,
				'tmp_quantity' => 2,
				'manpower_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $helper,
				'tmp_quantity' => 4,
				'manpower_id' => 4,
				'task_id' => $task_id,
				'status' => 1
				)
			);


		if($mode == 0)
			$result = $model->AddEstimateModules($materials, $equipments, $manpower);
		else
			$result = $model->UpdateEstimateModules($task_id,$materials, $equipments, $manpower);
		return $result;
	}

	function Embankment($embankment, $length, $height, $width, $project_id, $mode)
	{
		$model = $this->model;
		$result = new DataHandler();
		$volume = $embankment;

		$task = array(
			'task_description' => 'Embankment',
			'project_id' => $project_id,
			'status' => 1
			);
		$task_actual = array(
			'task_description' => 'Embankment',
			'project_id' => $project_id,
			'status' => 0
			);

		if($mode == 0)
		{
			$task_id = $model->AddEstimateTask($task);
			//$model->AddEstimateTask($task_actual);
		}
		else
			$task_id = $model->GetTaskId($project_id, 'Embankment');

		//*** Embankment *** #1
		$item104 = $volume*1.15;

		//equipments no. of hours
		$grader = ceil(ceil($item104/50)/8);
		$roller = $grader;
		$water_tank = ceil(ceil($item104/200)/8);

		//Fuel in Liters
		$sub_fuel = ($grader*8*10) + ($roller*8*10) + ($water_tank*8*5);
		$fuel = ceil($sub_fuel*1.15);

		//manpower hours
		$foreman = $grader; //1 Foreman
		$operator = $grader; //2 Operator
		$driver = $water_tank; //1 Driver
		$helper = $grader; //4 Helpers
		//** end embankment

		//insert data
		$materials = array(
			array(
				'tm_quantity' => $item104,
				'material_id' => 11,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $fuel,
				'material_id' => 6,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$equipments = array(
			array(
				'te_days' => $grader,
				'te_quantity' => 1,
				'equipment_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $roller,
				'te_quantity' => 1,
				'equipment_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $water_tank,
				'te_quantity' => 1,
				'equipment_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$manpower = array(
			array(
				'tmp_days' => $foreman,
				'tmp_quantity' => 1,
				'manpower_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $operator,
				'tmp_quantity' => 2,
				'manpower_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $driver,
				'tmp_quantity' => 1,
				'manpower_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $helper,
				'tmp_quantity' => 4,
				'manpower_id' => 4,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		if($mode == 0)
			$result = $model->AddEstimateModules($materials, $equipments, $manpower);
		else
			$result = $model->UpdateEstimateModules($task_id, $materials, $equipments, $manpower);
		return $result;
	}

	function SubGrade($sub_grade, $length, $height, $width, $project_id, $mode)
	{
		$model = $this->model;
		$result = new DataHandler();

		$task = array(
			'task_description' => 'Sub-grade',
			'project_id' => $project_id,
			'status' => 1
			);

		$task_actual = array(
			'task_description' => 'Sub-grade',
			'project_id' => $project_id,
			'status' => 0
			);

		if($mode == 0)
		{
			$task_id = $model->AddEstimateTask($task);
			//$model->AddEstimateTask($task_actual);
		}
		else
			$task_id = $model->GetTaskId($project_id, 'Sub-grade');

		//$volume = $length*$height*$width;
		//*** Sub-grade *** #2
		$item105 = $sub_grade;
		//equipments
		$grader = ceil(ceil($item105/300)/8);	//300m2/hr
		$roller = $grader;						//300m2/hr
		$water_tank = ceil(ceil($item105/1200)/8);		//1200m2/hr

		//Fuel in Liters
		$sub_fuel = ($grader*8*10) + ($roller*8*10) + ($water_tank*8*5);
		$fuel = ceil($sub_fuel*1.15);

		//manpower hours
		$foreman = $grader;
		$operator = $grader;
		$driver = $water_tank;
		$helper = $grader;
		//*** end sub-grade


		//insert data
		$materials = array(
			array(
				'tm_quantity' => $item105,
				'material_id' => 12,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $fuel,
				'material_id' => 6,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$equipments = array(
			array(
				'te_days' => $grader,
				'te_quantity' => 1,
				'equipment_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $roller,
				'te_quantity' => 1,
				'equipment_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $water_tank,
				'te_quantity' => 1,
				'equipment_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$manpower = array(
			array(
				'tmp_days' => $foreman,
				'tmp_quantity' => 1,
				'manpower_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $operator,
				'tmp_quantity' => 2,
				'manpower_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $driver,
				'tmp_quantity' => 1,
				'manpower_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $helper,
				'tmp_quantity' => 4,
				'manpower_id' => 4,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		if($mode == 0)
			$result = $model->AddEstimateModules($materials, $equipments, $manpower);
		else
			$result = $model->UpdateEstimateModules($task_id, $materials, $equipments, $manpower);

		return $result;
	}

	function SubBase($sub_base, $length, $height, $width, $project_id, $mode)
	{
		$model = $this->model;
		$result = new DataHandler();

		$task = array(
			'task_description' => 'Sub-base',
			'project_id' => $project_id,
			'status' => 1
			);

		$task_actual = array(
			'task_description' => 'Sub-base',
			'project_id' => $project_id,
			'status' => 0
			);

		if($mode == 0)
		{
			$task_id = $model->AddEstimateTask($task);
			//$model->AddEstimateTask($task_actual);
		}
		else
			$task_id = $model->GetTaskId($project_id, 'Sub-base');

		$volume = $sub_base;
		//*** Sub-base course *** #3
		$item200 = $volume*1.15;

		//equipments
		$grader = ceil(ceil($item200/40)/8);		//40m3/hr
		$roller = $grader;						//40m3/hr
		$water_tank = ceil(ceil($item200/160)/8);			//160m3/hr

		//fuel in liters
		$sub_fuel = ($grader*8*10) + ($roller*8*10) + ($water_tank*8*5);
		$fuel = ceil($sub_fuel*1.15);

		//manpower
		$foreman = $grader;
		$operator = $grader;
		$driver = $water_tank;
		$helper = $grader;
		//*** end sub-base


		//insert data
		$materials = array(
			array(
				'tm_quantity' => $item200,
				'material_id' => 13,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $fuel,
				'material_id' => 6,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$equipments = array(
			array(
				'te_days' => $grader,
				'te_quantity' => 1,
				'equipment_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $roller,
				'te_quantity' => 1,
				'equipment_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $water_tank,
				'te_quantity' => 1,
				'equipment_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$manpower = array(
			array(
				'tmp_days' => $foreman,
				'tmp_quantity' => 1,
				'manpower_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $operator,
				'tmp_quantity' => 2,
				'manpower_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $driver,
				'tmp_quantity' => 1,
				'manpower_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $helper,
				'tmp_quantity' => 4,
				'manpower_id' => 4,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		if($mode == 0)
			$result = $model->AddEstimateModules($materials, $equipments, $manpower);
		else
			$result = $model->UpdateEstimateModules($task_id, $materials, $equipments, $manpower);
		return $result;
	}

	function BaseCourse($base_course, $length, $height, $width, $project_id, $mode)
	{
		$model = $this->model;
		$result = new DataHandler();

		$task = array(
			'task_description' => 'Base course',
			'project_id' => $project_id,
			'status' => 1
			);

		$task_actual = array(
			'task_description' => 'Base course',
			'project_id' => $project_id,
			'status' => 0
			);

		if($mode == 0)
		{
			$task_id = $model->AddEstimateTask($task);
			//$model->AddEstimateTask($task_actual);
		}
		else
			$task_id = $model->GetTaskId($project_id, 'Base course');

		$volume = $base_course;
		//*** Base course *** #4
		$item201 = $volume*1.15;

		//equipments
		$grader = ceil(ceil($item201/50)/8);
		$roller = $grader;
		$water_tank = ceil(ceil($item201/200)/8);

		//fuel in liters
		$sub_fuel = ($grader*8*10) + ($roller*8*10) + ($water_tank*8*5);
		$fuel = ceil($sub_fuel*1.15);

		//manpower
		$foreman = $grader;
		$operator = $grader;
		$driver = $water_tank;
		$helper = $grader;

				//insert data
		$materials = array(
			array(
				'tm_quantity' => $item201,
				'material_id' => 14,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $fuel,
				'material_id' => 6,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$equipments = array(
			array(
				'te_days' => $grader,
				'te_quantity' => 1,
				'equipment_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $roller,
				'te_quantity' => 1,
				'equipment_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $water_tank,
				'te_quantity' => 1,
				'equipment_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$manpower = array(
			array(
				'tmp_days' => $foreman,
				'tmp_quantity' => 1,
				'manpower_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $operator,
				'tmp_quantity' => 2,
				'manpower_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $driver,
				'tmp_quantity' => 1,
				'manpower_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $helper,
				'tmp_quantity' => 4,
				'manpower_id' => 4,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		if($mode == 0)
			$result = $model->AddEstimateModules($materials, $equipments, $manpower);
		else
			$result = $model->UpdateEstimateModules($task_id, $materials, $equipments, $manpower);

		return $result;
	}


	function Pccp($length, $height, $width, $project_id, $mode)
	{
		$model = $this->model;
		$result = new DataHandler();

		$task = array(
			'task_description' => 'PCCP',
			'project_id' => $project_id,
			'status' => 1
			);

		$task_actual = array(
			'task_description' => 'PCCP',
			'project_id' => $project_id,
			'status' => 0
			);

		if($mode == 0)
		{
			$task_id = $model->AddEstimateTask($task);
			//$model->AddEstimateTask($task_actual);

		}
		else
			$task_id = $model->GetTaskId($project_id, 'PCCP');

		//*** PCCP *** #5
		$volume = $length*$height*$width;
		$area = $length*$width;
		$cement = ceil($volume*10);	//bags
		$gravel = ceil($volume);		
		$sand = ceil($volume*0.5);
		//deformed steel bars Pieces @ 0.75 spacing
		$steel_bars_pcs = $length / 0.75;
		// 1 length = 6.0m @market
		// pcs per 1 length of 6.0m
		$steel_bars_pcs_length = 6.0/0.75;
		// quantity of steel bars @6.0m each
		$steel_bars_required = ceil($steel_bars_pcs / $steel_bars_pcs_length);

		//Curing compound 1Lit/15m2
		$curing_compound = ceil($area/15);
		$asphalt = ceil($length/80);

		//Steel Forms
		$steel_forms = ($length/150)*62.5;

		//number of days both equipments and manpower
		$days = ($length*2)/150;

		//equipments
		$backhoe = $days;
		$payloader = $days;
		$transit_mixer = $days;
		$water_tank = $days;
		$concrete_vibrator = $days;
		$concrete_cutter = $days; 
		
		//FUEL
		$diesel = ceil($backhoe*80) + ceil($payloader*80) + ceil($transit_mixer*100) + ceil($water_tank*40);
		$gasoline = ceil($concrete_vibrator*10*2) + ceil($concrete_cutter*10);

		//manpower
		$foreman = $days;
		$operator = $days;
		$driver = $days;
		$mason = $days;
		$helper = $days;

				//insert data
		$materials = array(
			array(
				'tm_quantity' => $cement,
				'material_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $gravel,
				'material_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $sand,
				'material_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $steel_bars_required,
				'material_id' => 4,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $curing_compound,
				'material_id' => 7,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $asphalt,
				'material_id' => 8,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $steel_forms,
				'material_id' => 5,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $diesel,
				'material_id' => 9,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tm_quantity' => $gasoline,
				'material_id' => 10,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$equipments = array(
			array(
				'te_days' => $backhoe,
				'te_quantity' => 1,
				'equipment_id' => 4,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $payloader,
				'te_quantity' => 1,
				'equipment_id' => 5,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $transit_mixer,
				'te_quantity' => 4,
				'equipment_id' => 6,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $water_tank,
				'te_quantity' => 1,
				'equipment_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $concrete_vibrator,
				'te_quantity' => 2,
				'equipment_id' => 7,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'te_days' => $concrete_cutter,
				'te_quantity' => 1,
				'equipment_id' => 8,
				'task_id' => $task_id,
				'status' => 1
				)
			);

		$manpower = array(
			array(
				'tmp_days' => $foreman,
				'tmp_quantity' => 1,
				'manpower_id' => 1,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $operator,
				'tmp_quantity' => 2,
				'manpower_id' => 2,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $driver,
				'tmp_quantity' => 5,
				'manpower_id' => 3,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $mason,
				'tmp_quantity' => 4,
				'manpower_id' => 5,
				'task_id' => $task_id,
				'status' => 1
				),
			array(
				'tmp_days' => $helper,
				'tmp_quantity' => 15,
				'manpower_id' => 4,
				'task_id' => $task_id,
				'status' => 1
				)
			);


		if($mode == 0)
			$result = $model->AddEstimateModules($materials, $equipments, $manpower);
		else
			$result = $model->UpdateEstimateModules($task_id, $materials, $equipments, $manpower);

	}

	function ListView()
	{
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAll();
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function ProjectDropdown()
	{
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAll();
		$data = array();

		$default = array("key" => "", "value" => "--- Select Project ---");
		array_push($data, $default);

		foreach($row['rows'] as $key)
		{
			$r = array();
			$r['key'] = $key->project_id;
			$r['value'] = $key->project_name;

			array_push($data, $r);
		}

		$result->data = $data;
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	/* ListTaskById()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$data = $model->GetProjectTasks($id);
		$result->data = $data['rows'];
		$result->tag = $data['total'];

		echo json_encode($result);
	}*/

	function ListById()
	{
		$model = $this->model;
		$result = new DataHandler();
		$id = $this->session->userdata('id');
		$result = new DataHandler();
		extract($_POST);
		$data = $model->GetByUserId($id);
		$result->data = $data['rows'];
		$result->tag = $data['total'];

		echo json_encode($result);
	}

	function OptionList()
	{
		$result = new DataHandler();
		$model = $this->model;

		$row = $model->GetAll();
		$data = array();

		$default = array("key" => "", "value" => "--- Select Project ---");
		array_push($data, $default);

		foreach($row['rows'] as $key)
		{
			$r = array();
			$r["key"] = $key->project_id;
			$r["value"] = $key->project_name;

			array_push($data, $r);
		}

		$result->data = $data;
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function AddProjectLog($id, $remarks)
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->AddLog($id, $remarks))
		{
			$result->message = "Successfully Added!";
		}
		else
		{
			$result->message = "Failed!";
		}

		return $result;
	}
}

?>