<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Worker extends CI_Controller{
	private $model;
	private $model_name = "WorkerData";
	private $view_name = "engineer";

	function __construct()
	{
		parent::__construct();

		//SET MODEL
		$this->load->model($this->model_name);
		$this->model = $this->WorkerData;

		/*if ( ! $this->session->userdata('loggedin'))
		{
			redirect(base_url());
		}*/
	}

	function index()
	{
		$this->load->view($this->view_name);
	}

	function Initialize()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;

		$data = $model->GetDataById($id);
		$result->data = $data;

		echo json_encode($result);

	}

	function Edit()
	{
		$result = new DataHandler();
		$model = $this->model;
		$data = $model->ProcessFields($_POST);

		$data['user_password_hash'] = md5($data['user_password_text']);
		if($model->Update($data))
		{
			$result->error = false;
			$result->message = "Successfully updated!";
		}
		else
		{
			$result->error = true;
			$result->message = "Update failed!";
		}

		echo json_encode($result);

	}

	function Remove()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->Delete($id))
		{
			$result->error = false;
			$result->message = "Successfully deleted!";
		}
		else
		{
			$result->error = true;
			$result->error = "Deletion failed!";
		}

		echo json_encode($result);
	}

	function RemoveTaskWorker()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		if($model->DeleteTaskWorker($id, $user_id))
		{
			$result->error = false;
			$result->message = "Successfully deleted!";
		}
		else
		{
			$result->error = true;
			$result->error = "Deletion failed!";
		}

		echo json_encode($result);
	}

	function Save()
	{
		$model = $this->model;
		$result = new DataHandler();

		$data = $model->ProcessFields($_POST);
		$data['user_password_hash'] = md5($data['user_password_text']);

		if($model->Add($data))
		{
			$result->error = false;
			$result->message = "A new engineer is successfully added!";
			$result->tag = $model->return_id;
		}
		else
		{
			$result->error = false;
			$result->message = "Failed!";
		}

		echo json_encode($result);
	}

	function SaveTaskWorker()
	{
		$model = $this->model;
		$result = new DataHandler();

		$data = $model->ProcessFieldsTaskWorkers($_POST);

		if($model->AddTaskWorker($data))
		{
			$result->error = false;
			$result->message = "A new worker is successfully added!";
		}
		else
		{
			$result->error = false;
			$result->message = "Failed!";
		}

		echo json_encode($result);
	}

	function ListView()
	{
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAll();
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function ListTaskWorkers()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAllWorkersByTask($id);
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);

	}

	function LiskWorkerDeduction()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAllWorkerDeduction($id);
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function WorkerAttendance()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->UpdateAttendanceById($id, $type, $date, $check);
		if($row)
		{
			$result->data = $row['rows'];
			$result->tag = $row['total'];
			$result->message = "OK!";
			$result->checked = $check;
		}
		else
		{
			$result->message = "FAILED!";
		}

		echo json_encode($result);
	}

	function ListViewByTask()
	{
		extract($_POST);
		$model = $this->model;
		$result = new DataHandler();

		$row = $model->GetAllByTask($id);
		$result->data = $row['rows'];
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function KeyValueListWorkerKey()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;

		$row = $model->GetAllWorkerKey($type);
		$data = array();
		
		foreach($row['rows'] as $key)
		{
			$r = array();
			$r["key"] = $key->user_id;
			$r["value"] = $key->name;
			
			array_push($data,$r);
		}

		$result->data = $data;
		$result->tags = $row["total"];

		echo json_encode($result); 
	}

	function ListAllByProject()
	{
		extract($_POST);
		$result = new DataHandler();
		$model = $this->model;

		$row = $model->GetAllByProject($id);
		$data = array();

		$default = array("key" => "", "value" => "--- Select Worker ---");
		array_push($data, $default);

		foreach($row['rows'] as $key)
		{
			$r = array();
			$r["key"] = $key->user_id;
			$r["value"] = $key->name;

			array_push($data, $r);
		}

		$result->data = $data;
		$result->tag = $row['total'];

		echo json_encode($result);
	}

	function OptionList()
	{
		$result = new DataHandler();
		$model = $this->model;

		$row = $model->GetAll();
		$data = array();

		$default = array("key" => "", "value" => "--- Select Worker ---");
		array_push($data, $default);

		foreach($row['rows'] as $key)
		{
			$r = array();
			$r["key"] = $key->user_id;
			$r["value"] = $key->user_firstname.' '.$key->user_lastname;

			array_push($data, $r);
		}

		$result->data = $data;
		$result->tag = $row['total'];

		echo json_encode($result);
	}
}
?>