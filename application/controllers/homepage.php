<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller{
	private $view_name = "homepage";

	function index()
	{
		if ( ! $this->session->userdata('loggedin'))
		{
			redirect(base_url());
		}
		$this->load->view($this->view_name);
	}
}
?>