<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'shared/metadata.php'; ?>
	<script src="<?php echo $base_url; ?>js/client.js" type="text/javascript"></script>
	<script src="<?php echo $base_url; ?>js/clientUI.js" type="text/javascript"></script>
</head>

<body>
<div id="wrapper">
<?php include 'shared/navigation.php'; ?>
    <!-- Page Content -->
    <div id="page-wrapper">
    	 <!-- Navigation -->
    	 <div class="container-fluid">
    	 <!-- Navigation -->
	    	<div class="row">
	            <div class="col-lg-12" style="padding: 0px;">
	                <h1 class="page-header" style="font-size:28px;color:#555;height:40px; margin: 15px 50px;">
	                	<b>Clients</b>
	                	<button id="add-client-btn" class="btn btn-primary btn-mku" style="float:right"><b>Add Client</b></button>
	                	<button id="print-client-btn" class="btn btn-primary btn-mku" style="float:right" onclick="PrintClientList();"><b>Print all</b></button>
	                </h1>	
				</div>
				<!-- /.col-lg-12 -->
	       </div>
	        <div class="row" align="center" >
				<div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover" id="client_table">
	                    <thead>
	                        <tr>
	                            <th>Firstname</th>
	                            <th>Lastname</th>
	                            <th>Gender</th>
	                            <th width="180px">Contact Number</th>
	                            <th width="320px">Address</th>
	                            <th width="25px"></th>
	                            <th width="25px"></th>
	                            <!--<th width="25px"></th>-->
	                        </tr>
	                    </thead>
						<tbody id="client-list-result">
								<!-- RESULTS HERE -->
						 </tbody>
					</table>
				</div>
	        </div>
	    </div>
	</div>
</div>
<?php 
	include 'shared/modal/client-modal.php'; 
?>
</body>
</html>

<!-- client/s List -->
<script type="text/x-jQuery-tmpl" id="client-list-tmpl">
	<tr>
		<td>${user_firstname}</td>
		<td>${user_lastname}</td>
		<td>${user_gender}</td>
		<td>${user_contact_number}</td>
		<td>${user_address}</td>
		<td style="text-align:center;">
			<button type="button" class="view" style="padding:2px 2px;" onclick="ViewUser(${user_id})">
				<i class="fa fa-expand" aria-hidden="true"></i>
			</button>
		</td>
		<td style="text-align:center;">
			<button type="button" class="edit" style="padding:2px 4px;" onclick="Initialize(${user_id})">
				<i class="fa fa-pencil" aria-hidden="true"></i>
			</button>
		</td>
		<!--<td style="text-align:center;">
			<button type="button" class="remove" style="padding:2px 4px;" onclick="Remove(${user_id})">
				<i class="fa fa-trash" aria-hidden="true"></i>
			</button>
		</td>-->
	</tr>
</script>

<!-- client projects -->
<script type="text/x-jQuery-tmpl" id="client-project-list-tmpl">
	<tr>
		<td>${project_name}</td>
		<td>${project_location}</td>
	</tr>
</script>

