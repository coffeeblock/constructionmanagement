<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'shared/metadata.php'; ?>
	<script src="<?php echo $base_url; ?>js/material.js" type="text/javascript"></script>
	<script src="<?php echo $base_url; ?>js/materialUI.js" type="text/javascript"></script>
</head>

<body>
<div id="wrapper">
<?php include 'shared/navigation.php'; ?>
    <!-- Page Content -->
    <div id="page-wrapper">
    	 <!-- Navigation -->
    	 <div class="container-fluid">
    	 <!-- Navigation -->
	    	<div class="row">
	            <div class="col-lg-12" style="padding: 0px;">
	                <h1 class="page-header" style="font-size:28px;color:#555;height:40px; margin: 15px 50px;">
	                	<b>Materials</b>
	                	<button id="add-material-btn" class="btn btn-primary btn-mku" style="float:right"><b>Add material</b></button>
	                	<button id="print-inventory-btn" class="btn btn-primary btn-mku" style="float:right" onclick="PrintMaterialList();"><b>Print all</b></button>
	                </h1>	
				</div>
				<!-- /.col-lg-12 -->
	       </div>
	        <div class="row" align="center" >
				<div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover" id="material_table">
	                    <thead>
	                        <tr>
	                            <th>Name</th>
	                            <th>Unit</th>
	                            <th>Cost</th>
	                            <th width="15px"></th>
	                            <th width="15px"></th>
	                        </tr>
	                    </thead>
						<tbody id="material-list-result">
								<!-- RESULTS HERE -->
						 </tbody>
					</table>
				</div>
	        </div>
	    </div>
	</div>
</div>
<?php 
	include 'shared/modal/material-modal.php';
?>
</body>
</html>

<!-- material/s List -->
<script type="text/x-jQuery-tmpl" id="material-list-tmpl">
	<tr>
		<td>${material_name}</td>
		<td>${material_unit}</td>
		<td>${material_cost}</td>
		<td style="text-align:center;">
			<button type="button" class="edit" style="padding:2px 4px;" onclick="Initialize(${material_id}, ${status})">
				<i class="fa fa-pencil" aria-hidden="true"></i>
			</button>
		</td>
		<td style="text-align:center;">
			<button type="button" class="remove" status="${status}" style="padding:2px 4px;" onclick="Remove(${material_id})">
				<i class="fa fa-trash" aria-hidden="true"></i>
			</button>
		</td>
	</tr>
</script>

