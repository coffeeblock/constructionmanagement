<!DOCTYPE html>
<html lang="en">

<head>
	<?php include "shared/metadata.php"; ?>
    <script src="<?php echo $base_url; ?>js/login.js"></script>
    <script src="<?php echo $base_url; ?>js/loginUI.js"></script>
</head>

<body>
<div class="wrapper">
	<nav class="nav static-top" style="margin-bottom: 0; background-color: #af0303;">
		<img src="<?php echo $base_url; ?>images/header.png" style="height:90%;">
	</nav>
	<nav class="nav static-top" style="margin-top: -1px; background-color: #000000;">
	</nav>
    <div class="container">
    	 <!-- Navigation -->
        <div class="row">
            <div class="inner">
                <div class="board" style="width:50%;margin-top:8%;">
                    <div class="board-heading">
                        <h3 class="board-title" align="center"><b>System Log In</b></h3>
                    </div>
                    <div class="board-body" style="width:40%;height:200px;">
                        <span id="login-tip"></span>
                        <form class="form" style="width:100%;height:100%;">
                            <input id="username" class="input" placeholder="Username"    name="username" autofocus style="height:22px;width:90%;margin-bottom:4px;"><br>
                            <input id="password" class="input" placeholder="Password"    name="password" type="password" value="" style="height:22px;width:90%;margin-bottom:4px;">
                            
                            <button id="submit-btn" type="button" class="button" style="border-color:grey;background-color: #aa0404;margin:auto auto !important;width:100%;">
                                Login
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</body>

</html>
