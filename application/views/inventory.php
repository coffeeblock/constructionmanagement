<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'shared/metadata.php'; ?>
	<script src="<?php echo $base_url; ?>js/inventory.js" type="text/javascript"></script>
	<script src="<?php echo $base_url; ?>js/inventoryUI.js" type="text/javascript"></script>
</head>

<body>
<div id="wrapper">
<?php include 'shared/navigation.php'; ?>
    <!-- Page Content -->
    <div id="page-wrapper">
    	 <!-- Navigation -->
    	 <div class="container-fluid">
    	 <!-- Navigation -->
	    	<div class="row">
	            <div class="col-lg-12" style="padding: 0px;">
	                <h1 class="page-header" style="font-size:28px;color:#555;height:40px; margin: 15px 50px;">
	                	<b>Inventory</b>
	                	<button id="add-inventory-btn" class="btn btn-primary btn-mku" style="float:right"><b>Add Items</b></button>
	                	<button id="print-inventory-btn" class="btn btn-primary btn-mku" style="float:right" onclick="PrintInventoryList();"><b>Print all</b></button>
	                </h1>	
				</div>
				<!-- /.col-lg-12 -->
	       </div>
	        <div class="row" align="center" >
				<div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover" id="inventory_table">
	                    <thead>
	                        <tr>
	                            <th>Name</th>
	                            <th>Unit</th>
	                            <th>Quantity</th>
	                        </tr>
	                    </thead>
						<tbody id="inventory-list-result">
								<!-- RESULTS HERE -->
						 </tbody>
					</table>
				</div>
	        </div>
	    </div>
	</div>
</div>
<?php 
	include 'shared/modal/inventory-modal.php';
?>
</body>
</html>

<!-- inventory/s List -->
<script type="text/x-jQuery-tmpl" id="inventory-list-tmpl">
	<tr>
		<td>${material_name}</td>
		<td>${material_unit}</td>
		<td>${total}</td>
	</tr>
</script>

