<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'shared/metadata.php'; ?>
	<script src="<?php echo $base_url; ?>js/manpower.js" type="text/javascript"></script>
	<script src="<?php echo $base_url; ?>js/manpowerUI.js" type="text/javascript"></script>
	<script src="<?php echo $base_url; ?>js/worker.js" type="text/javascript"></script>
	<script src="<?php echo $base_url; ?>js/workerUI.js" type="text/javascript"></script>
</head>

<body>
<div id="wrapper">
<?php include 'shared/navigation.php'; ?>
    <!-- Page Content -->
    <div id="page-wrapper">
    	 <!-- Navigation -->
    	 <div class="container-fluid">
	    	<div class="row">
	            <div class="col-lg-12" style="padding: 0px;">
	                <h1 class="page-header" style="font-size:28px;color:#555;height:40px; margin: 15px 50px;">
	                	<b>Manpower</b>
	                	<button id="add-manpower-btn" class="btn btn-primary btn-mku" style="float:right"><b>Add manpower</b></button>
	                	<button id="print-inventory-btn" class="btn btn-primary btn-mku" style="float:right" onclick="PrintManpowerList();"><b>Print all</b></button>
	                </h1>	
				</div>
				<!-- /.col-lg-12 -->
	       </div>
	        <div class="row" align="center" >
				<div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover" id="manpower_table">
	                    <thead>
	                        <tr>
	                            <th>Name</th>
	                            <th>Rate per day</th>
	                            <th width="25px"></th>
	                            <th width="25px"></th>
	                        </tr>
	                    </thead>
						<tbody id="manpower-list-result">
								<!-- RESULTS HERE -->
						 </tbody>
					</table>
				</div>
	        </div>
	        <!-- WORKERS -->
	        <hr>
	        <h1 class="page-header" style="font-size:28px;color:#555;height:40px; margin: 15px 50px;">
	                	<b>Workers</b>
	                	<button id="add-worker-btn" class="btn btn-primary btn-mku" style="float:right"><b>Add worker</b></button>
	                	<button id="print-inventory-btn" class="btn btn-primary btn-mku" style="float:right" onclick="PrintManpowerList();"><b>Print all</b></button>
	                </h1>	
	        <div class="row" align="center" style="margin-bottom:20px;">
				<div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover" id="worker_table">
	                    <thead>
	                        <tr>
	                            <th>Firstname</th>
	                            <th>Lastname</th>
	                            <th>Age</th>
	                            <th>Gender</th>
	                            <th width="150px">Contact number</th>
	                            <th width="250px">Address</th>
	                            <th>Rate per day</th>
	                            <th width="25px"></th>
	                            <th width="25px"></th>
	                        </tr>
	                    </thead>
						<tbody id="worker-list-result">
								<!-- RESULTS HERE -->
						 </tbody>
					</table>
				</div>
	        </div>
	    </div>
	  </div>
</div>
<?php 
	include 'shared/modal/manpower-modal.php';
	include 'shared/modal/worker-modal.php';
?>
</body>
</html>

<!-- manpower/s List -->
<script type="text/x-jQuery-tmpl" id="manpower-list-tmpl">
	<tr>
		<td>${manpower_name}</td>
		<td>${manpower_rate_day}</td>
		<td style="text-align:center;">
			<button type="button" class="edit" style="padding:2px 4px;" onclick="Initialize(${manpower_id}, ${status})">
				<i class="fa fa-pencil" aria-hidden="true"></i>
			</button>
		</td>
		<td style="text-align:center;">
			<button type="button" class="remove" status="${status}" style="padding:2px 4px;" onclick="Remove(${manpower_id})">
				<i class="fa fa-trash" aria-hidden="true"></i>
			</button>
		</td>
	</tr>
</script>

<!-- worker/s List -->
<script type="text/x-jQuery-tmpl" id="worker-list-tmpl">
	<tr>
		<td>${user_firstname}</td>
		<td>${user_lastname}</td>
		<td>${user_age}</td>
		<td>${user_gender}</td>
		<td>${user_contact_number}</td>
		<td>${user_address}</td>
		<td>${user_rate}</td>
		<td style="text-align:center;">
			<button type="button" class="edit" style="padding:2px 4px;" onclick="WorkerInitialize(${user_id})">
				<i class="fa fa-pencil" aria-hidden="true"></i>
			</button>
		</td>
		<td style="text-align:center;">
			<button type="button" class="remove" style="padding:2px 4px;" onclick="WorkerRemove(${user_id})">
				<i class="fa fa-trash" aria-hidden="true"></i>
			</button>
		</td>
	</tr>
</script>

