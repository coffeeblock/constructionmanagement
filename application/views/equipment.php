<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'shared/metadata.php'; ?>
	<script src="<?php echo $base_url; ?>js/equipment.js" type="text/javascript"></script>
	<script src="<?php echo $base_url; ?>js/equipmentUI.js" type="text/javascript"></script>
</head>

<body>
<div id="wrapper">
<?php include 'shared/navigation.php'; ?>
    <!-- Page Content -->
    <div id="page-wrapper">
    	 <!-- Navigation -->
    	 <div class="container-fluid">
    	 <!-- Navigation -->
	    	<div class="row">
	            <div class="col-lg-12" style="padding: 0px;">
	                <h1 class="page-header" style="font-size:28px;color:#555;height:40px; margin: 15px 50px;">
	                	<b>Equipments</b>
	                	<button id="add-equipment-btn" class="btn btn-primary btn-mku" style="float:right"><b>Add equipment</b></button>
	                	<button id="print-equipment-btn" class="btn btn-primary btn-mku" style="float:right" onclick="PrintEquipmentList();"><b>Print all</b></button>
	                </h1>	
				</div>
				<!-- /.col-lg-12 -->
	       </div>
	        <div class="row" align="center" >
				<div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover" id="equipment_table" style="border:1px solid grey;">
	                    <thead>
	                        <tr>
	                            <th>Name</th>
	                            <th>Rate per day</th>
	                            <th width="15px"></th>
	                            <th width="15px"></th>
	                        </tr>
	                    </thead>
						<tbody style="border:1px solid grey;" id="equipment-list-result">
								<!-- RESULTS HERE -->
						 </tbody>
					</table>
				</div>
	        </div>
	        <div class="row">
	            <div class="col-lg-12" style="padding: 0px;">
	                <h1 class="page-header" style="font-size:28px;color:#555;height:40px; margin: 15px 50px;">
	                	<b>Logs</b>
	                	<button id="add-equipment-log-btn" class="btn btn-primary btn-mku" style="float:right"><b>Add log</b></button>
	                	<button id="print-equipment-btn" class="btn btn-primary btn-mku" style="float:right" onclick="PrintEquipmentList();"><b>Print all</b></button>
	                </h1>	
				</div>
				<!-- /.col-lg-12 -->
	       </div>
	         <div class="row" align="center" >
				<div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover" id="equipment_logs_table" style="border:1px solid grey;margin-bottom:20px;">
	                    <thead>
	                        <tr>
	                            <th>Name</th>
	                            <th>Project name</th>
	                            <th>Due</th>
	                            <th>OUT</th>
	                            <th>IN</th>
	                            <th>Worker</th>
	                            <th></th>
	                            <th></th>
	                        </tr>
	                    </thead>
						<tbody style="border:1px solid grey;" id="equipment-log-result">
								<!-- RESULTS HERE -->
						 </tbody>
					</table>
				</div>
	        </div>
	    </div>
	</div>
</div>
<?php 
	include 'shared/modal/equipment-modal.php';
	include 'shared/modal/equipment-log-modal.php';
?>
</body>
</html>

<!-- equipment/s List -->
<script type="text/x-jQuery-tmpl" id="equipment-list-tmpl">
	<tr>
		<td>${equipment_name}</td>
		<td>${equipment_rate_day}</td>
		<td style="text-align:center;">
			<button type="button" class="edit" style="padding:2px 4px;" onclick="Initialize(${equipment_id}, ${status})">
				<i class="fa fa-pencil" aria-hidden="true"></i>
			</button>
		</td>
		<td style="text-align:center;">
			<button type="button" class="remove" status="${status}" style="padding:2px 4px;" onclick="Remove(${equipment_id})">
				<i class="fa fa-trash" aria-hidden="true"></i>
			</button>
		</td>
	</tr>
</script>

<script type="text/x-jQuery-tmpl" id="equipment-log-tmpl">
	<tr>
		<td>${equipment_name}</td>
		<td>${project_name}</td>
		<td>${date_due}</td>
		<td>${date_sent}</td>
		<td>${date_returned}</td>
		<td>${worker_name}</td>
		<td style="text-align:center;">
			<button type="button" class="edit" style="padding:2px 4px;" onclick="InitializeLog(${id}, '${project_id}')">
				<i class="fa fa-pencil" aria-hidden="true"></i>
			</button>
		</td>
		<td style="text-align:center;">
			<button type="button" class="remove" status="${status}" style="padding:2px 4px;" onclick="RemoveLog(${id})">
				<i class="fa fa-trash" aria-hidden="true"></i>
			</button>
		</td>
	</tr>
</script>

