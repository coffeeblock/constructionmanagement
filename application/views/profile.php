<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'shared/metadata.php'; ?>
	<script src="<?php echo $base_url; ?>js/profile.js" type="text/javascript"></script>
	<script src="<?php echo $base_url; ?>js/profileUI.js" type="text/javascript"></script>
</head>

<body>
<div id="wrapper">
<?php include 'shared/navigation.php'; ?>
    <!-- Page Content -->
    <div id="page-wrapper">
    	 <!-- Navigation -->
    	 <div class="container-fluid">
	    	<div class="row">
	            <div class="col-lg-12" style="padding: 0px;">
	                <h1 class="page-header" style="font-size:28px;color:#555;height:40px; margin: 15px 338px;;">
	                	<b>Profile</b>
	                	<button id="edit-profile-btn" class="admin-control btn btn-primary btn-mku " style="float:right"><b>Edit profile</b></button>
	                </h1>	
				</div>
				<!-- /.col-lg-12 -->
	       </div>
	        <div class="row" align="center" style="width:50%;margin-right: 20%;margin-left:  25% !important;">
				<form id="add-user-form" name="add-user-form" method="POST" style="border: 1px solid grey;border-radius:4px;padding: 5%;background-color:#cccccc;">
	          <input type="hidden" id="user_id" name="user_id" class="input" value="<?php echo $this->session->userdata('id'); ?>">
	          <input type="hidden" id="user_type" value="<?php echo $this->session->userdata('user_type'); ?>" >
	          <table width="100%">
	            <tr>
	              <td><b>Firstname</b></td>
	              <td><input type="text" id="user_firstname" name="user_firstname" class="input" readonly></td>
	            </tr>
	            <tr>
	              <td><b>Lastname</b></td>
	              <td><input type="text" id="user_lastname" name="user_lastname" class="input" readonly></td>
	            </tr>
	            <tr id="rate-tr">
	              <td><b>Rate</b></td>
	              <td><input type="text" id="user_rate" name="user_rate" class="input" style="width:45%;margin:0px 0px;display:inline-block;" readonly>   /day</td>
	            </tr>
	            <tr>
	              <td><b>Username</b></td>
	              <td><input type="text" id="user_username" name="user_username" class="input" readonly></td>
	            </tr>
	            <tr>
	              <td><b>Password</b></td>
	              <td>
	                <input type="text" id="user_password" name="user_password" class="input" style="margin:0px 0px;clear:both;display:inline-block;" readonly>
	              </td>
	            </tr>
	            <tr>
	              <td><b>Age</b></td>
	              <td><input type="text" id="user_age" name="user_age" class="input" readonly></td>
	            </tr>
	            <tr>
	              <td><b>Gender</b></td>
	              <td>
	                <select id="user_gender" style="width:57%;" class="input" Placeholder="--Select Gender--" readonly disabled="true">
	                <option default disabled selected>-- Select Gender --</option>
	                  <option value="Female">Female</option>
	                  <option value="Male">Male</option>
	                </select>
	              </td>
	            </tr>
	            <tr>
	              <td><b>Contact Number</b></td>
	              <td><input type="text" id="user_contact_number" name="user_contact_number" class="input" readonly></td>
	            </tr>
	            <tr>
	              <td><b>Address</b></td>
	              <td><input type="text" id="user_address" name="user_address" class="input" readonly></td>
	            </tr>
	          </table>
		    </form>
	        </div>
	    </div>
	</div>
</div>
<?php
	//check user type
	if($this->session->userdata('user_type') != 2)
		include 'shared/modal/client-modal.php';
	else
		include 'shared/modal/engineer-modal.php';
?>
</body>
</html>


