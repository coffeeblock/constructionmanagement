           <div class="static-top mku-nav" role="navigation" ">
                <div class="side-nav" style="margin-top:-4px;">
                    <ul class="mku-nav side-menu" style="width:auto;margin-top:-2px !important;">
                        <li>
                            <a href="<?php echo $base_url; ?>client" style="min-height:40px !important; padding:9px 9px;" class="">
                                 <i class="fa fa-users" aria-hidden="true"></i>&nbsp
                                 CLIENTS
                            </a>
                        </li>
                        <li class="" style="">
                            <a href="<?php echo $base_url; ?>project" style="min-height:40px !important; padding:9px 9px;" class="">
                                 <i class="fa fa-sitemap" aria-hidden="true"></i>&nbsp
                                 PROJECTS
                            </a>
                        </li>
                        <li class="" style="">
                            <a href="<?php echo $base_url; ?>engineer" style="min-height:40px !important; padding:9px 9px;" class="">
                                <i class="fa fa-suitcase" aria-hidden="true"></i>&nbsp
                                ENGINEERS
                            </a>
                        </li>
                        <li id="module-nav" style="width:150px">
                            <a href="#" style="min-height:40px !important; padding:9px 9px;" class="">
                                <i class="fa fa-folder" aria-hidden="true"></i>&nbsp
                                 MODULES
                            </a>
                            <ul id="submodules">
                                <li class="" style="">
                                    <a href="<?php echo $base_url; ?>inventory" style="min-height:40px !important; padding:9px 9px;" class="">
                                        <i class="fa fa-folder" aria-hidden="true"></i>&nbsp
                                         INVENTORY
                                    </a>
                                </li>
                                <li class="" style="">
                                    <a href="<?php echo $base_url; ?>material" style="min-height:40px !important; padding:9px 9px;" class="">
                                         <i class="fa fa-cubes" aria-hidden="true"></i>&nbsp
                                          MATERIALS
                                    </a>
                                </li>
                                <li class="" >
                                    <a href="<?php echo $base_url; ?>equipment" style="min-height:40px !important; padding:9px 9px;" class="">
                                         <i class="fa fa-cogs" aria-hidden="true"></i>&nbsp
                                          EQUIPMENTS
                                    </a>
                                </li>
                                <li class="" style="">
                                    <a href="<?php echo $base_url; ?>manpower" style="min-height:40px !important; padding:9px 9px;" class="">
                                         <i class="fa fa-male" aria-hidden="true"></i>
                                          MANPOWER
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
	   </nav>
