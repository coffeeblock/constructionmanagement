 <?php $base_url = base_url(); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MKU Construction Management</title>

    <!-- system template -->
        <link href="<?php echo $base_url; ?>css/system.css" rel="stylesheet">
        <link href="<?php echo $base_url;?>jquery/jquery-ui-1.12.0.custom/jquery-ui.css" rel="Stylesheet" type="text/css" />

    <!-- DataTable -->
    <link href="<?php echo $base_url;?>bower_components/datatables/media/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $base_url;?>css/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $base_url;?>jquery/SimplegranttChart/css/style.css" rel="stylesheet" type="text/css">

    <!-- jQuery -->
    <script src="<?php echo $base_url; ?>jquery/jquery-1.12.4.js"></script>

    <script src="<?php echo $base_url;?>jquery/jquery-ui-1.12.0.custom/jquery-ui-1.10.4.custom.min.js"></script>


    <script src="<?php echo $base_url;?>bower_components/datatables/media/js/jquery.dataTables.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo $base_url; ?>bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <script src="<?php echo $base_url; ?>jquery/jquery.tmpl.js" type="text/javascript"></script>
    <script src="<?php echo $base_url; ?>jquery/SimplegranttChart/js/jquery.fn.gantt.js" type="text/javascript"></script>

    <script src="<?php echo $base_url; ?>js/system.js"></script>
