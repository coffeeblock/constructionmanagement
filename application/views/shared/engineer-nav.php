            <div class="static-top mku-nav" role="navigation" ">
                <div class="side-nav" style="margin-top:-4px;">
                    <ul class="mku-nav side-menu" style="width:auto;margin-top:-2px !important;z-index:7777 !important;">
                        <!--<li>
                            <a href="<?php echo $base_url; ?>user" style="min-height:40px !important; padding:9px 9px;" class="dropdown-toggle nav-link">
                                <i class="fa fa-suitcase fa-fw"></i>
                                 PROFILE
                            </a>
                        </li>-->
                        <li class="dropdown" style="height:100%;">
                            <a href="<?php echo $base_url; ?>project" style="min-height:40px !important; padding:9px 9px;" class="dropdown-toggle">
                                <i class="fa fa-sitemap fa-fw"></i>
                                 PROJECTS
                            </a>
                        </li>
                        <li class="dropdown" style="height:100%;">
                            <a href="<?php echo $base_url; ?>material" style="min-height:40px !important; padding:9px 9px;" class="dropdown-toggle">
                                <i class="glyphicon glyphicon-road"></i>
                                  MATERIALS
                            </a>
                        </li>
                        <li class="dropdown" style="height:100%;">
                            <a href="<?php echo $base_url; ?>equipment" style="min-height:40px !important; padding:9px 9px;" class="dropdown-toggle">
                                <i class="fa fa-wrench"></i>
                                  EQUIPMENTS
                            </a>
                        </li>
                        <li class="dropdown" style="height:100%;">
                            <a href="<?php echo $base_url; ?>manpower" style="min-height:40px !important; padding:9px 9px;" class="dropdown-toggle">
                                <i class="fa fa-users"></i>
                                  MANPOWER
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
       </nav>
