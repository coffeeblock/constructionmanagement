<?php $user_type = $this->session->userdata("user_type"); ?>
    <nav class="static-top" role="navigation" style="margin-bottom: 0px;border:none; background-color: #af0303;">
        <input id="user_type" type="hidden" value="<?php echo $user_type; ?>">
        <div class="" style="width:20%;display: inline-block;">
                <a class="" href="#" style="padding: 2px 2px;">
                    <img src="<?php echo $base_url; ?>images/header.png" style="height:49px;">
                </a>
        </div>
            <!-- /.navbar-header -->
         <ul class="nav-right" style="width:270px;height:50px;padding:0px;margin: 0px 0px;">
            <li style="width:auto;height:20px;padding:15px 8px;">
            <a href="<?php echo $base_url;  ?>user" style="color:#fff;width:100%;height:100%;">
                <i class="fa fa-user" aria-hidden="true"></i>
                <?php 
                    echo $this->session->userdata("firstname").' '.$this->session->userdata("lastname"); 
                ?>
                </a>
            </li>
            <li style="width:80px;height:20px;padding:15px 8px;"><a href="<?php echo $base_url; ?>user/logout" style="color:#fff;width:100%;height:100%;">
                    <i class="fa fa-sign-out" aria-hidden="true"></i>
                    Logout
                </a>
            </li>
        </ul>

    <?php
        switch($user_type)
        {
            case 1:
                include 'admin-nav.php';
                break;
            case 2:
                include 'engineer-nav.php';
                break;
            case 3:
                include 'client-nav.php';
                break;
        }
    ?>