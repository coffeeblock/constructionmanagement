<div class="modal-plane" id="project-modal">
  <div class="modal-inner" style="width:650px;">
      <div class="modal-header">
        <button type="button" class="close" onclick="closeModal('project-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">
          <span>
            <button id="project-header" style="background-color:transparent;border:1px solid grey;border-radius:5px;"></button>
            <button id="project-estimate-header" style="background-color:transparent;border:1px solid grey;border-radius:5px;" onclick="ViewEstimated();"></button>
          </span> 
        </h4>
      </div>
      <div class="modal-body" style="overflow-y:scroll;height:480px;">
        <span id="add-project-tip"></span>
        <form id="add-project-form" name="add-project-form" method="POST">
          <button id="print-estimated-btn" class="button btn-mku" style="float:right" onclick="PrintEstimates();"><b>Print estimated</b></button>
          <input type="hidden" id="project_id" name="project_id" class="input">
          <input type="hidden" id="project_total_task_cost" name="project_total_task_cost" class="input">
          <input type="hidden" id="project_equipment_due_cost" name="project_equipment_due_cost" class="input">
          <div id="project-details">
            <div class="left" style="display:inline-block;width:50%;">
              <table width="100%" class="upper-table">
                <tr>
                  <td><b>Overall progress: </b></td>
                  <td><b><span id="project-percentage"></span></b></td>
                </tr>
                <tr>
                  <td><b>Name</b></td>
                  <td><input type="text" id="project_name" name="project_name" class="input" ></td>
                </tr>
                <tr>
                  <td><b>Location</b></td>
                  <td><input type="text" id="project_location" name="project_location" class="input"></td>
                </tr>
                <tr>
                  <td><b>Start date</b></td>
                  <td>
                    <input type="date" id="p_start_date" name="start_date" class="input">
                  </td>
                </tr>
                <tr>
                  <td><b>End date</b></td>
                  <td>
                    <input type="date" id="p_end_date" name="project_end_date" class="input">
                  </td>
                </tr>
                <tr>
                  <td><b>Dimensions</b></td>
                  <td>
                    <table class="sub-table">
                      <tr>
                        <td width="70px">Length(m):</td> 
                        <td>
                          <input type="text" id="project_dim_length" name="project_dim_length" class="input input-dimension" onkeypress="return NumberOnly(event, this)">
                        </td>
                    </tr>
                    <tr>
                      <td width="70px">Height(m): </td>
                        <td> 
                          <input type="text" id="project_dim_height" name="project_dim_height" class="input input-dimension" onkeypress="return NumberOnly(event, this)">
                        </td>
                    </tr>
                    <tr>
                      <td width="70px">Width(m):</td> 
                      <td>
                        <input type="text" id="project_dim_width" name="project_dim_width" class="input input-dimension" onkeypress="return NumberOnly(event, this)">
                      </td>
                    </tr>
                    <tr>
                      <td width="70px">Lanes:</td> 
                      <td>
                        <input type="text" id="project_dim_lanes" name="project_dim_lanes" class="input input-dimension" onkeypress="return NumberOnly(event, this)">
                      </td>
                    </tr>
                    </table>
                  </td>
                </tr>
                <tr>
                  <td><b>Engineer</b></td>
                  <td>
                    <select id="project_engineer" class="input" value="">
                    </select>
                  </td>
                </tr>
                <tr>
                  <td><b>Client</b></td>
                  <td>
                    <select id="project_client" class="input" value="">
                    </select>
                  </td>
                </tr>
                <tr>
                  <hr>
                  <td colspan="2"><b>Task Estimate</b></td>
                </tr>
                <tr>
                  <td>Excavation</td>
                  <td>
                    <input type="text" id="excavation" name="excavation" class="input input-dimension" onkeypress="return NumberOnly(event, this)"> m³
                  </td>
                </tr>
                <tr>
                  <td>Embankment</td>
                  <td>
                    <input type="text" id="embankment" name="embankment" class="input input-dimension" onkeypress="return NumberOnly(event, this)"> m³
                  </td>
                </tr>
                <tr>
                  <td>Sub-grade</td>
                  <td>
                    <input type="text" id="sub-grade" name="sub-grade" class="input input-dimension" onkeypress="return NumberOnly(event, this)"> m² 
                  </td>
                </tr>
                <tr>
                  <td>Sub-base</td>
                  <td>
                    <input type="text" id="sub-base" name="sub-base" class="input input-dimension" onkeypress="return NumberOnly(event, this)"> m³
                  </td>
                </tr>
                <tr>
                  <td>Base course</td>
                  <td>
                    <input type="text" id="base-course" name="base-course" class="input input-dimension" onkeypress="return NumberOnly(event, this)"> m³
                  </td>
                </tr>
              </table>
            </div>
            <div id="project-total" style="position:absolute;top:20px;display:inline-block;width:45%;margin-left:10px;">
              <b>Project Cost</b>
              <hr style="margin-top:10px;">
              <table style="width:100%;margin-top:5px;border-collapse: collapse;display: table;">
                <thead>
                    <tr>
                      <th width="30%">Task</th>
                      <th width="20%">Days</th>
                      <th width="30%">Cost</th>
                      <th>Status</th>
                    </tr>
                </thead>
                <tbody id="project-total-cost" >

                </tbody>
              </table>
              <table style="width:100%;margin-top:5px;border-collapse: collapse;display: table;">
                <tbody id="project-summary">

                </tbody>
              </table>
              <br>
              <b>Additional Fees</b>
              <table  style="width:100%;">
                <thead>
                  <tr>
                    <th>Equipment</th>
                    <th>Days due</th>
                    <th>Amount</th>
                  </tr>
                </thead>
                <tbody id="equipment-due-result" style="color:red;">

                </tbody>
              </table>
              <table style="width:100%;margin-top:5px;border-collapse: collapse;display: table;">
                <tbody id="equipment-due-summary">

                </tbody>
              </table>
            </div>
          </div>
          <div id="estimated-cost">
            <b>Generated task</b>
            <table style="width:75%;">
              <tbody id="estimated-cost-result">
                
              </tbody>
            </table>  
            <table id="estimated-total-cost" style="width:75%;">

            </table>
          </div>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="button btn-default" onclick="closeModal('project-modal');">Close</button>
        <button type="button" class="button btn-mku" id="project-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-inner -->
</div><!-- /.modal -->
