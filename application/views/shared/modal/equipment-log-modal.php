<div class="modal-plane" id="equipment-log-modal">
    <div class="modal-inner" >
      <div class="modal-header">
        <button type="button" class="close" onclick="closeModal('equipment-log-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Equipment</h4>
      </div>
      <div class="modal-body">
        <span id="add-equipment-log-tip"></span>
        <form id="add-equipment-log-form" name="add-equipment-log-form" method="POST">
          <input type="hidden" id="el_id" name="el_id" class="input">
          <table width="100%">
            <tr>
              <td><b>Project Name</b></td>
              <td>
                <select type="text" id="project_id" name="project_id" class="input" style="width:59%;" >
                </select>
              </td>
            </tr>
            <tr>
              <td><b>Equipment Name</b></td>
              <td><select type="text" id="equipment_log_id" name="equipment_log_id" class="input" style="width:59%;" >
                  </select>
              </td>
            </tr>
            <tr>
              <td><b>Liable Person</b></td>
              <td>
                <select type="text" id="worker_id" name="worker_id" class="input" style="width:59%;" >
                </select>
              </td>
            </tr>
            <tr>
              <td><b>Date Due</b></td>
              <td><input type="date" id="el_date_due" name="el_date_due" class="input"></td>
            </tr>
            <tr>
              <td><b>Date Sent</b></td>
              <td><input type="date" id="el_date_sent" name="el_date_sent" class="input"></td>
            </tr>
            <tr>
              <td><b>Date Returned</b></td>
              <td><input type="date" id="el_date_returned" name="el_time_returned" class="input"></td>
            </tr>
          </table>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeModal('equipment-log-modal');" >Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="equipment-log-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-inner -->
</div><!-- /.modal -->
