<div class="modal-plane" id="engineer-modal">
    <div class="modal-inner" style="height:550px;">
      <div class="modal-header">
        <button type="button" class="close" onclick="closeModal('engineer-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <button id="engineer-header" style="background-color:transparent;border:1px solid grey;border-radius:5px;">Engineer</button>
        <button id="engineer-project-header" style="background-color:transparent;border:1px solid grey;border-radius:5px;">Projects</button>
      </div>
      <div class="modal-body" style="height:400px;">
        <span id="add-engr-tip"></span>
        <form id="add-engineer-form" name="add-engineer-form" method="POST">
          <input type="hidden" id="engr_id" name="engr_id" class="input">
          <input type="hidden" id="engr_usertype" name="engr_id" class="input" 
                  value="<?php echo $this->session->userdata('user_type'); ?>">
          <table id="engineer-info" width="100%">
            <tr>
              <td><b>Firstname</b></td>
              <td><input type="text" id="engr_firstname" name="engr_firstname" class="input"></td>
            </tr>
            <tr>
              <td><b>Lastname</b></td>
              <td><input type="text" id="engr_lastname" name="engr_lastname" class="input"></td>
            </tr>
            <tr>
              <td><b>Rate</b></td>
              <td><input type="text" id="engr_rate" name="engr_rate" class="input" style="width:45%;margin:0px 0px;display:inline-block;">   /day</td>
            </tr>
            <tr>
              <td><b>Psername</b></td>
              <td><input type="text" id="engr_username" name="engr_username" class="input"></td>
            </tr>
            <tr>
              <td><b>Password</b></td>
              <td>
                <input type="text" id="engr_password" name="engr_password" class="input" style="width:50%;margin:0px 0px;clear:both;display:inline-block;">
                <button id="rand-pass-btn" type="button" class="btn" style="display:inline-block;height:100%;">
                  generate password
                </button>
              </td>
            </tr>
            <tr>
              <td><b>Age</b></td>
              <td><input type="text" id="engr_age" name="engr_age" class="input"></td>
            </tr>
            <tr>
              <td><b>Gender</b></td>
              <td>
                <select id="engr_gender" class="input" Placeholder="--Select Gender--">
                <option default disabled selected>-- Select Gender --</option>
                  <option value="Female">Female</option>
                  <option value="Male">Male</option>
                </select>
              </td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><input type="text" id="engr_contact_number" name="engr_contact_number" class="input"></td>
            </tr>
            <tr>
              <td><b>Address</b></td>
              <td><input type="text" id="engr_address" name="engr_address" class="input"></td>
            </tr>
          </table>

           <!-- engineer projects -->
          <table id="engineer-projects-tbl" width="100%" border="1" cellspacing="0">
            <thead>
              <th>Project name</th>
              <th>Status</th>
            </thead>
            <tbody id="engineer-projects-result">
              
            </tbody>
          </table>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeModal('engineer-modal');">Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="engineer-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
