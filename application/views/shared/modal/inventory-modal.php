<div class="modal-plane" id="inventory-modal">
    <div class="modal-inner" >
      <div class="modal-header">
        <button type="button" clas
        <button type="button" class="close" onclick="closeModal('inventory-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Item Quantity</h4>
      </div>
      <div class="modal-body">
        <span id="add-inventory-tip"></span>
        <form id="add-inventory-form" name="add-inventory-form" method="POST">
          <input type="hidden" id="inventory_id" name="inventory_id" class="input">
          <table width="100%">
            <tr>
              <td><b>Item</b></td>
              <td>
                  <select id="material_id" class="input" value="" style="width:80%;">
                  </select>
              </td>
            </tr>
            <tr>
              <td><b>Quantity</b></td>
              <td><input type="text" id="inventory_quantity" name="inventory_quantity" style="width:20%;" class="input"
                  readonly></td>
            </tr>
          </table>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeModal('inventory-modal');">Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="inventory-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
