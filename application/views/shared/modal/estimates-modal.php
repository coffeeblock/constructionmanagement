<div class="modal-plane" id="estimates-modal">
    <div class="modal-inner" style="width:950px;height:500px;">
      <div class="modal-header">
        <button type="button" class="close" onclick="closeModal('estimates-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">estimates</h4>
      </div>
      <div class="modal-body" style="width:95%;height:100%;">
        <span id="add-engr-tip"></span>
        <form id="add-estimates-form" name="add-estimates-form" method="POST">
          <div class="gantt"></div>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeModal('estimates-modal');">Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="estimates-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
