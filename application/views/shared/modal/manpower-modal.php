<div class="modal-plane" id="manpower-modal">
    <div class="modal-inner" >
      <div class="modal-header">
        <button type="button" class="close" onclick="closeModal('worker-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Manpower</h4>
      </div>
      <div class="modal-body">
        <span id="add-manpower-tip"></span>
        <form id="add-manpower-form" name="add-manpower-form" method="POST">
          <input type="hidden" id="manpower_id" name="manpower_id" class="input">
          <table width="100%">
            <tr>
              <td><b>Name</b></td>
              <td><input type="text" id="manpower_name" name="manpower_name" class="input" ></td>
            </tr>
            <tr>
              <td><b>Rate/Day</b></td>
              <td><input type="text" id="manpower_rate_day" name="manpower_rate_day" class="input"
                    onkeypress="return NumberOnly(event, this)"></td>
            </tr>
          </table>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeModal('manpower-modal');">Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="manpower-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-inner -->
</div><!-- /.modal -->
