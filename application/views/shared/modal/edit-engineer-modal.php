<div class="modal fade" id="edit-engineer-modal">
  <div class="modal-dialog" style="width:600px;">
    <div class="modal-content" style="width:100%;">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Edit</h4>
      </div>
      <div class="modal-body">
        <span id="add-engr-tip"></span>
        <form id="add-engineer-form" name="add-engineer-form" method="POST">
          <table width="100%">
            <tr>
              <td><b>Firstname</b></td>
              <td><input type="text" id="e_engr_firstname" name="engr_firstname" class="form-control input-sm"></td>
            </tr>
            <tr>
              <td><b>Lastname</b></td>
              <td><input type="text" id="e_engr_lastname" name="engr_lastname" class="form-control input-sm"></td>
            </tr>
            <tr>
              <td><b>userrname</b></td>
              <td><input type="text" id="e_engr_username" name="engr_username" class="form-control input-sm"></td>
            </tr>
            <tr>
              <td><b>Password</b></td>
              <td>
                <input type="text" id="e_engr_password" name="engr_password" class="form-control input-sm" style="width:85%;margin:0px 0px;clear:both;display:inline-block;" >
                <button id="e-rand-pass-btn" type="button" class="btn" style="display:inline-block;">
                  <i class="glyphicon glyphicon-refresh"></i>
                </button>
              </td>
            </tr>
            <tr>
              <td><b>Age</b></td>
              <td><input type="text" id="e_engr_age" name="engr_age" class="form-control input-sm"></td>
            </tr>
            <tr>
              <td><b>Gender</b></td>
              <td>
                <select id="e_engr_gender" class="form-control input-sm" Placeholder="--Select Gender--">
                  <option default disabled selected>-- Select Gender --</option>
                    <option value="Female">Female</option>
                    <option value="Male">Male</option>
                </select>
              </td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><input type="text" id="e_engr_contact_number" name="engr_contact_number" class="form-control input-sm"></td>
            </tr>
            <tr>
              <td><b>Address</b></td>
              <td><input type="text" id="e_engr_address" name="engr_address" class="form-control input-sm"></td>
            </tr>
          </table>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="engr-update-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
