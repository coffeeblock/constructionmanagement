<div class="modal-plane" id="worker-modal">
    <div class="modal-inner" >
      <div class="modal-header">
        <button type="button" class="close" onclick="closeModal('worker-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Worker</h4>
      </div>
      <div class="modal-body">
        <span id="add-worker-tip"></span>
        <form id="add-worker-form" name="add-worker-form" method="POST">
          <input type="hidden" id="worker_id" name="worker_id" class="input">
          <table width="100%">
            <tr>
              <td><b>Firstname</b></td>
              <td><input type="text" id="worker_firstname" name="worker_firstname" class="input"></td>
            </tr>
            <tr>
              <td><b>Lastname</b></td>
              <td><input type="text" id="worker_lastname" name="worker_lastname" class="input"></td>
            </tr>
            <tr>
              <td><b>Rate</b></td>
              <td><input type="text" id="worker_rate" name="worker_rate" class="input" style="width:45%;margin:0px 0px;display:inline-block;">   /day</td>
            </tr>
            <tr>
              <td><b>Age</b></td>
              <td><input type="text" id="worker_age" name="worker_age" class="input"></td>
            </tr>
            <tr>
              <td><b>Gender</b></td>
              <td>
                <select id="worker_gender" class="input" Placeholder="--Select Gender--">
                <option default disabled selected>-- Select Gender --</option>
                  <option value="Female">Female</option>
                  <option value="Male">Male</option>
                </select>
              </td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><input type="text" id="worker_contact_number" name="worker_contact_number" class="input"></td>
            </tr>
            <tr>
              <td><b>Address</b></td>
              <td><input type="text" id="worker_address" name="worker_address" class="input"></td>
            </tr>
          </table>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeModal('worker-modal');">Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="worker-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-inner -->
</div><!-- /.modal -->
