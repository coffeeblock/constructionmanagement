<div class="modal-plane" id="material-modal">
    <div class="modal-inner" >
      <div class="modal-header">
        <button type="button" class="close" onclick="closeModal('material-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Material</h4>
      </div>
      <div class="modal-body">
        <span id="add-material-tip"></span>
        <form id="add-material-form" name="add-material-form" method="POST">
          <input type="hidden" id="material_id" name="material_id" class="input">
          <table width="100%">
            <tr>
              <td><b>Name</b></td>
              <td><input type="text" id="material_name" name="material_name" class="input" 
                    readonly></td>
            </tr>
            <tr>
              <td><b>Unit</b></td>
              <td><input type="text" id="material_unit" name="material_unit" class="input"
                  readonly></td>
            </tr>
            <tr>
              <td><b>Cost</b></td>
              <td><input type="text" id="material_cost" name="material_cost" class="input"
                  onkeypress="return NumberOnly(event, this)"></td>
            </tr>
          </table>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeModal('material-modal');">Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="material-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-inner -->
</div><!-- /.modal -->
