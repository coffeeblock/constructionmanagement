<div class="modal-plane" id="client-modal">
    <div class="modal-inner" style="height:550px;">
      <div class="modal-header">
        <button type="button" class="close" onclick="closeModal('client-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <button id="client-header" style="background-color:transparent;border:1px solid grey;border-radius:5px;">Client</button>
        <button id="client-project-header" style="background-color:transparent;border:1px solid grey;border-radius:5px;">Projects</button>
      </div>
      <div class="modal-body" style="height:400px;">
        <span id="add-client-tip"></span>
        <form id="add-client-form" name="add-client-form" method="POST">
          <input type="hidden" id="client_id" name="client_id" class="input">
          <input type="hidden" id="client_usertype" name="client_id" class="input" 
                  value="<?php echo $this->session->userdata('user_type'); ?>">
          <table id="client-info" width="100%">
            <tr>
              <td><b>Firstname</b></td>
              <td><input type="text" id="client_firstname" name="client_firstname" class="input"></td>
            </tr>
            <tr>
              <td><b>Lastname</b></td>
              <td><input type="text" id="client_lastname" name="client_lastname" class="input"></td>
            </tr>
            <tr>
              <td><b>username</b></td>
              <td><input type="text" id="client_username" name="client_username" class="input"></td>
            </tr>
            <tr>
              <td><b>Password</b></td>
              <td>
                <input type="text" id="client_password" name="client_password" class="input" style="margin:0px 0px;clear:both;display:inline-block;">
                <button id="rand-client-pass-btn" type="button" class="btn" style="display:inline-block;width:25px;margin-left:4px;color:#333;">
                  <i class="fa fa-refresh" aria-hidden="true"></i>
                </button>
              </td>
            </tr>
            <tr>
              <td><b>Age</b></td>
              <td><input type="text" id="client_age" name="client_age" class="input"></td>
            </tr>
            <tr>
              <td><b>Gender</b></td>
              <td>
                <select id="client_gender" style="width:57%;" class="input" Placeholder="--Select Gender--">
                <option default disabled selected>-- Select Gender --</option>
                  <option value="Female">Female</option>
                  <option value="Male">Male</option>
                </select>
              </td>
            </tr>
            <tr>
              <td><b>Contact Number</b></td>
              <td><input type="text" id="client_contact_number" name="client_contact_number" class="input"></td>
            </tr>
            <tr>
              <td><b>Address</b></td>
              <td><input type="text" id="client_address" name="client_address" class="input"></td>
            </tr>
          </table>

          <!-- client projects -->
          <table id="client-projects-tbl" width="100%" border="1" cellspacing="0">
            <thead>
              <th>Project name</th>
              <th>Status</th>
            </thead>
            <tbody id="client-projects-result">
              
            </tbody>
          </table>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeModal('client-modal');">Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="client-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
