<div class="modal-plane" id="equipment-modal">
    <div class="modal-inner" >
      <div class="modal-header">
        <button type="button" class="close" onclick="closeModal('equipment-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Equipment</h4>
      </div>
      <div class="modal-body">
        <span id="add-equipment-tip"></span>
        <form id="add-equipment-form" name="add-equipment-form" method="POST">
          <input type="hidden" id="equipment_id" name="equipment_id" class="input">
          <table width="100%">
            <tr>
              <td><b>Name</b></td>
              <td><input type="text" id="equipment_name" name="equipment_name" class="input" ></td>
            </tr>
            <tr>
              <td><b>Rate/Day</b></td>
              <td><input type="text" id="equipment_rate_day" name="equipment_rate_day" class="input"
                    onkeypress="return NumberOnly(event, this)"></td>
            </tr>
          </table>
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" onclick="closeModal('equipment-modal');" >Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="equipment-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-inner -->
</div><!-- /.modal -->
