<div class="modal-plane" id="task-modal">
    <div class="modal-inner" style="width:800px !important;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" onclick="closeModal('task-modal');" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">
          <span>
            <button id="task-header" style="background-color:transparent;border:1px solid grey;border-radius:5px;"></button> | 
            <button id="task-returned-subheader" style="background-color:transparent;border:1px solid grey;border-radius:5px;">Returned Items</button> | 
            <button id="task-worker-subheader" style="background-color:transparent;border:1px solid grey;border-radius:5px;">Workers</button> | 
            <button id="task-sub-header" style="background-color:transparent;border:1px solid grey;border-radius:5px;"></button>
          </span> 
        </h4>
      </div>
      <div class="modal-body" style="overflow-y:scroll;height:500px;padding-left:30px;">
        <span id="add-task-tip"></span>
        <button id="start-btns" class="button btn-mku" style="float:right;display:block;">START</button>
        <button id="done-btns" class="button btn-mku" style="float:right;display:block;">DONE</button>
          <div id="task-details" >
        <form id="add-task-form" name="add-task-form" method="POST">
          <input type="hidden" id="task_id" name="task_id" class="input form-control input-sm">
          <input type="hidden" id="workers_deduction" name="workers_deduction" class="input form-control input-sm">
          <hr style="margin-bottom:1px;margin-top:1px;">
          <b>Details</b>
          <table width="70%">
            <tr>
              <td><b>Description</b></td>
              <td><input type="text" id="task_name" name="task_name" class="input form-control input-sm" ></td>
            </tr>
            <tr>
              <td><b>Start date</b></td>
              <td><input type="date" id="task_start_date" name="task_start_date" class="input form-control input-sm"></td>
            </tr>
            <tr>
              <td><b>End date</b></td>
              <td>
                <input type="date" id="task_end_date" name="task_end_date" class="input form-control input-sm">
              </td>
            </tr>
          </table>
          <hr style="margin-bottom:1px;background-color:red !important;">
          <b>Materials</b>
          <table class="task-table-inputs">
            <tbody id="task_materials_result"> 

            </tbody>
          </table>
          <hr style="margin-bottom:1px;background-color:red !important;">
          <b>Equipments</b>
          <table class="task-table-inputs"> 
            <thead>
              <tr>
                <th></th>
                <th style="text-align:center;">Qty</th>
                <th style="text-align:center;">Days</th>
              </tr>
            </thead>
            <tbody id="task_equipments_result"> 

            </tbody>
          </table>
          <hr style="margin-bottom:1px;background-color:red !important;">
          <b>Manpower</b>
          <table class="task-table-inputs">
            <thead>
              <tr>
                <th></th>
                <th style="text-align:center;">Qty</th>
                <th style="text-align:center;">Days</th>
              </tr>
            </thead>
            <tbody id="task_manpower_result"> 

            </tbody>
          </table>
          </div>
          <!-- This is where the sub total is displayed -->
        <div id="task-sub-total">
          <!-- Materials Sub Total -->
          <b>MATERIALS</b>
          <table style="width:75%;margin-top:8px;">
            <thead>
              <tr>
                <th>Material</th>
                <th>Quantity</th>
                <th>Cost</th>
              </tr>
            </thead>
            <tbody id="task-material-total-result">

            </tbody>
          </table>
          <!-- Equipments Sub Total -->
          <hr style="margin-bottom:1px;background-color:red !important;">
          <b>EQUIPMENTS</b>
          <table style="width:75%;margin-top:8px;">
            <thead>
              <tr>
                <th>Equipment</th>
                <th>Quantity</th>
                <th>Days</th>
                <th>Cost</th>
              </tr>
            </thead>
            <tbody id="task-equipment-total-result">

            </tbody>
          </table>
          <!-- Manpower Sub Total -->
          <hr style="margin-bottom:1px;background-color:red !important;">
          <b>MANPOWER</b>
          <table style="width:75%;margin-top:8px;">
            <thead>
              <tr>
                <th>Manpower</th>
                <th>Quantity</th>
                <th>Days</th>
                <th>Cost</th>
              </tr>
            </thead>
            <tbody id="task-manpower-total-result">

            </tbody>
          </table>
          <hr style="margin-bottom:1px;background-color:red !important;">
          <b>SUMMARY</b>
          <table style="width:75%;margin-top:8px;">
            <tbody id="task-summary">

            </tbody>
          </table>
        </div>
        <!------------ WORKERS ----------------- -->
        <div id="task-workers">
            <b>WORKERS</b>
            <table style="width:70%;">
              <tr>
                <td>
                  <select style="width:100%;" id="manpower_id" class="input"></select>
                </td>
                <td>
                  <input type="hidden" id="worker_id">
                  <select style="width:200px;" id="task_worker_name" class="input"></select>
                </td>
                <td>
                <button type="button" id="add-task-worker" style="float:left;" class="btn btn-primary btn-mku">Add</button>
                </td>
              </tr>
            </table>

            <table style="width:90%;" border="1" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Type</th>
                  <th></th>
                </tr>
              </thead>
              <tbody id="task-worker-result">

              </tbody>
            </table>
            <hr>
            <b>Attendance deduction</b>
            <table style="width:90%;" border="1" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Deduction</th>
                </tr>
              </thead>
              <tbody id="task-worker-deduction-result">

              </tbody>
            </table>
        </div>
        <div id="task-returned-materials">
          <b>Returned Materials</b>
          <hr>
          <table width="70%">
            <tr>
              <td width="20%">Material</td>
              <td>
                <select style="width:90%;" id="returned_id" class="input"></select>
              </td>
              <td width="20%">
                <input id="returned_quantity" name="returned_quantity" type="text"  class="input">
              </td>
              <td>
                <button type="button" id="add-returned-item" style="float:left;" class="btn btn-primary btn-mku">Add</button>
              </td>
            </tr>
          </table>
          <table width="80%" border="1" cellspacing="0">
            <thead>
              <tr>
                <th>Name</th>
                <th>Qty. Returned</th>
                <th></th>
              </tr>
            </thead>
            <tbody id="items-returned-result">
              
            </tbody>
          </table>
        </div>
        <!-- ** END WORKERS -->
		    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="closeModal('task-modal');">Close</button>
        <button type="button" class="btn btn-primary btn-mku" id="task-btns" >Save changes</button>
      </div>
    </div><!-- /.modal-inner -->
</div><!-- /.modal -->
