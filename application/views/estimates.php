<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'shared/metadata.php'; ?>
	<script src="<?php echo $base_url; ?>js/estimate.js" type="text/javascript"></script>
	<script src="<?php echo $base_url; ?>js/estimateUI.js" type="text/javascript"></script>
	<style>
		.ui-autocomplete
		{
		    position:absolute !important;
		    cursor:default !important;
		    z-index:1001 !important
		}
	</style>
</head>

<body>
<div id="wrapper">
<?php include 'shared/navigation.php'; ?>
    <!-- Page Content -->
    <div id="page-wrapper">
    	 <!-- Navigation -->
    	 <div class="container-fluid">
	    	<div class="row">
	            <div class="col-lg-12" style="padding: 0px;">
	                <h1 class="page-header" style="font-size:28px;color:#555;height:40px; margin: 15px 50px;">
	                	<b id="project_name"></b>
	                	<input type="hidden" id="user_type" value="<?php echo $this->session->userdata("user_type"); ?>">
	                	<button id="view-estimate-btn" class="admin-control btn btn-primary btn-mku " style="float:right"><b>Estimates</b></button>
	                	<button id="print-project-btn" class="btn btn-primary btn-mku" style="float:right" onclick="PrintTaskList();"><b>Print all</b></button>
	                </h1>	
	                <input type="hidden" name="project_id" id="project_id" value="<?php echo $_GET['view_id']; ?>">
				</div>
				<!-- /.col-lg-12 -->
	       </div>
	        <div class="row" align="center" >
				<div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover" id="task_table">
	                    <thead>
	                        <tr>
	                            <th>Name</th>
	                            <th>Start</th>
	                            <th>End</th>
	                            <th>Status</th>
	                            <th width="25px"></th>
	                            <th class="admin-control" width="25px"></th>
	                            <th class="admin-control" width="25px"></th>
	                        </tr>
	                    </thead>
						<tbody id="task-list-result">
								<!-- RESULTS HERE -->
						 </tbody>
					</table>
				</div>
	        </div>
	    </div>
	</div>
</div>
<?php 
	include 'shared/modal/task-modal.php';
	include 'shared/modal/estimates-modal.php';
?>
</body>
</html>

<!-- task/s List -->
<script type="text/x-jQuery-tmpl" id="task-list-tmpl">
	<tr>
		<td>${task_description}</td>
		<td>${task_start_date}</td>
		<td>${task_end_date}</td> 
		<td>
			{{if status == 0}}
				<div style="background-color:#af0303;padding:2px;color:#fff;width:90%;margin: auto auto;text-align:center;font-weight:bold;">Pending</div>
			{{else status == 2}}
				<div style="background-color:#54a354;padding:2px;color:#fff;width:90%;margin: auto auto;text-align:center;font-weight:bold;">Done</div>
			{{else status == 3}}
				<div style="background-color:orange;padding:2px;color:#fff;width:90%;margin: auto auto;text-align:center;font-weight:bold;">In Progress</div>
			{{/if}}
		</td> 
		<td style="text-align:center;">
			<button type="button" class="view" style="padding:2px 4px;" onclick="View(${task_id})">
				<i class="fa fa-expand" aria-hidden="true"></i>
			</button>
		</td>
		<td class="admin-control" style="text-align:center;">
			<button type="button" class="edit" style="padding:2px 4px;" onclick="Initialize(${task_id})">
				<i class="fa fa-pencil" aria-hidden="true"></i>
			</button>
		</td>
		<td class="admin-control" style="text-align:center;">
			<button type="button" class="remove" style="padding:2px 4px;" onclick="Remove(${task_id})">
				<i class="fa fa-trash" aria-hidden="true"></i>
			</button>
		</td>
	</tr>
</script>

<script type="text/x-jQuery-tmpl" id="task-materials-tmpl">
	<tr>
		<td width="50px">${material_name}</td>
		<td>
			<input type="text" id="${material_id}" cat="material"  class="input form-control input-sm">
		</td>
	</tr>
</script>
<script type="text/x-jQuery-tmpl" id="task-equipments-tmpl">
	<tr>
		<td width="50px">${equipment_name}</td>
		<td>
			<input type="text" id="${equipment_id}" cat="equipment"  class="input form-control input-sm">
		</td>
		<td>
			<input type="text" id="${equipment_id}" cat="equipment" qty="true"  class="input form-control input-sm">
		</td>
	</tr>
</script>
<script type="text/x-jQuery-tmpl" id="task-manpower-tmpl">
	<tr>
		<td width="50px">${manpower_name}</td>
		<td>
			<input type="text" id="${manpower_id}" cat="manpower"  class="input form-control input-sm">
		</td>
		<td>
			<input type="text" id="${manpower_id}" cat="manpower" qty="true"  class="input form-control input-sm">
		</td>
	</tr>
</script>
<!-- Task total cost template --
---- Materials -->
<script type="text/x-jQuery-tmpl" id="task-material-total-tmpl">
	<tr>
		<td width="230px">${material_name}</td>
		<td>${tm_quantity}</td>
		<td width="80px">
			<input type="hidden" name="material-cost" value="${tm_total}">
			${tm_total}
		</td>
	</tr>
</script>
<!-- Equipments -->
<script type="text/x-jQuery-tmpl" id="task-equipment-total-tmpl">
	<tr>
		<td width="230px">${equipment_name}</td>
		<td>${te_quantity}</td>
		<td>${te_days}</td>
		<td width="80px">
			<input type="hidden" name="equipment-cost" value="${te_total}">
			${te_total}
		</td>
	</tr>
</script>
<!-- Manpower -->
<script type="text/x-jQuery-tmpl" id="task-manpower-total-tmpl">
	<tr>
		<td width="230px">${manpower_name}</td>
		<td>${tmp_quantity}</td>
		<td>${tmp_days}</td>
		<td width="80px">
			<input type="hidden" name="manpower-cost" value="${tmp_total}">
			${tmp_total}
		</td>
	</tr>
</script>

<!-- Workers input -->
<script type="text/x-jQuery-tmpl" id="task-worker-tmpl">
	<tr>
		<td>${name}</td>
		<td>
			${manpower_name}
		</td>
		<td class="admin-control" style="text-align:center;">
			<button type="button" class="remove" style="padding:2px 4px;" onclick="RemoveTaskWorker(${tw_id}, ${user_id})">
				<i class="fa fa-trash" aria-hidden="true"></i>
			</button>
		</td>
	</tr>
</script>

