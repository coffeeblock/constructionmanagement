<!DOCTYPE html>
<html lang="en">

<head>
	<?php include 'shared/metadata.php'; ?>
	<script src="<?php echo $base_url; ?>js/project.js" type="text/javascript"></script>
	<script src="<?php echo $base_url; ?>js/projectUI.js" type="text/javascript"></script>
</head>

<body>
<div id="wrapper">
<?php include 'shared/navigation.php'; ?>
    <!-- Page Content -->
    <div id="page-wrapper">
    	 <!-- Navigation -->
    	 <div class="container-fluid">
	    	<div class="row">
	            <div class="col-lg-12" style="padding: 0px;">
	                <h1 class="page-header" style="font-size:28px;color:#555;height:40px; margin: 15px 50px;">
	                	<b>Projects</b>
	                	<button id="add-project-btn" class="admin-control btn btn-primary btn-mku " style="float:right"><b>Add Project</b></button>
	                	<button id="print-project-btn" class="btn btn-primary btn-mku" style="float:right" onclick="PrintProjectList();"><b>Print all</b></button>
	                </h1>	
				</div>
				<!-- /.col-lg-12 -->
	       </div>
	        <div class="row" align="center" >
				<div class="dataTable_wrapper">
	                <table class="table table-striped table-bordered table-hover" id="project_table">
	                    <thead>
	                        <tr>
	                            <th>Name</th>
	                            <th>Location</th>
	                            <th>Start</th>
	                            <th>End</th>
	                            <th>Engineer</th>
	                            <th>Client</th>
	                            <!--<th>Status</th>-->
	                            <th width="25px"></th>
	                            <th width="25px"></th>
	                            <th></th>
	                            <th class="admin-control" width="25px"></th>
	                            <th class="admin-control" width="25px"></th>
	                        </tr>
	                    </thead>
						<tbody id="project-list-result">
								<!-- RESULTS HERE -->
						 </tbody>
					</table>
				</div>
	        </div>
	    </div>
	</div>
</div>
<?php 
	include 'shared/modal/project-modal.php';
?>
</body>
</html>

<!-- project/s List -->
<script type="text/x-jQuery-tmpl" id="project-list-tmpl">
	<tr>
		<td>${project_name}</td>
		<td>${project_location}</td>
		<td>${project_start_date}</td>
		<td>${project_end_date}</td>
		<td>${engineer_name}</td>
		<td>${client_name}</td>
		<!--<td>${status}</td>-->
		<td style="text-align:center;">
			<button type="button" class="view" style="width:auto !important;padding:2px 2px;" 
			onclick="ViewEstimates(${project_id}, '${project_name}');">
				Estimates
			</button>
		</td>
		<td style="text-align:center;">
			<button type="button" class="view" style="width:auto !important;padding:2px 2px;" 
			onclick="ViewTask(${project_id}, '${project_name}');">
				Task
			</button>
		</td>
		<td style="text-align:center;">
			<button type="button" class="view" style="padding:2px 2px;" onclick="ViewProject(${project_id})">
				<i class="fa fa-expand" aria-hidden="true"></i>
			</button>
		</td>
		<td class="admin-control" style="text-align:center;">
			<button type="button" class="edit" style="padding:2px 4px;" onclick="Initialize(${project_id})">
				<i class="fa fa-pencil" aria-hidden="true"></i>
			</button>
		</td>
		<td class="admin-control" style="text-align:center;">
			<button type="button" class="remove" style="padding:2px 4px;" onclick="Remove(${project_id})">
				<i class="fa fa-trash" aria-hidden="true"></i>
			</button>
		</td>
	</tr>
</script>
<!--- project total cost -->
<script type="text/x-jQuery-tmpl" id="project-total-cost-tmpl">
	<tr>
		<td width="30%">${task_description}</td>
		<td width="20%">${total_days}</td>
		<td width="30%">${total_cost}</td>
		<td>
			{{if status == 2}}
				<label style="color:#5abf6f;font-weight:bold;">Done</label>
			{{/if}}
			{{if status == 3}}
				<label style="color:#e7981c;font-weight:bold;">In Progress</label>
			{{/if}}
			{{if status == 0}}
				<label style="color:#da4242;font-weight:bold;">Pending</label>
			{{/if}}
		</td>
	</tr>
</script>

<!--- equipment due cost -->
<script type="text/x-jQuery-tmpl" id="equipment-due-tmpl">
	<tr>
		<td width="30%">${equipment_name}</td>
		<td width="20%">${due_days}</td>
		<td colspan="2" width="30%">${amount_due}</td>
	</tr>
</script>

<!-- Estimated/Generated -->
<script type="text/x-jQuery-tmpl" id="estimated-cost-tmpl">
	<tr>
		<td><b><i>${task_description}</i><span style="background-color:#111;color:#fff;padding:1px;float:right;" id="total-${task_id}" total="task-sub-total"></span></b>
		
		<hr style="margin: 0px 0px;width:97%;display:inline-block;">
			<button type="button" style="width:15px;height:15px;display:inline-block;font-size:40%;color:#333;" onclick="ToggleEstimate(${task_id});">
				<b>V</b>
			</button>
		</td>
	</tr>
	<tr>
		<td style="display:none;" id="task-${task_id}">
			<!--<b>Materials</b>-->
			<table width="100%">
				<thead>
					<tr>
						<th>Materials</th>
						<th>Quantity</th>
						<th>Unit</th>
						<th>Price</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody id="estimated-materials-${task_id}-result">
				</tbody>
			</table>
			<hr>
			
			<table width="100%">
				<thead>
					<tr>
						<th>Equipments</th>
						<th>Quantity</th>
						<th># of Days</th>
						<th>Rate/Day</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody id="estimated-equipments-${task_id}-result">

				</tbody>
			</table>
			<hr>
			
			<table width="100%">
			<thead>
					<tr>
						<th>Manpower</th>
						<th>Quantity</th>
						<th># of Days</th>
						<th>Rate/Day</th>
						<th>Amount</th>
					</tr>
				</thead>
				<tbody id="estimated-manpower-${task_id}-result">

				</tbody>
			</table>
		</td>
	</tr>
</script>

<script type="text/x-jQuery-tmpl" id="estimated-materials-tmpl">
	<tr>
		<td width="100px">${material_name}</td>
		<td width="100px">${tm_quantity}</td>
		<td width="100px">${material_unit}</td>
		<td width="100px">${material_cost}</td>
		<td width="100px">${tm_total}</td>
	</tr>
</script>	
<script type="text/x-jQuery-tmpl" id="estimated-equipments-tmpl">
	<tr>
		<td width="100px">${equipment_name}</td>
		<td width="100px">${te_quantity}</td>
		<td width="100px">${te_days}</td>
		<td width="100px">${equipment_rate_day}</td>
		<td width="100px">${te_total}</td>	
	</tr>
</script>	
<script type="text/x-jQuery-tmpl" id="estimated-manpower-tmpl">
	<tr>
		<td width="100px">${manpower_name}</td>
		<td width="100px">${tmp_quantity}</td>
		<td width="100px">${tmp_days}</td>
		<td width="100px">${manpower_rate_day}</td>
		<td width="100px">${tmp_total}</td>
	<tr>
</script>	

