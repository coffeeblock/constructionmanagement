<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class EquipmentData extends CI_Model{
	public $details;
	public $return_id;
	private $key = "equipment_id";
	private $table = "tbl_equipment";

	function GetAll()
	{
		$sql = "SELECT * FROM ".$this->table." ";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function GetAllByProject($id)
	{
		$sql = "SELECT e.equipment_name, e.equipment_id
				FROM tbl_equipment as e
				INNER JOIN tbl_task_equipment as te
				ON e.equipment_id = te.equipment_id
				INNER JOIN tbl_task as t
				ON t.task_id = te.task_id
				WHERE t.project_id = '$id'
				AND te.te_quantity != 0
				GROUP BY e.equipment_id ";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function GetAllLogs()
	{
		$sql = "SELECT el.el_id as id, el.el_date_returned as date_returned, el.el_date_sent as date_sent,
				CONCAT(u.user_firstname,' ',u.user_lastname) as worker_name, el.el_date_due as date_due,p.project_id,
				IF(el.status = 0, 'Out', 'Returned') as status, e.equipment_name, p.project_name
				FROM tbl_equipment_log as el
				INNER JOIN tbl_user as u
				ON el.user_id = u.user_id
				INNER JOIN tbl_equipment as e
				ON el.equipment_id = e.equipment_id
				INNER JOIN tbl_project as p
				ON p.project_id = el.project_id";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function GetAllEquipmentDue($id)
	{
		$sql = "SELECT log.*, e.equipment_name, e.equipment_rate_day,
				DATEDIFF(log.el_date_returned,log.el_date_due) as due_days,
				(DATEDIFF(log.el_date_returned,log.el_date_due) * e.equipment_rate_day) as amount_due
				FROM tbl_equipment_log as log
				INNER JOIN tbl_equipment as e
				ON log.equipment_id = e.equipment_id
				WHERE log.project_id = '$id'";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}


	function Add($data)
	{
		$query = $this->db->insert($this->table, $data);
		if (@$query)
		{
			$this->return_id = $this->db->insert_id();
			return true;
		}
		else
		{
			return false;
		}
	}

	function AddLog($data)
	{
		$query = $this->db->insert('tbl_equipment_log', $data);
		if (@$query)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function Delete($id)
	{
		$tables = array(
			$this->table);
		$this->db->where($this->key, $id);
		$this->db->delete($tables); 
		
		return true;
	}

	function DeleteLog($id)
	{
		$tables = array(
			'tbl_equipment_log');
		$this->db->where('el_id', $id);
		$this->db->delete($tables); 
		
		return true;
	}

	function GetFields()
	{
		$sql = "SELECT * FROM ".$this->table ." ";

		$query = $this->db->query($sql);
		$fields = $query->list_fields();

		return $fields;
	}

	function GetFieldsLog()
	{
		$sql = "SELECT * FROM tbl_equipment_log";

		$query = $this->db->query($sql);
		$fields = $query->list_fields();

		return $fields;
	}

	function GetDataById($id)
	{
		$sql = "SELECT * FROM ".$this->table." 
				WHERE equipment_id = '$id' ";

		$query = $this->db->query($sql);
		if($query)
		{
			$row = $query->result();
			$result = $row[0];
		}

		return $result;
	}

	function GetLogDataById($id)
	{
		$sql = "SELECT el.el_id, el.el_date_returned, el.el_date_sent,el.equipment_id,
				CONCAT(u.user_firstname,' ',u.user_lastname) as worker_name, el.el_date_due,
				IF(el.status = 0, 'Out', 'Returned') as status, e.equipment_name, p.project_name,
				p.project_id, u.user_id	
				FROM tbl_equipment_log as el
				INNER JOIN tbl_user as u
				ON el.user_id = u.user_id
				INNER JOIN tbl_equipment as e
				ON el.equipment_id = e.equipment_id
				INNER JOIN tbl_project as p
				ON p.project_id = el.project_id 
				WHERE el_id = '$id' ";

		$query = $this->db->query($sql);
		if($query)
		{
			$row = $query->result();
			$result = $row[0];
		}

		return $result;
	}

	function ProcessFields($inputs)
	{
		$result = array();

		$fields = $this->GetFields();
		foreach($fields as $key)
		{
			if(isset($inputs[$key]))
			{
				$result[$key] = $inputs[$key];
			} 
			else
			{
				$result[$key] = "";
			}
		}

		return $result;
	}

	function ProcessFieldsLogs($inputs)
	{
		$result = array();

		$fields = $this->GetFieldsLog();
		foreach($fields as $key)
		{
			if(isset($inputs[$key]))
			{
				$result[$key] = $inputs[$key];
			} 
			else
			{
				$result[$key] = "";
			}
		}

		return $result;
	}

	function Update($data)
	{
		$this->db->where($this->key,$data[$this->key]);
		$query = $this->db->update($this->table, $data);
		if (@$query)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function UpdateLog($data)
	{
		$this->db->where('el_id',$data['el_id']);
		$query = $this->db->update('tbl_equipment_log', $data);
		if (@$query)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>