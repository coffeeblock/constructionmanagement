<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ClientData extends CI_Model{
	public $details;
	public $return_id;
	private $key = "user_id";
	private $table = "tbl_user";

	function GetAll()
	{
		$sql = "SELECT * FROM ".$this->table." 
				WHERE user_type = 3";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}


	function Add($data)
	{
		$query = $this->db->insert($this->table, $data);
		if (@$query)
		{
			$this->return_id = $this->db->insert_id();
			return true;
		}
		else
		{
			return false;
		}
	}

	function Delete($id)
	{
		$tables = array(
			$this->table);
		$this->db->where($this->key, $id);
		$this->db->delete($tables); 
		
		return true;
	}

	function GetFields()
	{
		$sql = "SELECT * FROM ".$this->table ." ";

		$query = $this->db->query($sql);
		$fields = $query->list_fields();

		return $fields;
	}

	function GetDataById($id)
	{
		$sql = "SELECT * FROM ".$this->table." 
				WHERE user_id = '$id' ";

		$query = $this->db->query($sql);
		if($query)
		{
			$row = $query->result();
			$result = $row[0];
		}

		return $result;
	}
	
	function ProcessFields($inputs)
	{
		$result = array();

		$fields = $this->GetFields();
		foreach($fields as $key)
		{
			if(isset($inputs[$key]))
			{
				$result[$key] = $inputs[$key];
			} 
			else
			{
				$result[$key] = "";
			}
		}

		return $result;
	}

	function Update($data)
	{
		$this->db->where($this->key,$data[$this->key]);
		$query = $this->db->update($this->table, $data);
		if (@$query)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>