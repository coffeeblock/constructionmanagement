<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MaterialData extends CI_Model{
	public $details;
	public $return_id;
	private $key = "material_id";
	private $table = "tbl_material";

	function GetAll()
	{
		$sql = "SELECT * FROM ".$this->table." ";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function GetAllByTask($id)
	{
		$sql = "SELECT tm.*, m.material_name
				FROM tbl_task_material as tm
				INNER JOIN tbl_material as m
				ON tm.material_id = m.material_id
				WHERE tm.task_id = '$id'
				AND tm.`status` = 0
				AND tm.tm_quantity != 0";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function GetAllReturnedItems($id)
	{
		$sql = "SELECT ri.*, m.material_name	
				FROM tbl_returned_items as ri
				INNER JOIN tbl_material as m
				ON ri.material_id = m.material_id
				WHERE ri.task_id = '$id'";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}


	function Add($data)
	{
		$query = $this->db->insert($this->table, $data);
		if (@$query)
		{
			$this->return_id = $this->db->insert_id();
			$sql = "INSERT INTO 
						tbl_inventory
					VALUES('', 0, $this->return_id, 0)";

			$query = $this->db->query($sql);
			return true;
		}
		else
		{
			return false;
		}
	}

	function AddReturnedItems($quantity, $material_id, $task_id)
	{
		$sql = "INSERT INTO tbl_returned_items VALUES('', '$quantity', '$material_id', '$task_id', '0')";
		$query = $this->db->query($sql);
		if (@$query)
		{
			$sql = "UPDATE tbl_inventory
					SET inventory_quantity = inventory_quantity + $quantity
					WHERE material_id = $material_id";

			$query = $this->db->query($sql);
			return true;
		}
		else
		{
			return false;
		}
	}

	function Delete($id)
	{
		$tables = array(
			$this->table);
		$this->db->where($this->key, $id);
		$this->db->delete($tables); 
		
		return true;
	}

	function DeleteReturned($id, $quantity)
	{
		$tables = array(
			"tbl_returned_items");
		$this->db->where("ri_id", $id);
		$this->db->delete($tables); 

		$sql = "UPDATE tbl_inventory SET inventory_quantity = inventory_quantity  - $quantity";
		$query = $this->db->query($sql);
		
		return true;
	}

	function GetFields()
	{
		$sql = "SELECT * FROM ".$this->table ." ";

		$query = $this->db->query($sql);
		$fields = $query->list_fields();

		return $fields;
	}

	function GetDataById($id)
	{
		$sql = "SELECT * FROM ".$this->table." 
				WHERE material_id = '$id' ";

		$query = $this->db->query($sql);
		if($query)
		{
			$row = $query->result();
			$result = $row[0];
		}

		return $result;
	}
	function ProcessFields($inputs)
	{
		$result = array();

		$fields = $this->GetFields();
		foreach($fields as $key)
		{
			if(isset($inputs[$key]))
			{
				$result[$key] = $inputs[$key];
			} 
			else
			{
				$result[$key] = "";
			}
		}

		return $result;
	}

	function Update($data)
	{
		$this->db->where($this->key,$data[$this->key]);
		$query = $this->db->update($this->table, $data);
		if (@$query)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>