<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class WorkerData extends CI_Model{
	public $details;
	public $return_id;
	private $key = "user_id";
	private $table = "tbl_user";

	function GetAll()
	{
		$sql = "SELECT * FROM ".$this->table." 
				WHERE user_type = 5";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function GetAllByProject($id)
	{
		$sql = "SELECT CONCAT(u.user_firstname,' ',u.user_lastname) as name, u.user_id
				FROM tbl_user as u
				INNER JOIN tbl_task_worker as w
				ON u.user_id = w.user_id
				INNER JOIN tbl_task as t
				ON w.task_id = t.task_id
				WHERE t.project_id = '$id'";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function GetAllByTask($id)
	{
		$sql = "SELECT tw.tw_id, u.user_id, CONCAT(u.user_firstname,' ',u.user_lastname) as name, m.manpower_name
				FROM tbl_user as u
				INNER JOIN tbl_manpower as m
				ON u.manpower_id = m.manpower_id
				INNER JOIN tbl_task_worker as tw
				ON tw.user_id = u.user_id
				AND tw.task_id = $id ";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function GetAllWorkersByTask($id)
	{
		$sql = "SELECT a.*, CONCAT(u.user_firstname,' ',u.user_lastname) as name 
				FROM tbl_task_worker as a
				INNER JOIN
				tbl_user as u
				ON a.user_id = u.user_id
				WHERE task_id = '$id'";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function GetAllWorkerDeduction($id)
	{
		$sql = "SELECT a.*, CONCAT(u.user_firstname,' ',u.user_lastname) as name,
				m.manpower_rate_day,
				(IF(a.a_am = 0, m.manpower_rate_day/2,0) + IF(a.a_pm = 0, m.manpower_rate_day/2,0)) as deduction
				FROM tbl_attendance as a
				INNER JOIN tbl_task_worker as tw
				ON tw.tw_id = a.tw_id
				INNER JOIN tbl_user as u
				ON u.user_id = tw.user_id
				INNER JOIN tbl_manpower as m
				ON m.manpower_id = u.manpower_id
				WHERE tw.task_id = '$id'";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function UpdateAttendanceById($id, $type, $date, $check)
	{
		if($type == 0)
		{
			$sql = "UPDATE tbl_attendance SET a_am = '$check'
					WHERE tw_id = '$id'
					AND a_date = '$date'";
		}
		else if($type == 1)
		{
			$sql = "UPDATE tbl_attendance SET a_pm = '$check'
					WHERE tw_id = '$id'
					AND a_date = '$date'";
		}

		$query = $this->db->query($sql);
		if($query)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function Add($data)
	{
		$query = $this->db->insert($this->table, $data);
		if (@$query)
		{
			$this->return_id = $this->db->insert_id();
			return true;
		}
		else
		{
			return false;
		}
	}

	function AddTaskWorker($data)
	{
		$query = $this->db->insert('tbl_task_worker', $data);
		$id = $data['user_id'];
		if (@$query)
		{
			$sql = "UPDATE tbl_user SET status = 1 WHERE user_id = $id";
			$this->db->query($sql);
			return true;
		}
		else
		{
			return false;
		}
	}

	function Delete($id)
	{
		$tables = array(
			$this->table);
		$this->db->where($this->key, $id);
		$this->db->delete($tables); 
		
		return true;
	}

	function DeleteTaskWorker($id, $user_id)
	{
		$tables = array(
			'tbl_task_worker');
		$this->db->where('tw_id', $id);
		$this->db->delete($tables); 

		$sql = "UPDATE tbl_user SET status = 0 WHERE user_id = '$user_id'";
		$this->db->query($sql);
		
		return true;
	}

	function GetFields()
	{
		$sql = "SELECT * FROM ".$this->table ." ";

		$query = $this->db->query($sql);
		$fields = $query->list_fields();

		return $fields;
	}

	function GetFieldsTaskWorkers()
	{
		$sql = "SELECT * FROM tbl_task_worker";

		$query = $this->db->query($sql);
		$fields = $query->list_fields();

		return $fields;
	}

	function GetDataById($id)
	{
		$sql = "SELECT * FROM ".$this->table." 
				WHERE user_id = '$id' ";

		$query = $this->db->query($sql);
		if($query)
		{
			$row = $query->result();
			$result = $row[0];
		}

		return $result;
	}
	function ProcessFields($inputs)
	{
		$result = array();

		$fields = $this->GetFields();
		foreach($fields as $key)
		{
			if(isset($inputs[$key]))
			{
				$result[$key] = $inputs[$key];
			} 
			else
			{
				$result[$key] = "";
			}
		}

		return $result;
	}

	function ProcessFieldsTaskWorkers($inputs)
	{
		$result = array();

		$fields = $this->GetFieldsTaskWorkers();
		foreach($fields as $key)
		{
			if(isset($inputs[$key]))
			{
				$result[$key] = $inputs[$key];
			} 
			else
			{
				$result[$key] = "";
			}
		}

		return $result;
	}

	function GetAllWorkerKey($type)
	{
		$sql = "SELECT u.user_id, CONCAT(u.user_firstname,' ',u.user_lastname) as name
				FROM ".$this->table." as u
				INNER JOIN
				tbl_manpower as m
				ON u.manpower_id = m.manpower_id
				WHERE 
				 (
				(user_type > 2 AND user_type <= 5)
				AND
				(u.manpower_id = '$type')
				AND u.status != 1)

				";
		
		$query = $this->db->query($sql);
		
		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function Update($data)
	{
		$this->db->where($this->key,$data[$this->key]);
		$query = $this->db->update($this->table, $data);
		if (@$query)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

}

?>