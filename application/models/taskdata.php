<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class TaskData extends CI_Model{
	public $details;
	public $return_id;
	private $key = "task_id";
	private $table = "tbl_task";

	function GetAll($id)
	{
		$sql = "SELECT * FROM ".$this->table.
				" WHERE project_id = $id
				AND (status = 0 OR status = 2 OR status = 3)
				ORDER BY task_start_date";
		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		//get project name
		$sql = "SELECT project_name FROM tbl_project WHERE project_id = '$id'";
		$query = $this->db->query($sql);
		$row = $query->result();
		$result['project_name'] = $row[0];
		

		return $result;
	}

	function GetAllEstimates($id)
	{
		$sql = "SELECT * FROM ".$this->table.
				" WHERE project_id = $id
				AND (status = 1)
				ORDER BY task_start_date";
		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		//get project name
		$sql = "SELECT project_name FROM tbl_project WHERE project_id = '$id'";
		$query = $this->db->query($sql);
		$row = $query->result();
		$result['project_name'] = $row[0];
		

		return $result;
	}

	function GetGenerated($id)
	{
		$sql = "SELECT * FROM ".$this->table.
				" WHERE project_id = $id
				AND status = 1";
		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();

		return $result;
	}

	function GetTask($id)
	{
		$sql = "SELECT * FROM tbl_task WHERE task_id = $id";

		$query = $this->db->query($sql);

		if($query)
		{
			$row = $query->result();
			$result = $row[0]; 
		}

		return $result;
	}

	function GetMaterialByTask($id, $status)
	{
		$sql = "SELECT 
				m.material_id, m.material_name, m.material_unit,
				tm.tm_quantity, tm.tm_total,tm.tm_id,tm.task_id,
				m.material_cost
				FROM tbl_material as m
				INNER JOIN
					tbl_task_material as tm
				ON
					m.material_id = tm.material_id
				WHERE
					tm.task_id = $id
				AND
					tm.status = $status
				AND 
					(tm.tm_quantity != 0
					OR
					tm.tm_total !=0)";
		$query = $this->db->query($sql);
		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		
		return $result;
	}

	function GetMaterialByTaskSync($id, $status)
	{
		$sql = "SELECT 
				m.material_id, m.material_name, m.material_unit,
			
				(tm.tm_total - ((IFNULL((SELECT ri_quantity FROM tbl_returned_items ri 
				WHERE ri.material_id = tm.material_id AND ri.task_id = tm.task_id),0)) * m.material_cost)) as tm_total,
				tm.tm_id,tm.task_id,
				m.material_cost,
				(tm.tm_quantity - (IFNULL((SELECT ri_quantity FROM tbl_returned_items ri 
				WHERE ri.material_id = tm.material_id AND ri.task_id = tm.task_id),0)) )
				 as tm_quantity
				FROM tbl_material as m
				INNER JOIN
					tbl_task_material as tm
				ON
					m.material_id = tm.material_id
				WHERE
					tm.task_id = $id
				AND
					tm.status = $status
				AND 
					(tm.tm_quantity != 0
					OR
					tm.tm_total !=0)";
		$query = $this->db->query($sql);
		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		
		return $result;
	}

	function SetTaskDone($id)
	{
		$sql = "UPDATE ".$this->table.
				" SET status = 2
				WHERE task_id = '$id'";
		$query = $this->db->query($sql);

		if($query)
			return true;
		else
			return false;

	}

	function SetTaskStart($id)
	{
		$sql = "UPDATE ".$this->table.
				" SET status = 3
				WHERE task_id = '$id'";
		$query = $this->db->query($sql);

		if($query)
			return true;
		else
			return false;

	}

	function GetEquipmentByTask($id, $status)
	{
		$sql = "SELECT 
				e.equipment_id,e.equipment_name, te.te_quantity,
				te.te_days, te.te_id, te.te_total,te.te_id, te.task_id,
				e.equipment_rate_day
				FROM tbl_equipment as e
				INNER JOIN
					tbl_task_equipment as te
				ON
					e.equipment_id = te.equipment_id
				WHERE
					te.task_id = $id
				AND
					te.status = $status
				AND
					(te.te_quantity != 0
					OR
					te.te_total != 0)";
		$query = $this->db->query($sql);
		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		
		return $result;
	}

	function GetManpowerByTask($id, $status)
	{
		$sql = "SELECT 
				m.manpower_id,m.manpower_name, tmp.tmp_days, m.manpower_rate_day,
				tmp.tmp_quantity, tmp.tmp_total, tmp.tmp_id, tmp.task_id
				FROM tbl_manpower as m
				INNER JOIN
					tbl_task_manpower as tmp
				ON
					m.manpower_id = tmp.manpower_id
				WHERE
					tmp.task_id = $id
				AND
					tmp.status = $status
				AND
					(tmp.tmp_quantity != 0
					OR
					tmp.tmp_total != 0)";
		$query = $this->db->query($sql);
		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		
		return $result;
	}


	function Add($task_name, $task_start_date, $task_end_date,
							$project_id,$strMat, $strEquip, $strMan)
	{
		$sql = "INSERT INTO tbl_task VALUES('','$task_name','','$task_start_date',
											'$task_end_date', '$project_id','')";
		$query = $this->db->query($sql);
		$this->return_id = $this->db->insert_id();
		if (@$query)
		{
			$materials = array();
			$equipments = array();
			$manpower = array();
			//Get cost
			$mat_rows = $this->ProcessMaterials();
			$equip_rows = $this->ProcessEquipment();
			$man_rows = $this->ProcessManpower();
			// convert string to array
			//Materials
			$arr = explode(":", $strMat);
			foreach($arr as $key)
			{
				$r = explode(',',$key);
				$i = 0;
				foreach($r as $k)
				{
					$f = explode('=',$k);
						$p = array();
						@$g[$f[0]] = $f[1];
				}
				foreach($mat_rows as $rows)
				{
					if($g['material_id'] == $rows['material_id'])
					{
						$g['tm_total'] = $rows['material_cost']*$g['tm_quantity'];
						$check = $this->CheckInventory($g['material_id'],$g['tm_quantity'],'');
						if($check == false)
						{
							return  false;
						}
					}
					$i++;
				}
				$g['task_id'] = $this->return_id;
				array_push($materials, $g);

			}
			//Equipments
			$arr2 = explode(":", $strEquip);
			foreach($arr2 as $key)
			{
				$r2 = explode(',',$key);
				foreach($r2 as $k)
				{
					$f2 = explode('=',$k);
						$p = array();
						@$g2[$f2[0]] = $f2[1];
				}
				foreach($equip_rows as $rows)
				{
					if($g2['equipment_id'] == $rows['equipment_id'])
						$g2['te_total'] = $rows['equipment_rate_day']*$g2['te_days']*$g2['te_quantity'];
					$i++;
				}
				$g2['task_id'] = $this->return_id;
				array_push($equipments, $g2);
			}
			//Manpower
			$arr3 = explode(":", $strMan);
			foreach($arr3 as $key)
			{
				$r3 = explode(',',$key);
				foreach($r3 as $k)
				{
					$f3 = explode('=',$k);
						$p = array();
						@$g3[$f3[0]] = $f3[1];
				}
				foreach($man_rows as $rows)
				{
					if($g3['manpower_id'] == $rows['manpower_id'])
						$g3['tmp_total'] = $rows['manpower_rate_day']*$g3['tmp_days']*$g3['tmp_quantity'];
					$i++;
				}
				$g3['task_id'] = $this->return_id;
				array_push($manpower, $g3);
			}
			
			//insert all data
			$query = $this->db->insert_batch('tbl_task_material', $materials);
			$query = $this->db->insert_batch('tbl_task_equipment', $equipments);
			$query = $this->db->insert_batch('tbl_task_manpower', $manpower);

			if($query)
				return true;
			else
				return false;		
		}
		else
		{
			return false;
		}
	}

	function CheckInventory($id, $quantity, $task_id)
	{
		$sql = "SELECT i.inventory_quantity - SUM(tm.tm_quantity) as inventory
				FROM tbl_task_material as tm
				INNER JOIN
					tbl_inventory as i
				ON i.material_id = tm.material_id
				WHERE i.material_id = $id";
		$query = $this->db->query($sql);

		if($task_id != '')
		{
			$sql2 = "SELECT tm_quantity
					FROM tbl_task_material
					WHERE
					task_id = $task_id
					AND
					material_id = $id";
			$query2 = $this->db->query($sql2);
			$row2 = $query2->result();
			$tm_quantity = @$row2[0]->tm_quantity; 
		}

		if(@$query)
		{
			$row = $query->result();
			$result = $row[0]; 
			if($task_id != '')
			{
				if($quantity > $tm_quantity)
				{
					$quantity = $quantity - $tm_quantity;
					if(($row[0]->inventory - abs($quantity)) < 0)
					{
						return false;
					}
					else
					{
						return true;
					}
				}
				else
				{
					return true;
				}
			}
			else
			{
				if(($row[0]->inventory - abs($quantity)) < 0)
					{
						return false;
					}
					else
					{
						return true;
					}
			}
		}
		else
		{
			return false;
		}

	}

	function Update($task_id, $task_name, $task_start_date, $task_end_date
					,$strMat, $strEquip, $strMan)
	{
		$sql = "UPDATE 
					tbl_task 
				SET task_description = '$task_name', task_start_date = '$task_start_date',
					task_end_date = '$task_end_date'
				WHERE
					task_id = '$task_id'";
		$query = $this->db->query($sql);
		if (@$query)
		{
			$materials = array();
			$equipments = array();
			$manpower = array();
			//Get cost
			$mat_rows = $this->ProcessMaterials();
			$equip_rows = $this->ProcessEquipment();
			$man_rows = $this->ProcessManpower();
			// convert string to array
			//Materials
			$arr = explode(":", $strMat);
			foreach($arr as $key)
			{
				$r = explode(',',$key);
				$i = 0;
				foreach($r as $k)
				{
					$f = explode('=',$k);
						$p = array();
						@$g[$f[0]] = $f[1];
				}
				foreach($mat_rows as $rows)
				{
					if($g['material_id'] == $rows['material_id'])
					{
						$g['tm_total'] = $rows['material_cost']*$g['tm_quantity'];
						$check = $this->CheckInventory($g['material_id'],$g['tm_quantity'],$task_id);
						if($check == false)
						{
							return  false;
						}
					}
				}
				array_push($materials, $g);

			}
			//Equipments
			$arr2 = explode(":", $strEquip);
			foreach($arr2 as $key)
			{
				$r2 = explode(',',$key);
				foreach($r2 as $k)
				{
					$f2 = explode('=',$k);
						$p = array();
						@$g2[$f2[0]] = $f2[1];
				}
				foreach($equip_rows as $rows)
				{
					if($g2['equipment_id'] == $rows['equipment_id'])
						$g2['te_total'] = $rows['equipment_rate_day']*$g2['te_days']*$g2['te_quantity'];
				}
				array_push($equipments, $g2);
			}
			//Manpower
			$arr3 = explode(":", $strMan);
			foreach($arr3 as $key)
			{
				$r3 = explode(',',$key);
				foreach($r3 as $k)
				{
					$f3 = explode('=',$k);
						$p = array();
						@$g3[$f3[0]] = $f3[1];
				}
				foreach($man_rows as $rows)
				{
					if($g3['manpower_id'] == $rows['manpower_id'])
						$g3['tmp_total'] = $rows['manpower_rate_day']*$g3['tmp_days']*$g3['tmp_quantity'];
				}
				array_push($manpower, $g3);
			}
			
			//insert all data
			$this->db->where("task_id = $task_id");
			$query = $this->db->update_batch('tbl_task_material', $materials, 'material_id');
			$this->db->where("task_id = $task_id");
			$query = $this->db->update_batch('tbl_task_equipment', $equipments, 'equipment_id');
			$this->db->where("task_id = $task_id");
			$query = $this->db->update_batch('tbl_task_manpower', $manpower, 'manpower_id');

				return true;	
		}
		else
		{
			return false;
		}
	}

	function ProcessMaterials()
	{
		$sql = "SELECT * FROM tbl_material";
		$query = $this->db->query($sql);

		$result = $query->result();
		$result = json_decode(json_encode($result), true);

		return $result;
	}

	function ProcessEquipment()
	{
		$sql = "SELECT * FROM tbl_equipment";
		$query = $this->db->query($sql);

		$result = $query->result();
		$result = json_decode(json_encode($result), true);

		return $result;
	}

	function ProcessManpower()
	{
		$sql = "SELECT * FROM tbl_manpower";
		$query = $this->db->query($sql);

		$result = $query->result();
		$result = json_decode(json_encode($result), true);

		return $result;
	}

	function Delete($id)
	{
		$tables = array($this->table,
						'tbl_task_material',
						'tbl_task_equipment',
						'tbl_task_manpower');
		$this->db->where($this->key, $id);
		$this->db->delete($tables); 
		
		return true;
	}

	function GetFields()
	{
		$sql = "SELECT * FROM ".$this->table ." ";

		$query = $this->db->query($sql);
		$fields = $query->list_fields();

		return $fields;
	}

	function GetDataById($id)
	{
		$sql = "CALL GetProjectById($id)";

		$query = $this->db->query($sql);
		if($query)
		{
			$row = $query->result();
			$result = $row[0]; 
		}

		return $result;
	}

	function GetByUserId($id)
	{
		$sql = "CALL GetProjectByUserId($id)";

		$query = $this->db->query($sql);
		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		
		return $result;
	}

	function DateAvailability($task_start_date, $task_end_date, $project_id)
	{
		$sql = "SELECT task_start_date, task_end_date
				FROM ".$this->table." 
				WHERE project_id = '$project_id'";

		$query = $this->db->query($sql);
		$result['rows'] = $query->result();

		foreach($result['rows'] as $key)
		{
			if(($task_start_date >= $key->task_start_date) && ($task_end_date <= $key->task_end_date))
			{
				return false;
			}
		}

		return true;

	}

	function ProcessFields($inputs)
	{
		$result = array();

		$fields = $this->GetFields();
		foreach($fields as $key)
		{
			if(isset($inputs[$key]))
			{
				$result[$key] = $inputs[$key];
			} 
			else
			{
				$result[$key] = "";
			}
		}

		return $result;
	}

}
?>