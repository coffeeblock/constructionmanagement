<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ProjectData extends CI_Model{
	public $details;
	public $return_id;
	private $key = "project_id";
	private $table = "tbl_project";

	function GetAll()
	{
		$sql = "SELECT * FROM view_project";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		return $result;
	}

	function GetTask($id)
	{
		$sql = "SELECT * FROM tbl_task WHERE task_id = '$id'";

		$query = $this->db->query($sql);

		if($query)
		{
			$row = $query->result();
			$result = $row[0]; 
		}

		return $result;
	}

	function GetEstimateItems($id)
	{
		$sql = "SELECT t.task_id,t.task_description,IF(t.task_description='Sub-grade',
				FORMAT(tm.tm_quantity,2),FORMAT((tm.tm_quantity/1.15),2)) as quantity
				FROM tbl_task_material as tm
				INNER JOIN tbl_task as t
				ON
					tm.task_id = t.task_id
				AND
					t.project_id = $id

				GROUP BY t.task_id";

		$query = $this->db->query($sql);

		$result['rows'] = $query->result();
		return $result;
	}

	function GetMaterialByTask($id)
	{
		$sql = "SELECT 
				m.material_id, tm.tm_quantity,tm.tm_id
				FROM tbl_material as m
				INNER JOIN
					tbl_task_material as tm
				ON
					m.material_id = tm.material_id
				WHERE
					tm.task_id = $id";
		$query = $this->db->query($sql);
		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		
		return $result;
	}

	function UpdateProjectDuration($id)
	{
		$sql = "SELECT MAX(tm.tmp_days) as duration
				FROM tbl_task_manpower as tm
				INNER JOIN tbl_task as t
				ON tm.task_id = t.task_id
				WHERE t.project_id = '$id'
				AND tm.`status` = 1";

		$query = $this->db->query($sql);
		$result['rows'] = $query->result();
		$duration =  $result['rows'][0]->duration;


		if(@$query)
		{
			$sql1 = "UPDATE tbl_project 
					SET project_end_date = DATE_ADD(project_start_date,INTERVAL $duration DAY) 
					WHERE project_id = '$id' ";
			$this->db->query($sql1);
		}
		
		return $duration;
	}

	/*function UpdateTaskDuration($id)
	{
		$sql = "SELECT MAX(tm.tmp_days) as duration
				FROM tbl_task_manpower as tm
				INNER JOIN tbl_task as t
				ON tm.task_id = t.task_id
				WHERE tm.task_id = '$id'
				AND tm.`status` = 1";

	}*/

	function GetEquipmentByTask($id)
	{
		$sql = "SELECT 
				e.equipment_id, te.te_days,te.te_id
				FROM tbl_equipment as e
				INNER JOIN
					tbl_task_equipment as te
				ON
					e.equipment_id = te.equipment_id
				WHERE
					te.task_id = $id";
		$query = $this->db->query($sql);
		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		
		return $result;
	}

	function GetManpowerByTask($id)
	{
		$sql = "SELECT 
				m.manpower_id, tmp.tmp_days,tmp.tmp_id
				FROM tbl_manpower as m
				INNER JOIN
					tbl_task_manpower as tmp
				ON
					m.manpower_id = tmp.manpower_id
				WHERE
					tmp.task_id = $id";
		$query = $this->db->query($sql);
		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		
		return $result;
	}


	function Add($data, $engineer_id, $client_id)
	{
		$query = $this->db->insert($this->table, $data);
		if (@$query)
		{
			$this->return_id = $this->db->insert_id();

			$this->db->trans_start();
			$sql1 = "INSERT INTO tbl_project_engineer 
					VALUES('', '$engineer_id', $this->return_id, 0)";
			$this->db->query($sql1);
			$sql2 = "INSERT INTO tbl_project_client 
					VALUES('', '$client_id', $this->return_id, 0)";
			$this->db->query($sql2);
			$this->db->trans_complete();

			return true;
		}
		else
		{
			return false;
		}
	} 

	function GetTaskId($project_id, $task_description)
	{
		$sql = "SELECT task_id
				FROM tbl_task
				WHERE
					project_id = '$project_id'	
				AND
					task_description = '$task_description'";

		$query = $this->db->query($sql);
		if($query)
		{
			$row = $query->result();
			$result = $row[0]->task_id; 
		}

		return $result;
	}


	function AddEstimateTask($data)
	{
		$query = $this->db->insert('tbl_task', $data);
		$task_id = $this->db->insert_id();
		if ($query)
		{
			return $task_id;
		}
		else
			return '';

	}

	function AddEstimateModules($materials, $equipments, $manpower)
	{
		$mat_rows = $this->ProcessMaterials();
		$equip_rows = $this->ProcessEquipment();
		$man_rows = $this->ProcessManpower();
		$i = 0;
		foreach($materials as $mat => $array)
		{
				foreach($mat_rows as $rows)
				{
					if($materials[$mat]['material_id'] == $rows['material_id'])
					{
						$materials[$mat]['tm_total'] = $materials[$mat]['tm_quantity']*$rows['material_cost'];

					}
				}
		}
		
		foreach ($equipments as $equip => $array) 
		{
			foreach($equip_rows as $rows)
			{
				if($equipments[$equip]['equipment_id'] == $rows['equipment_id'])
				{
					$equipments[$equip]['te_total'] = $equipments[$equip]['te_quantity']*$equipments[$equip]['te_days']*$rows['equipment_rate_day'];	
				}
			}
		}

		foreach($manpower as $man => $array)
		{
			foreach($man_rows as $rows)
			{
				if($manpower[$man]['manpower_id'] == $rows['manpower_id'])
				{
					$manpower[$man]['tmp_total'] = $manpower[$man]['tmp_quantity']*$manpower[$man]['tmp_days']*$rows['manpower_rate_day'];
				}
			}
		}

		$query = $this->db->insert_batch('tbl_task_material', $materials);
		$query = $this->db->insert_batch('tbl_task_equipment', $equipments);
		$query = $this->db->insert_batch('tbl_task_manpower', $manpower);

		return $materials;
	}

	function UpdateEstimateModules($task_id, $materials, $equipments, $manpower)
	{
		$mat_rows = $this->ProcessMaterials();
		$equip_rows = $this->ProcessEquipment();
		$man_rows = $this->ProcessManpower();
		$i = 0;
		foreach($materials as $mat => $array)
		{
				foreach($mat_rows as $rows)
				{
					if($materials[$mat]['material_id'] == $rows['material_id'])
					{
						$materials[$mat]['tm_total'] = $materials[$mat]['tm_quantity']*$rows['material_cost'];

					}
				}
		}
		
		foreach ($equipments as $equip => $array) 
		{
			foreach($equip_rows as $rows)
			{
				if($equipments[$equip]['equipment_id'] == $rows['equipment_id'])
				{
					$equipments[$equip]['te_total'] = $equipments[$equip]['te_quantity']*$equipments[$equip]['te_days']*$rows['equipment_rate_day'];	
				}
			}
		}

		foreach($manpower as $man => $array)
		{
			foreach($man_rows as $rows)
			{
				if($manpower[$man]['manpower_id'] == $rows['manpower_id'])
				{
					$manpower[$man]['tmp_total'] = $manpower[$man]['tmp_quantity']*$manpower[$man]['tmp_days']*$rows['manpower_rate_day'];
				}
			}
		}

		$this->db->where("task_id = $task_id");	
		$query = $this->db->update_batch('tbl_task_material', $materials, 'material_id');
		$this->db->where("task_id = $task_id");
		$query = $this->db->update_batch('tbl_task_equipment', $equipments, 'equipment_id');
		$this->db->where("task_id = $task_id");
		$query = $this->db->update_batch('tbl_task_manpower', $manpower, 'manpower_id');

		return $materials;
	}

	function ProcessMaterials()
	{
		$sql = "SELECT * FROM tbl_material";
		$query = $this->db->query($sql);

		$result = $query->result();
		$result = json_decode(json_encode($result), true);

		return $result;
	}

	function ProcessEquipment()
	{
		$sql = "SELECT * FROM tbl_equipment";
		$query = $this->db->query($sql);

		$result = $query->result();
		$result = json_decode(json_encode($result), true);

		return $result;
	}

	function ProcessManpower()
	{
		$sql = "SELECT * FROM tbl_manpower";
		$query = $this->db->query($sql);

		$result = $query->result();
		$result = json_decode(json_encode($result), true);

		return $result;
	}

	function GetTotalCostById($id, $status)
	{
		$sql = "SELECT t.status, t.task_start_date, t.task_description,tm.task_id,tm.materials, te.equipments, tmp.manpower,
				 (IFNULL((
				 		tm.materials -
				 			IFNULL((
							 	SELECT SUM(ri.ri_quantity*matt.material_cost)
							 	FROM tbl_returned_items as ri
							 	INNER JOIN tbl_task_material as tmat
							 	ON ri.task_id = tmat.task_id
							 	INNER JOIN tbl_material as matt
							 	ON tmat.material_id = matt.material_id
							 	WHERE ri.material_id = tmat.material_id
							 ),0)
				 ),0) + te.equipments + tmp.manpower) as total_cost,
				 IF(eq_days > man_days, eq_days, man_days) as total_days,
				 IFNULL((
				 		tm.materials -
				 			IFNULL((
							 	SELECT SUM(ri.ri_quantity*matt.material_cost)
							 	FROM tbl_returned_items as ri
							 	INNER JOIN tbl_task_material as tmat
							 	ON ri.task_id = tmat.task_id
							 	INNER JOIN tbl_material as matt
							 	ON tmat.material_id = matt.material_id
							 	WHERE ri.material_id = tmat.material_id
							 ),0)
				 ),0) as remaining_materials
				FROM(
					SELECT  task_id,sum(tm_total) as materials
					FROM tbl_task_material
					GROUP BY task_id) tm
				INNER JOIN(
					SELECT  MAX(te_days) eq_days, task_id,sum(te_total) as equipments
					FROM tbl_task_equipment
					GROUP BY task_id) te
				INNER JOIN(
					SELECT MAX(tmp_days) as man_days, task_id,sum(tmp_total) as manpower
					FROM tbl_task_manpower
					GROUP BY task_id) tmp
				ON tm.task_id = te.task_id
				INNER JOIN tbl_task t
				ON t.task_id = tm.task_id
				WHERE t.task_id = te.task_id
				AND t.task_id = tmp.task_id
				AND t.project_id = '$id'

				AND (t.status = 0 OR t.status = 2 OR t.status = 3)
				
				ORDER BY t.task_start_date";

		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();

		return $result;
	}


	function GetProjectTasks($id)
	{
		$sql = "SELECT * FROM tbl_task WHERE project_id=$id";
		$query = $this->db->query($sql);

		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();

		return $result;
	}

	function Delete($id)
	{
		$tables = array(
			$this->table,
			'tbl_project_engineer',
			'tbl_project_client');
		$this->db->where($this->key, $id);
		$this->db->delete($tables); 
		
		return true;
	}

	function GetFields()
	{
		$sql = "SELECT * FROM ".$this->table ." ";

		$query = $this->db->query($sql);
		$fields = $query->list_fields();

		return $fields;
	}

	function GetDataById($id)
	{
		$sql = "CALL GetProjectById($id)";

		$query = $this->db->query($sql);
		if($query)
		{
			$row = $query->result();
			$result = $row[0]; 
		}

		return $result;
	}

	function GetByUserId($id)
	{
		$sql = "CALL GetProjectByUserId($id)";

		$query = $this->db->query($sql);
		$result['total'] = $query->num_rows();
		$result['rows'] = $query->result();
		
		return $result;
	}

	function ProcessFields($inputs)
	{
		$result = array();

		$fields = $this->GetFields();
		foreach($fields as $key)
		{
			if(isset($inputs[$key]))
			{
				$result[$key] = $inputs[$key];
			} 
			else
			{
				$result[$key] = "";
			}
		}

		return $result;
	}

	function Update($data, $engineer_id, $client_id)
	{
		$key_value = $data[$this->key];

		$this->db->where($this->key,$data[$this->key]);
		$query = $this->db->update($this->table, $data);
		if (@$query)
		{
			$this->db->trans_start();
			$sql1 = "UPDATE tbl_project_engineer
						SET user_id = '$engineer_id'
						WHERE project_id = '$key_value'";
			$this->db->query($sql1);
			$sql2 = "UPDATE tbl_project_client
						SET user_id = '$client_id' 
						WHERE project_id = '$key_value'";
			$this->db->query($sql2);
			$this->db->trans_complete();

				return true;
		}
		else
		{
			return false;
		}
	}

	function AddLog($id, $remarks)
	{
		$sql = "INSERT INTO tbl_logs VALUES('', $remarks, $id, 0)";
		$query = $this->query->db($sql);

		if($query)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
?>