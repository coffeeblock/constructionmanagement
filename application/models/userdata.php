<?php
if (! defined('BASEPATH')) exit('No direct script access allowed');

class UserData extends CI_Model{
	public $details;
	private $table = "tbl_user";
	private $key = "user_id";

	function login($username, $password)
	{
		$sql = "SELECT * FROM ".$this->table."
				WHERE
				user_username = '$username'
				AND
				user_password_hash = '".md5($password)."'
				";

		$query = $this->db->query($sql);

		if($query->num_rows() == 1)
		{
			$result = $query->result();
			$this->details = $result[0];
			$this->set_session();
			return true;
		}
		else
		{
			return false;
		}
	}

	function Update($data)
	{
		$this->db->where($this->key,$data[$this->key]);
		$query = $this->db->update($this->table, $data);
		if (@$query)
		{
			return $data[$this->key];
		}
		else
		{
			return false;
		}
	}

	function GetFields()
	{
		$sql = "SELECT * FROM ".$this->table ." ";

		$query = $this->db->query($sql);
		$fields = $query->list_fields();

		return $fields;
	}

	function ProcessFields($inputs)
	{
		$result = array();

		$fields = $this->GetFields();
		foreach($fields as $key)
		{
			if(isset($inputs[$key]))
			{
				$result[$key] = $inputs[$key];
			} 
			else
			{
				$result[$key] = "";
			}
		}

		return $result;
	}

	function AddWorker($data)
	{
		$query = $this->db->insert($this->table, $data);
		if (@$query)
		{
			$this->return_id = $this->db->insert_id();
			return true;
		}
		else
		{
			return false;
		}
	}

	function GetDataById($id)
	{
		$sql = "SELECT * FROM ".$this->table." 
				WHERE user_id = '$id' ";

		$query = $this->db->query($sql);
		if($query)
		{
			$row = $query->result();
			$result = $row[0];
		}

		return $result;
	}

	function CheckPasswordAvailaility($password)
	{
		$sql = "SELECT * FROM ".$this->table."
				WHERE
				user_password_hash = '".md5($password)."'
				";
		$query = $this->db->query($sql);

		$result = $query->num_rows();

		if($result != 0)
		{
			return false;
		}
		else
		{
			return true;
		}
	}

	function set_session()
	{
		$data_arr = array(
					"loggedin" => true,
					"id" => $this->details->user_id,
					"firstname" => $this->details->user_firstname,
					"lastname" => $this->details->user_lastname,
					"username" => $this->details->user_username,
					"user_type" => $this->details->user_type,
					"age" => $this->details->user_age,
					"gender" => $this->details->user_gender,
					"contact_number" => $this->details->user_contact_number,
					"address" => $this->details->user_address,
					);

		$this->session->set_userdata($data_arr);
	}

	function unset_session()
	{
		//unset user data
		$this->session->unset_userdata('loggedin');
		$this->session->unset_userdata('id');
		$this->session->unset_userdata('firstname');
		$this->session->unset_userdata('lastname');
		$this->session->unset_userdata('username');
		$this->session->unset_userdata('user_type');
		$this->session->unset_userdata('age');
		$this->session->unset_userdata('gender');
		$this->session->unset_userdata('contact_number');
		$this->session->unset_userdata('address');
		
		return true;
	}

	function verifyLogin()
	{
		if($this->session->userdata('loggedin'))
		{
			return true;
		}
		else
		{
			return false;
		}
	}


}

?>