if( typeof (System) === "undefined"){
	System = {};
}

System.Client = function System$Client() {
	var $this = this;
	var controller = "client";

	//elements
	var $table = $("#client_table");
	var $tbody = $("#client-list-result");
	var $tmpl = $("#client-list-tmpl");
	var $tip = $("#add-client-tip");
	var $rand_btn = $("#rand-client-pass-btn");
	var $client_btns = $("#client-btns");
	var $fields;

	var $client_id = $("#client_id");
	var $client_firstname = $("#client_firstname");
	var $client_lastname = $("#client_lastname");
	var $client_username = $("#client_username");
	var $client_password = $("#client_password");
	var $client_age = $("#client_age");
	var $client_gender = $("#client_gender");
	var	$client_contact_number = $("#client_contact_number");
	var $client_address = $("#client_address");

	$this.Load = function System$$Load()
	{
		$this.Fields();
	}

	$this.Initialize = function System$$Initialize(id)
	{
		$this.Clear();

		$.post(controller + '/Initialize',
		{
			id : id
		},
		function(data)
		{
			var result = eval("("+data+")");
			var list = result.data;

			$client_id.val(list.user_id);
			$client_firstname.val(list.user_firstname);
			$client_lastname.val(list.user_lastname);
			$client_username.val(list.user_username);
			$client_password.val(list.user_password_text);
			$client_age.val(list.user_age);
			$client_gender.val(list.user_gender);
			$client_contact_number.val(list.user_contact_number);
			$client_address.val(list.user_address);

			openModal('client-modal');
		});
	}


	$this.Remove = function System$$Remove(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/Remove',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListView();
			});
		}
	}

	$this.ListView = function System$$ListView()
	{
		$.post(controller + '/ListView',
		{
			//null
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl.tmpl(list);
			$tbody.html($tmplResult);

			$table.dataTable();
		});
	}

	$this.ProjectListView = function System$$ListView()
	{
		var id = $("#client_id").val();
		$.post('project/ListById',
		{
			id : id
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			// populate template
			var $tmplResult = $("#client-project-list-tmpl").tmpl(list);
			$("#client-projects-result").html($tmplResult);
		});
	}

	$this.Save = function System$$Save()
	{
		var client_firstname = $client_firstname.val();
		var client_lastname = $client_lastname.val();
		var client_username = $client_username.val();
		var client_password = $client_password.val();	
		var client_age = $client_age.val();
		var client_contact_number = $client_contact_number.val();
		var client_gender = $client_gender.val();
		var client_address = $client_address.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Save',
		{
			user_firstname : client_firstname,
			user_lastname : client_lastname,
			user_username : client_username,
			user_password_text : client_password,
			user_password_hash : '',
			user_age : client_age,
			user_gender : client_gender,
			user_contact_number : client_contact_number,
			user_address : client_address,
			user_type : 3,
			status : 0
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);

			closeModal('client-modal');
			$this.ListView();
			window.location.href = "project";
		});
	}

	$this.Update = function System$$Update(id)
	{
		var id = $client_id.val();
		var firstname = $client_firstname.val();
		var lastname = $client_lastname.val();
		var username = $client_username.val();
		var password = $client_password.val();
		var age = $client_age.val();
		var gender = $client_gender.val();
		var contact_number = $client_contact_number.val();
		var address = $client_address.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Edit',
		{
			user_id : id,
			user_firstname : firstname,
			user_lastname : lastname,
			user_username : username,
			user_password_text : password,
			user_age : age,
			user_gender : gender,
			user_contact_number : contact_number,
			user_address : address,
			user_type: 3
		},
		function(data)
		{
			var result = eval("("+data+")");
			alert(result.message);

			closeModal('client-modal');
			$this.ListView();
		});

	}

	$this.Fields = function System$$Fields()
	{
		var $temp = $();

		$temp = $temp.add($client_id);
		$temp = $temp.add($client_firstname);
		$temp = $temp.add($client_lastname);
		$temp = $temp.add($client_username);
		$temp = $temp.add($client_password);
		$temp = $temp.add($client_age);
		$temp = $temp.add($client_gender);
		$temp = $temp.add($client_contact_number);
		$temp = $temp.add($client_address);

		$this.$fields = $temp;

	}

	$this.Validation = function System$$Validate()
	{
		var retVal = true;

		Validate($tip, $client_firstname, "Firstname is required.");
		Validate($tip, $client_lastname, "Lastname is required.");
		Validate($tip, $client_username, "Username is required.");
		Validate($tip, $client_password, "Password is required.");
		Validate($tip, $client_age, "Age is required.");
		Validate($tip, $client_gender, "Gender is required.");
		Validate($tip, $client_contact_number, "Contact number is required.");
		Validate($tip, $client_address, "Address is required.");

		$this.ClearError();

		return retVal;
	}

	$this.Clear = function System$$Clear()
	{
		$this.ClearError();

		$this.$fields.val("");
	}

	$this.EnableReadonly = function System$$EnableReadonly()
	{
		$this.$fields.prop('readonly', true);
		$client_gender.attr('disabled', true);
		$client_btns.attr('disabled', true);
		$rand_btn.attr('disabled', true);
	}

	$this.DisableReadonly = function System$$DisableReadonly()
	{
		$this.$fields.prop('readonly', false);
		$client_gender.attr('disabled', false);
		$client_btns.attr('disabled', false);
		$rand_btn.attr('disabled', false);
	}

	$this.ClearError = function System$$ClearError()
	{
		$this.$fields.removeClass("system-error");

		$tip.html("");
	}
}