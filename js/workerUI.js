var worker;
$(function()
{
	worker = new System.Worker();

	worker.ListView();
	worker.Fields();

	//Add worker
	$("#add-worker-btn").click(function()
	{
		worker.Clear();
		openModal('worker-modal');
	});

	//Save Info
	$("#worker-btns").click(function()
	{
		var id = $("#worker_id").val();
		if(id != "")
			worker.Update();
		else
			worker.Save();
	});

});

function WorkerInitialize(id)
{
	worker.DisableReadonly();
	worker.Initialize(id);
}

function WorkerRemove(id)
{
	worker.Remove(id);
}

function PrintworkerList()
{
	window.open(window.location.origin + '/print/print_worker_list.php', '_blank');
}
