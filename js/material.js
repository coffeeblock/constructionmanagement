if( typeof (System) === "undefined"){
	System = {};
}

System.Material = function System$Material() {
	var $this = this;
	var controller = "material";

	//elements
	var $table = $("#material_table");
	var $tbody = $("#material-list-result");
	var $tmpl = $("#material-list-tmpl");
	var $tip = $("#add-material-tip");
	var $rand_btn = $("#rand-material-pass-btn");
	var $material_btns = $("#material-btns");
	var $fields;

	var $material_id = $("#material_id");
	var $material_name = $("#material_name");
	var $material_unit = $("#material_unit");
	var $material_cost = $("#material_cost");

	$this.Load = function System$$Load()
	{
		$this.Fields();
	}

	$this.Initialize = function System$$Initialize(id, status)
	{
		$this.Clear();

		if(status == 1)
			$("#material_name").attr('disabled','disabled');
		else
			$("#material_name").removeAttr('disabled');

		$.post(controller + '/Initialize',
		{
			id : id
		},
		function(data)
		{
			var result = eval("("+data+")");
			var list = result.data;

			$material_id.val(list.material_id);
			$material_name.val(list.material_name);
			$material_unit.val(list.material_unit);
			$material_cost.val(list.material_cost);

			openModal('material-modal');
		});
	}


	$this.Remove = function System$$Remove(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/Remove',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListView();
			});
		}
	}

	$this.ListView = function System$$ListView()
	{
		$.post(controller + '/ListView',
		{
			//null
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl.tmpl(list);
			$tbody.html($tmplResult);

			$table.dataTable();
			$('[status="1"]').hide();
		});

	}

	$this.Save = function System$$Save()
	{
		var material_name = $material_name.val();
		var material_unit = $material_unit.val();
		var material_cost = $material_cost.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Save',
		{
			material_name : material_name,
			material_unit : material_unit,
			material_cost : material_cost
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);

			closeModal('material-modal');
			$this.ListView();
		});
	}

	$this.Update = function System$$Update(id)
	{
		var id = $material_id.val();
		var name = $material_name.val();
		var unit = $material_unit.val();
		var cost = $material_cost.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Edit',
		{
			material_id : id,
			material_name : name,
			material_unit : unit,
			material_cost : cost
		},
		function(data)
		{
			var result = eval("("+data+")");
			alert(result.message);

			closeModal('material-modal');
			$this.ListView();
		});

	}

	$this.Fields = function System$$Fields()
	{
		var $temp = $();

		$temp = $temp.add($material_id);
		$temp = $temp.add($material_name);
		$temp = $temp.add($material_unit);
		$temp = $temp.add($material_cost);

		$this.$fields = $temp;

	}

	$this.Validation = function System$$Validate()
	{
		var retVal = true;

		Validate($tip, $material_name, "Name is required.");
		Validate($tip, $material_unit, "Unit is required.");
		Validate($tip, $material_cost, "Cost is required.");

		$this.ClearError();

		return retVal;
	}

	$this.Clear = function System$$Clear()
	{
		$this.ClearError();

		$this.$fields.val("");
	}

	$this.EnableReadonly = function System$$EnableReadonly()
	{
		$this.$fields.prop('readonly', true);
		$material_btns.attr('disabled', true);
		$rand_btn.attr('disabled', true);
	}

	$this.DisableReadonly = function System$$DisableReadonly()
	{
		$this.$fields.prop('readonly', false);
		$material_btns.attr('disabled', false);
		$rand_btn.attr('disabled', false);
	}

	$this.ClearError = function System$$ClearError()
	{
		$this.$fields.removeClass("system-error");

		$tip.html("");
	}
}