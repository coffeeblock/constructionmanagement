if( typeof (System) === "undefined"){
	System = {};
}

System.Engineer = function System$Engineer() {
	var $this = this;
	var controller = "engineer";

	//elements
	var $table = $("#engineer_table");
	var $tbody = $("#engineer-list-result");
	var $tmpl = $("#engineer-list-tmpl");
	var $tip = $("#add-engr-tip");
	var $rand_btn = $("#rand-pass-btn");
	var $engineer_btns = $("#engineer-btns");
	var $fields;

	var $engr_id = $("#engr_id");
	var $engr_firstname = $("#engr_firstname");
	var $engr_lastname = $("#engr_lastname");
	var $engr_rate = $("#engr_rate");
	var $engr_username = $("#engr_username");
	var $engr_password = $("#engr_password");
	var $engr_age = $("#engr_age");
	var $engr_gender = $("#engr_gender");
	var $engr_contact_number = $("#engr_contact_number");
	var $engr_address = $("#engr_address");

	$this.Load = function System$$Load()
	{
		$this.Fields();
	}

	$this.Initialize = function System$$Initialize(id)
	{
		$this.Clear();

		$.post(controller + '/Initialize',
		{
			id : id
		},
		function(data)
		{
			var result = eval("("+data+")");
			var list = result.data;

			$engr_id.val(list.user_id);
			$engr_firstname.val(list.user_firstname);
			$engr_lastname.val(list.user_lastname);
			$engr_rate.val(list.user_rate);
			$engr_username.val(list.user_username);
			$engr_password.val(list.user_password_text);
			$engr_age.val(list.user_age);
			$engr_gender.val(list.user_gender);
			$engr_contact_number.val(list.user_contact_number);
			$engr_address.val(list.user_address);

			openModal('engineer-modal');
		});
	}


	$this.Remove = function System$$Remove(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/Remove',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListView();
			});
		}
	}

	$this.ListView = function System$$ListView()
	{
		$.post(controller + '/ListView',
		{
			//null
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl.tmpl(list);
			$tbody.html($tmplResult);

			$table.dataTable();
		});
	}

	$this.ProjectListView = function System$$ListView()
	{
		var id = $("#engr_id").val();
		$.post('project/ListById',
		{
			id : id
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			// populate template
			var $tmplResult = $("#engineer-project-list-tmpl").tmpl(list);
			$("#engineer-projects-result").html($tmplResult);
		});
	}

	$this.Save = function System$$Save()
	{
		var engr_firstname = $engr_firstname.val();
		var engr_lastname = $engr_lastname.val();
		var engr_rate = $engr_rate.val();
		var engr_username = $engr_username.val();
		var engr_password = $engr_password.val();	
		var engr_age = $engr_age.val();
		var engr_contact_number = $engr_contact_number.val();
		var engr_gender = $engr_gender.val();
		var engr_address = $engr_address.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Save',
		{
			user_firstname : engr_firstname,
			user_lastname : engr_lastname,
			user_username : engr_username,
			user_rate : engr_rate,
			user_password_text : engr_password,
			user_password_hash : '',
			user_age : engr_age,
			user_gender : engr_gender,
			user_contact_number : engr_contact_number,
			user_address : engr_address,
			user_type : 2,
			status : 0
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);

			closeModal('engineer-modal');
			$this.ListView();
		});
	}

	$this.Update = function System$$Update(id)
	{
		var id = $engr_id.val();
		var firstname = $engr_firstname.val();
		var lastname = $engr_lastname.val();
		var rate = $engr_rate.val();
		var username = $engr_username.val();
		var password = $engr_password.val();
		var age = $engr_age.val();
		var gender = $engr_gender.val();
		var contact_number = $engr_contact_number.val();
		var address = $engr_address.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Edit',
		{
			user_id : id,
			user_firstname : firstname,
			user_lastname : lastname,
			user_rate : rate,
			user_username : username,
			user_password_text : password,
			user_age : age,
			user_gender : gender,
			user_contact_number : contact_number,
			user_address : address,
			user_type: 2
		},
		function(data)
		{
			var result = eval("("+data+")");
			alert(result.message);

			closeModal('engineer-modal');
			$this.ListView();
		});

	}

	$this.Fields = function System$$Fields()
	{
		var $temp = $();

		$temp = $temp.add($engr_id);
		$temp = $temp.add($engr_firstname);
		$temp = $temp.add($engr_lastname);
		$temp = $temp.add($engr_rate);
		$temp = $temp.add($engr_username);
		$temp = $temp.add($engr_password);
		$temp = $temp.add($engr_age);
		$temp = $temp.add($engr_gender);
		$temp = $temp.add($engr_contact_number);
		$temp = $temp.add($engr_address);

		$this.$fields = $temp;

	}

	$this.Validation = function System$$Validate()
	{
		var retVal = true;

		Validate($tip, $engr_firstname, "Firstname is required.");
		Validate($tip, $engr_lastname, "Lastname is required.");
		Validate($tip, $engr_rate, "Rate per day is required.");
		Validate($tip, $engr_username, "Username is required.");
		Validate($tip, $engr_password, "Password is required.");
		Validate($tip, $engr_age, "Age is required.");
		Validate($tip, $engr_gender, "Gender is required.");
		Validate($tip, $engr_contact_number, "Contact number is required.");
		Validate($tip, $engr_address, "Address is required.");

		$this.ClearError();

		return retVal;
	}

	$this.Clear = function System$$Clear()
	{
		$this.ClearError();

		$this.$fields.val("");
	}

	$this.EnableReadonly = function System$$EnableReadonly()
	{
		$this.$fields.prop('readonly', true);
		$engr_gender.attr('disabled', true);
		$engineer_btns.attr('disabled', true);
		$rand_btn.attr('disabled', true);
	}

	$this.DisableReadonly = function System$$DisableReadonly()
	{
		$this.$fields.prop('readonly', false);
		$engr_gender.attr('disabled', false);
		$engineer_btns.attr('disabled', false);
		$rand_btn.attr('disabled', false);
	}

	$this.ClearError = function System$$ClearError()
	{
		$this.$fields.removeClass("system-error");

		$tip.html("");
	}
}