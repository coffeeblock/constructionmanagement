function openModal(id)
{
	$('#'+id).show();
}

function closeModal(id)
{
	$('#'+id).hide();
}

function Validate($tip, $element, message)
{
	var text = $element.val();

	if($.trim(text) == "" || $.trim(text) == "null")
	{
		$element.addClass('system-error');
		$element.focus();
		$tip.html(message);
		$tip.css({"color":"red"});

		throw new Error('This is not an error. This is just to abort javascript');
	}
}

Number.prototype.zeroPad = function()
{
	return ('0'+this).slice(-2);
}

function GeneratePassword(id)
{
	var $id = $("#" + id);
	$.post('user/GeneratePassword',{},
	function(data)
	{
		var result = eval("("+ data +")");
		$id.val(result.data.password);
	});
}

function GetPopulateDropdown( controller , id , parameter, callback)
{
	var $id = $("#"+id);
	$.post(controller , parameter , function(data){
		var result = eval("("+data+")");
			
			$id.html("");
			for(var i in result.data)
			{
				$id.append(
                 $('<option></option>')
                    .val(result.data[i].key)
                    .html(result.data[i].value));
            }

          if(callback != null && typeof(callback) == "function")
          {
          	callback(true);
          }
	});
}

function NumberOnly(evt, obj) {
        var charCode = (evt.which) ? evt.which : event.keyCode;

        if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        } else {
            if ($.trim($(obj).val()) == "" && charCode == 46) //-- Not allowed to enter dot first.
                return false;

            if (charCode == 46 && $(obj).val().indexOf('.') != -1) { //-- Checking if this value has already a decimal point
                return false;
            }
            return true;
        }
};
