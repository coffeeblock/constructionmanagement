var client;
$(function()
{
	client = new System.Client();

	client.ListView();
	client.Fields();

	//Add client modal
	$("#add-client-btn").click(function()
	{	
		client.DisableReadonly();
		client.Clear();
		$("#client-modal-header").text('Add client');
		$("#client-projects-tbl").hide();
		$("#client-project-header").hide();
		GeneratePassword('client_password');
		openModal('client-modal');
	});

		//Save Info
	$("#client-btns").click(function()
	{
		var id = $("#client_id").val();
		if(id != "")
			client.Update();
		else
			client.Save();
	});

	$("#client-header").click(function()
	{
		$("#client-info").show();
		$("#client-projects-tbl").hide();
	});

	$("#client-project-header").click(function()
	{
		client.ProjectListView();
		$("#client-projects-tbl").show();
		$("#client-info").hide();
	});

	//generate password
	$("#rand-client-pass-btn").click(function()
	{
		GeneratePassword('client_password');
	});

});

function Initialize(id)
{
	client.DisableReadonly();
	client.Initialize(id);
	$("#client-modal-header").text('Edit client');
	$("#client-project-header").hide();
	$("#client-projects-tbl").hide();
	$("#client-info").show();
}

function Remove(id)
{
	client.Remove(id);
}

function ViewUser(id)
{
	client.EnableReadonly();
	client.Initialize(id);
	$("#client-project-header").show();
	$("#client-projects-tbl").hide();
	$("#client-info").show();
}

function PrintClientList()
{
	window.open(window.location.origin + '/print/print_client_list.php', '_blank');
}