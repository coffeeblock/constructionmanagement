if( typeof (System) === "undefined"){
	System = {};
}

System.Profile = function System$Profile() {
	var $this = this;
	var controller = "user";

	//elements
	var $tip = $("#add-profile-tip");
	var $profile_btns = $("#profile-btns");
	var $admin_control = $(".admin-control");
	var $fields;

	//fields
	var $user_id = $("#user_id");
	var $user_type = $("#user_type");
	var $user_firstname = $("#user_firstname");
	var $user_lastname = $("#user_lastname");
	var	$user_rate = $("#engr_rate");
	var $user_username = $("#user_username");
	var $user_password = $("#user_password");
	var $user_age = $("#user_age");
	var $user_gender = $("#user_gender");
	var $user_contact_number = $("#user_contact_number");
	var $user_address = $("#user_address");
	var $user_rate_tr = $("#rate-tr");


	$this.Load = function System$$Load()
	{
		$this.Fields();

		//hide or show rate field
		var user_type = $user_type.val();
		if(user_type == 2)
		{
			$user_rate_tr.show();
		}
		else
		{
			$user_rate_tr.hide();
		}
	}

	$this.Initialize = function System$$Initialize(mode)
	{
		var type;	
		if(mode == 1)
			type = 'engr';
		else if(mode == 2)
			type = 'client'
		else
			$this.Clear();

		var id = $user_id.val();
		$.post(controller + '/Initialize',
		{
			id : id
		},
		function(data)
		{
			var result = eval("("+data+")");
			var list = result.data;

			if(mode == 0)
			{
				$user_id.val(list.user_id);
				$user_firstname.val(list.user_firstname);
				$user_lastname.val(list.user_lastname);
				$user_username.val(list.user_username);
				$user_password.val(list.user_password_text);
				$user_age.val(list.user_age);
				$user_gender.val(list.user_gender);
				$user_contact_number.val(list.user_contact_number);
				$user_address.val(list.user_address);
			}
			else
			{
				//Client or Engr modal elements
				$("#" + type + "_id").val(list.user_id);
				$("#" + type + "_firstname").val(list.user_firstname);
				$("#" + type + "_lastname").val(list.user_lastname);
				$("#" + type + "_username").val(list.user_username);
				$("#" + type + "_rate").val(list.user_rate);
				$("#" + type + "_password").val(list.user_password_text);
				$("#" + type + "_age").val(list.user_age);
				$("#" + type + "_gender").val(list.user_gender);
				$("#" + type + "_contact_number").val(list.user_contact_number);
				$("#" + type + "_address").val(list.user_address);
			}
			
		});
	}


	$this.Remove = function System$$Remove(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/Remove',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListView();
			});
		}
	}



	$this.Update = function System$$Update(mode)
	{
		var type;
		if(mode == 1)
			type = 'engr';
		else 
			type = 'client';

		var id = $("#" + type + "_id").val();
		var user_type = $("#" + type + "_usertype").val();
		var firstname = $("#" + type + "_firstname").val();
		var lastname = $("#" + type + "_lastname").val();
		var username = $("#" + type + "_username").val();
		var rate = $("#" + type + "_rate").val();
		var password = $("#" + type + "_password").val();
		var age = $("#" + type + "_age").val();
		var gender = $("#" + type + "_gender").val();
		var contact_number = $("#" + type + "_contact_number").val();
		var address = $("#" + type + "_address").val()

		if(!$this.Validation(type))
		{
			return;
		}
		
		$.post(controller + '/Edit',
		{
			user_id : id,
			user_firstname : firstname,
			user_lastname : lastname,
			user_username : username,
			user_rate : rate,
			user_password_text : password,
			user_password_hash : '',
			user_age : age,
			user_gender : gender,
			user_contact_number : contact_number,
			user_address : address,
			user_type : user_type
		},
		function(data)
		{
			var result = eval("("+data+")");
			alert(result.message);

			closeModal('engineer-modal');
			closeModal('client-modal');
			$this.Initialize(0);
		});

	}

	$this.Fields = function System$$Fields()
	{
		var $temp = $();

		$temp = $temp.add($user_firstname);
		$temp = $temp.add($user_lastname);
		$temp = $temp.add($user_username);
		$temp = $temp.add($user_password);
		$temp = $temp.add($user_age);
		$temp = $temp.add($user_gender);
		$temp = $temp.add($user_contact_number);
		$temp = $temp.add($user_address);

		$this.$fields = $temp;

	}

	$this.Validation = function System$$Validate(type)
	{
		var retVal = true;

		Validate($tip, $("#" + type + "_firstname"), "Firstname is required.");
		Validate($tip, $("#" + type + "_lastname"), "Lastname is required.");
		Validate($tip, $("#" + type + "_username"), "Username is required.");
		Validate($tip, $("#" + type + "_password"), "Password is required.");
		Validate($tip, $("#" + type + "_age"), "Age is required.");
		Validate($tip, $("#" + type + "_gender"), "Gender is required.");
		Validate($tip, $("#" + type + "_contact_number"), "Contact number is required.");
		Validate($tip, $("#" + type + "_address"), "Address is required.");

		$this.ClearError();

		return retVal;
	}

	$this.Clear = function System$$Clear()
	{
		$this.ClearError();

		$this.$fields.val("");
	}

	$this.EnableReadonly = function System$$EnableReadonly()
	{
		$this.$fields.prop('readonly', true);
		$user_gender.attr('disabled', true);
		$profile_btns.attr('disabled', true);
		$rand_btn.attr('disabled', true);
	}

	$this.DisableReadonly = function System$$DisableReadonly()
	{
		$this.$fields.prop('readonly', false);
		$user_gender.attr('disabled', false);
		$profile_btns.attr('disabled', false);
		$rand_btn.attr('disabled', false);
	}

	$this.ClearError = function System$$ClearError()
	{
		$this.$fields.removeClass("system-error");

		$tip.html("");
	}

}