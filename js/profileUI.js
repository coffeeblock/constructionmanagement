var profile;
$(function()
{
	profile = new System.Profile();

	profile.Load();
	profile.Initialize(0);

	//Edit profile
	$("#edit-profile-btn").click(function()
	{
		var user_type = $("#user_type").val();
		$("#client-modal-header").text('Edit profile');

		if(user_type == 2)
		{
			profile.Initialize(1);	
			openModal('engineer-modal');
		}
		else
		{	
			profile.Initialize(2);
			openModal("client-modal");
		}
	});

	$("#engineer-btns").click(function()
	{
		profile.Update(1);
	});

	$("#client-btns").click(function()
	{
		profile.Update(2);
	});
})