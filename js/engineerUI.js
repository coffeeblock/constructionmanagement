var engineer;
$(function(){
	engineer = new System.Engineer();
	
	engineer.Load();
	engineer.ListView();
	
	//Add engineer modal
	$("#add-engr-btn").click(function()
	{	
		engineer.DisableReadonly();
		engineer.Clear();
		GeneratePassword('engr_password');
		openModal('engineer-modal');
	});

	//Save Info
	$("#engineer-btns").click(function()
	{
		var id = $("#engr_id").val();
		if(id != "")
			engineer.Update();
		else
			engineer.Save();
	});

	$("#engineer-header").click(function()
	{
		$("#engineer-info").show();
		$("#engineer-projects-tbl").hide();
	});

	$("#engineer-project-header").click(function()
	{
		engineer.ProjectListView();
		$("#engineer-projects-tbl").show();
		$("#engineer-info").hide();
	});


	//generate password
	$("#rand-pass-btn").click(function()
	{
		GeneratePassword('engr_password');
	});
});

function Initialize(id)
{
	engineer.DisableReadonly();
	engineer.Initialize(id);
	$("#engineer-project-header").hide();
	$("#engineer-projects-tbl").hide();
	$("#engineer-info").show();
}

function Remove(id)
{
	engineer.Remove(id);
}

function ViewUser(id)
{
	engineer.EnableReadonly();
	engineer.Initialize(id);
	$("#engineer-project-header").show();
	$("#engineer-projects-tbl").hide();
	$("#engineer-info").show();
}

function PrintEngineerList()
{
	window.open(window.location.origin + '/print/print_engineer_list.php', '_blank');
}




