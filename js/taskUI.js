var task;
$(function()
{
	task = new System.Task();

	var id = $("#project_id").val();
	var type = "";


	task.Load();
	task.ListView();
	task.MaterialList();
	task.EquipmentList();
	task.ManpowerList();
	task.Fields();

	GetPopulateDropdown("project/ProjectDropdown", "task_project_id", {}, function(data){});

	$("#done-btns").hide();
	$("#start-btns").hide();
	//alert(window.project_id);

	//Add task modal
	$("#add-task-btn").click(function()
	{	
		$("#task-sub-header").hide();
		$("#task-details").show();
		$("#start-btns").hide();
		$("#task-returned-materials").hide();
		$("#task-header").text('Add Task');
		$("#task-sub-total").hide();
		task.DisableReadonly();
		task.Clear();
		openModal('task-modal');
	});

	$("#add-returned-item").click(function()
	{	
		task.AddReturnedItems();
	});	

		//Save Info
	$("#task-btns").click(function()
	{
		var id = $("#task_id").val();
		if(id != "")
			task.Update(id);
		else
			task.Save();
	});

	//generate password
	$("#rand-task-pass-btn").click(function()
	{
		GeneratePassword('task_password');
	});

	$("#task-header").click(function()
	{
		$("#task-returned-materials").hide();
		$("#task-details").show();
		$("#task-sub-total").hide();
		$("#task-workers").hide();
	});

	$("#task-sub-header").click(function()
	{
		$("#task-returned-materials").hide();
		$("#task-details").hide();
		$("#task-sub-total").show();
		$("#start-btns").hide();
		$("#done-btns").hide();
		$("#task-workers").hide();
		task.ViewTaskCost();
	});

	$("#task-worker-subheader").click(function()
	{
		var id = $("#task_id").val();
		GetPopulateDropdown("manpower/ListByTask", "manpower_id", {id : id}, function(data){});
		$("#task-details").hide();
		$("#task-sub-total").hide();
		$("#start-btns").hide();
		$("#done-btns").hide();
		$("#task-workers").show();
		$("#task-returned-materials").hide();
		task.WorkerListView();
		task.WorkerDeductionListView();
	});

	$("#task-returned-subheader").click(function()
	{
		$("#task-returned-materials").show();
		task.ReturnedListView();
		$("#start-btns").hide();
		$("#done-btns").hide();
		GetPopulateDropdown("material/ListViewByTask", "returned_id", {id : $("#task_id").val()}, function(data){});
		$("#task-details").hide();
		$("#task-sub-total").hide();
		$("#task-workers").hide();
	});

	$("#done-btns").click(function()
	{
		task.Done();
	});

	$("#start-btns").click(function()
	{
		task.Start();
	});

	$("#add-task-worker").click(function()
	{
		task.AddWorker();
	});

	$("#manpower_id").change(function()
	{
		type = $("#manpower_id").val();
		GetPopulateDropdown("worker/KeyValueListWorkerKey", "task_worker_name", {type: type}, function(data){alert(data);});
		//alert(type);
	});

	/*$("#task_worker_name").autocomplete({
        source: function (request, response) {
			
            $.post("worker/KeyValueListWorkerKey", {
                'keyword': request.term,
                'type' : type
            },
            function (data) {
            		//alert(data);
                      var result = eval("("+data+")");
                      response($.map(result.data, function (item) {
                          return {
                              label: item.value,
                              value: item.value,
                              id: item.key
                          }
                      }));
                  });
        },
        minLength: 0,
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function (event, ui) {
			  $("#task_worker_name").val(ui.item.value);
			  $("#worker_id").val(ui.item.id);
			  return false;
        }
    }).keypress(function (event, ui) {
		//$("#supplier_name").val("");
    }).focus(function (){
		  //$(this).data("autocomplete").search($(this).val());
    });*/
});

function Initialize(id)
{
	$("#task-header").text('Task Details');
	$("#task-sub-header").hide();
	$("#task-details").show();
	$("#task-workers").hide();
	$("#task-returned-materials").hide();
	task.DisableReadonly();
	task.Initialize(id,function(data)
	{
		openModal('task-modal');
		task.WorkerDeductionListView(id);
	});
	GetPopulateDropdown("manpower/ListByTask", "manpower_id", {id : id}, function(data){});

	$("#task-sub-total").hide();
}

function Remove(id)
{
	task.Remove(id);
}

function RemoveTaskWorker(id, user_id)
{
	task.RemoveTaskWorker(id, user_id);
}

function View(id)
{
	$("#task-header").text('Task Details');
	$("#task-header").show();
	$("#task-sub-header").show();
	$("#task-sub-header").text('Total Cost');
	$("#task-details").show();
	$("#task-returned-materials").hide();
	task.EnableReadonly();
	task.Initialize(id,function(data)
	{
		openModal('task-modal');
		task.WorkerDeductionListView(id);
	});
	$("#task-workers").hide();
	$("#task-sub-total").hide();

}

function PrintTaskList()
{
	var id = $("#project_id").val();
	window.open(window.location.origin + '/print/print_task_list.php?id='+id , '_blank');
}

function RemoveReturned(id, quantity)
{
	task.RemoveReturned(id, quantity);
}