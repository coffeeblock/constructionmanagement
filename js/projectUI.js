var project;
$(function()
{
	project = new System.Project()
	project.Load()
	project.ListView();

	//Add project modal
	$("#add-project-btn").click(function()
	{	
		$("#project-estimate-header").hide();
		$("#estimated-cost").hide();
		$("#print-estimated-btn").hide();
		$("#project-header").text('Add project');
		$("#project-modal .left").css('width','100%');
		project.MinimizeModal();
		$("#project-total").hide();
		project.DisableReadonly();
		project.Clear();
		openModal('project-modal');
	});

	$("#project-btns").click(function()
	{
		var project_id = $("#project_id").val();

		project.DisableReadonly();
		if(project_id != "")
			project.Update();
		else
			project.Save();

		project.MinimizeModal();
		
	});

	$("#project-header").click(function()
	{
		$("#project-details").show();
		$("#estimated-cost").hide();
		$("#print-estimated-btn").hide();
	});

	$('.modal-title a').click(function (event) {
	  event.preventDefault();
	  // or use return false;
	});

	$("#done-btns").click(function()
	{
		
	});
	
	GetPopulateDropdown("engineer/OptionList", "project_engineer", {}, function(data){});
	GetPopulateDropdown("client/OptionList", "project_client", {}, function(data){});
});

function Initialize(id)
{
	$("#project-estimate-header").hide();
	$("#project-header").text('Edit Project');
	$("#project-details").show();
	$("#estimated-cost").hide();
	$("#print-estimated-btn").hide();
	project.MinimizeModal();
	$("#project-total").hide();
	project.DisableReadonly();
	project.Initialize(id,function(data)
	{
		openModal('project-modal');
	});
}

function Remove(id)
{
	project.Remove(id);
}

function ViewProject(id)
{
	$("#project-header").show();
	$("#project-estimate-header").show();
	$("#project-total").show();
	$("#print-estimated-btn").hide();
	$("#project-header").text('Project Details');
	$("#project-estimate-header").text('Estimated');
	$("#project-details").show();
	$("#estimated-cost").hide();
	project.MaximizeModal();
	project.EnableReadonly();
	project.Initialize(id,function(data)
	{
		openModal('project-modal');
		project.ViewTotal(id);	
		project.ListEquipmentDue();
	});

}

function ViewTask(id)
{
	var task_window = window.open(window.location.origin+
					'/constructionmanagement.git/task?view_id=' + id, "_blank");
	//task_window.project_id = id;
}

function ViewEstimates(id)
{
	var task_window = window.open(window.location.origin+
					'/constructionmanagement.git/task/estimate?view_id=' + id, "_blank");
	//task_window.project_id = id;
}

function ViewEstimated()
{
	$("#project-details").hide();
	$("#estimated-cost").show();
	$("#print-estimated-btn").show();
	project.Estimated();
}

function ToggleEstimate(id)
{
	$("#task-" + id).toggle();
}

function PrintProjectList()
{
	window.open(window.location.origin + '/print/print_project_list.php', '_blank');
}

function PrintEstimates()
{
	var id = $("#project_id").val();
	window.open(window.location.origin + '/print/print_estimated.php?id='+id, '_blank');
}