if( typeof (System) === "undefined"){
	System = {};
}

System.inventory = function System$inventory() {
	var $this = this;
	var controller = "inventory";

	//elements
	var $table = $("#inventory_table");
	var $tbody = $("#inventory-list-result");
	var $tmpl = $("#inventory-list-tmpl");
	var $tip = $("#add-inventory-tip");
	var $rand_btn = $("#rand-inventory-pass-btn");
	var $inventory_btns = $("#inventory-btns");
	var $fields;

	var $inventory_id = $("#inventory_id");
	var $material_id = $("#material_id");
	var $inventory_quantity = $("#inventory_quantity");

	$this.Load = function System$$Load()
	{
		$this.Fields();
	}

	$this.Initialize = function System$$Initialize(id)
	{
		$this.Clear();

		$.post(controller + '/Initialize',
		{
			id : id
		},
		function(data)
		{
			var result = eval("("+data+")");
			var list = result.data;

			$inventory_id.val(list.inventory_id);
			$material_id.val(list.material_id);
			$inventory_quantity.val(list.inventory_quantity);

			openModal('inventory-modal');
		});
	}


	$this.Remove = function System$$Remove(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/Remove',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListView();
			});
		}
	}

	$this.ListView = function System$$ListView()
	{
		$.post(controller + '/ListView',
		{
			//null
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl.tmpl(list);
			$tbody.html($tmplResult);

			$table.dataTable();
		});
	}

	$this.Save = function System$$Save()
	{

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Save',
		{
			material_id : $material_id.val(),
			inventory_quantity : $inventory_quantity.val()
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);

			closeModal('inventory-modal');
			$this.ListView();
		});
	}

	$this.Update = function System$$Update(id)
	{
		var id = $inventory_id.val();
		var name = $material_id.val();
		var unit = $inventory_quantity.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Edit',
		{
			inventory_id : id,
			material_id : name,
			inventory_quantity : unit
		},
		function(data)
		{
			var result = eval("("+data+")");
			alert(result.message);

			closeModal('inventory-modal');
			$this.ListView();
		});

	}

	$this.Fields = function System$$Fields()
	{
		var $temp = $();

		$temp = $temp.add($inventory_id);
		$temp = $temp.add($material_id);
		$temp = $temp.add($inventory_quantity);

		$this.$fields = $temp;

	}

	$this.Validation = function System$$Validate()
	{
		var retVal = true;

		Validate($tip, $material_id, "Name is required.");
		Validate($tip, $inventory_quantity, "Unit is required.");

		$this.ClearError();

		return retVal;
	}

	$this.Clear = function System$$Clear()
	{
		$this.ClearError();

		$this.$fields.val("");
	}

	$this.EnableReadonly = function System$$EnableReadonly()
	{
		$this.$fields.prop('readonly', true);
		$inventory_btns.attr('disabled', true);
		$rand_btn.attr('disabled', true);
	}

	$this.DisableReadonly = function System$$DisableReadonly()
	{
		$this.$fields.prop('readonly', false);
		$inventory_btns.attr('disabled', false);
		$rand_btn.attr('disabled', false);
	}

	$this.ClearError = function System$$ClearError()
	{
		$this.$fields.removeClass("system-error");

		$tip.html("");
	}
}