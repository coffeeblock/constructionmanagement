if( typeof (System) === "undefined"){
	System = {};
}

System.Equipment = function System$Equipment() {
	var $this = this;
	var controller = "equipment";

	//elements
	var $table = $("#equipment_table");
	var $table_logs = $("#equipment_logs_table");
	var $tbody = $("#equipment-list-result");
	var $tbody_logs = $("#equipment-log-result");
	var $tmpl = $("#equipment-list-tmpl");
	var $tmpl_logs = $("#equipment-log-tmpl");
	var $tip = $("#add-equipment-tip");
	var $rand_btn = $("#rand-equipment-pass-btn");
	var $equipment_btns = $("#equipment-btns");
	var $fields;

	var $equipment_id = $("#equipment_id");
	var $equipment_name = $("#equipment_name");
	var $equipment_rate_day = $("#equipment_rate_day");

	$this.Load = function System$$Load()
	{
		$this.Fields();
	}

	$this.Initialize = function System$$Initialize(id, status)
	{
		$this.Clear();

		if(status == 1)
			$("#equipment_name").attr('disabled','disabled');
		else
			$("#equipment_name").removeAttr('disabled');

		$.post(controller + '/Initialize',
		{
			id : id
		},
		function(data)
		{
			var result = eval("("+data+")");
			var list = result.data;

			$equipment_id.val(list.equipment_id);
			$equipment_name.val(list.equipment_name);
			$equipment_rate_day.val(list.equipment_rate_day);

			openModal('equipment-modal');
		});
	}

	$this.InitializeLog = function System$$Initialize(id, callback)
	{
		$this.Clear();

		$.post(controller + '/InitializeLog',
		{
			id : id
		},
		function(data)
		{
			alert(data);
			var result = eval("("+data+")");
			var list = result.data;

			$("#el_id").val(list.el_id);
			$("#project_id").val(list.project_id);
			$("#el_date_due").val(list.el_date_due);
			$("#el_date_sent").val(list.el_date_sent);
			$("#el_date_returned").val(list.el_date_returned);
			$("#worker_id").val(list.user_id);
			$("#equipment_log_id").val(list.equipment_id);

			if(callback != null && typeof(callback) == "function")
			{
				callback(true);
			}

		});
	}

	$this.Remove = function System$$Remove(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/Remove',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListView();
			});
		}
	}

	$this.RemoveLog = function System$$RemoveLog(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/RemoveLog',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListViewLogs();
			});
		}
	}

	$this.ListView = function System$$ListView()
	{
		$.post(controller + '/ListView',
		{
			//null
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl.tmpl(list);
			$tbody.html($tmplResult);

			$table.dataTable();
			$('[status="1"]').hide();
		});

	}

	$this.ListViewLogs = function System$$ListView()
	{
		$.post(controller + '/ListViewLogs',
		{
			//null
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$table_logs.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl_logs.tmpl(list);
			$tbody_logs.html($tmplResult);

			$table_logs.dataTable();
		});

	}

	$this.Save = function System$$Save()
	{
		var equipment_name = $equipment_name.val();
		var equipment_rate_day = $equipment_rate_day.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Save',
		{
			equipment_name : equipment_name,
			equipment_rate_day : equipment_rate_day
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);

			closeModal('equipment-modal');
			$this.ListView();
		});
	}

	$this.SaveLog = function System$$SaveLog()
	{

		/*if(!$this.Validation())
		{
			return;
		}*/
		//alert($("#el_id").val());

		$.post(controller + '/SaveLogs',
		{
			el_id : $("#el_id").val(),
			el_date_sent : $("#el_date_sent").val(),
			el_date_returned : $("#el_date_returned").val(),
			el_date_due : $("#el_date_due").val(),
			equipment_id : $("#equipment_log_id").val(),
			project_id : $("#project_id").val(),
			user_id : $("#worker_id").val()
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);

			closeModal('equipment-log-modal');
			$this.ListViewLogs();
		});
	}

	$this.Update = function System$$Update(id)
	{
		var id = $equipment_id.val();
		var name = $equipment_name.val();
		var rate = $equipment_rate_day.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Edit',
		{
			equipment_id : id,
			equipment_name : name,
			equipment_rate_day : rate
		},
		function(data)
		{
			var result = eval("("+data+")");
			alert(result.message);

			closeModal('equipment-modal');
			$this.ListView();
		});

	}

	$this.Fields = function System$$Fields()
	{
		var $temp = $();

		$temp = $temp.add($equipment_id);
		$temp = $temp.add($equipment_name);
		$temp = $temp.add($equipment_rate_day);

		$this.$fields = $temp;

	}

	$this.Validation = function System$$Validate()
	{
		var retVal = true;

		Validate($tip, $equipment_name, "Name is required.");
		Validate($tip, $equipment_rate_day, "rate is required.");

		$this.ClearError();

		return retVal;
	}

	$this.Clear = function System$$Clear()
	{
		$this.ClearError();

		$this.$fields.val("");
	}

	$this.EnableReadonly = function System$$EnableReadonly()
	{
		$this.$fields.prop('readonly', true);
		$equipment_btns.attr('disabled', true);
		$rand_btn.attr('disabled', true);
	}

	$this.DisableReadonly = function System$$DisableReadonly()
	{
		$this.$fields.prop('readonly', false);
		$equipment_btns.attr('disabled', false);
		$rand_btn.attr('disabled', false);
	}

	$this.ClearError = function System$$ClearError()
	{
		$this.$fields.removeClass("system-error");

		$tip.html("");
	}
}