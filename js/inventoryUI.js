var inventory;
$(function()
{
	inventory = new System.inventory();

	inventory.ListView();
	inventory.Fields();

	//Add inventory modal
	$("#add-inventory-btn").click(function()
	{	
		inventory.DisableReadonly();
		inventory.Clear();
		GetPopulateDropdown('material/OptionList','material_id', {});
		openModal('inventory-modal');
	});

		//Save Info
	$("#inventory-btns").click(function()
	{
		var id = $("#inventory_id").val();
		if(id != "")
			inventory.Update();
		else
			inventory.Save();
	});

});

function Initialize(id)
{
	inventory.DisableReadonly();
	inventory.Initialize(id);
}

function Remove(id)
{
	inventory.Remove(id);
}

function PrintInventoryList()
{
	window.open(window.location.origin + '/print/print_inventory_list.php', '_blank');
}
