if( typeof (System) === "undefined"){
	System = {};
}

System.Task = function System$Task() {
	var $this = this;
	var controller = "../task";

	//elements
	var $table = $("#task_table");
	var $tbody = $("#task-list-result");
	// template result ID's
	var $tbody_material = $("#task_materials_result");
	var $tbody_equipment = $("#task_equipments_result");
	var $tbody_manpower = $("#task_manpower_result");
	var $tbody_material_cost = $("#task-material-total-result");
	var $tbody_equipment_cost = $("#task-equipment-total-result");
	var $tbody_manpower_cost = $("#task-manpower-total-result");
	// template ID's
	var $tmpl = $("#task-list-tmpl");
	var $tmpl_material = $("#task-materials-tmpl");
	var $tmpl_equipment = $("#task-equipments-tmpl");
	var $tmpl_manpower = $("#task-manpower-tmpl");
	var $tmpl_material_cost = $("#task-material-total-tmpl");
	var $tmpl_equipment_cost = $("#task-equipment-total-tmpl");
	var $tmpl_manpower_cost = $("#task-manpower-total-tmpl");
	var $tip = $("#add-task-tip");

	var $rand_btn = $("#rand-task-pass-btn");
	var $task_btns = $("#task-btns");
	var $fields;

	var $task_id = $("#task_id");
	var $task_name = $("#task_name");
	var $task_start_date = $("#task_start_date");
	var $task_end_date = $("#task_end_date");
	var $task_project_id = $("#task_project_id");
	var materials = $('[cat="material"]');
	var equipments = $('[cat="equipment"]');
	var manpower = $('[cat="manpower"]');

	$this.Load = function System$$Load()
	{
		var user_type = $("#user_type").val();
			if(user_type != 1 && user_type != 2)
			{
				$("button.admin-control").hide();
			}
			else
				$("button.admin-control").show();
		$this.Fields();
	}

	$this.Initialize = function System$$Initialize(id)
	{
		$this.Clear();

		$this.TaskModules(id);

		$.post(controller + '/Initialize',
		{
			id : id
		},
		function(data)
		{
			var result = eval("("+data+")");
			var list = result.data;

			if(list.status == 2)
			{
				$("#done-btns").hide();
				$("#start-btns").hide();
			}
			else if(list.status == 3)
			{
				$("#done-btns").show();
				$("#start-btns").hide();
			}
			else if(list.status == 0)
			{
				$("#start-btns").show();
				$("#done-btns").hide();
			}


			$task_id.val(list.task_id);
			$task_name.val(list.task_description);
			$task_start_date.val(list.task_start_date);
			$task_end_date.val(list.task_end_date);
			$task_project_id.val(list.project_id);

			openModal('task-modal');
		});
	}

	$this.TaskModules = function System$$TaskModules(id)
	{
		$.post(controller + '/GetTaskModules',
		{
			id : id,
			status: 1
		},
		function(data)
		{
			alert(data);
			var result = eval("("+data+")");
			var mat = result.materials;
			var equip = result.equipments;
			var man = result.manpower;


				var i;
				//-------------------------------------
				var materials = $('[cat="material"]');
				materials.each(function()
				{
					var mat_id = $(this).attr('id');

					for(i = 0; i<mat.length; i++)
					{
						if(mat_id == mat[i]['material_id'])
						{
							$(this).val(mat[i]['tm_quantity']);
						}
					}
				});
				//--------------------------------------
				var equipments = $('[cat="equipment"]');
				equipments.each(function()
				{
					var equip_id = $(this).attr('id');
					var qty = $(this).attr('qty');

					for(i = 0; i<equip.length; i++)
					{
						if(equip_id == equip[i]['equipment_id'])
						{
							if(qty != '' && qty != null)
							{
								$(this).val(equip[i]['te_days']);
							}
							else
								$(this).val(equip[i]['te_quantity']);
						}
					}
				});
				//--------------------------------------
				var manpower = $('[cat="manpower"]');
				manpower.each(function()
				{
					var man_id = $(this).attr('id');
					var qty = $(this).attr('qty');
					for(i = 0; i<man.length; i++)
					{
						if(man_id == man[i]['manpower_id'])
						{
							if(qty != '' && qty != null)
							{
								$(this).val(man[i]['tmp_days']);
							}
							else
								$(this).val(man[i]['tmp_quantity']);
						}
					}
				});
				$("#add-task-form :input").each(function()
				{
					if($(this).val() == '' || $(this).val() == null)
					{
						$(this).attr('empty','true');
					}
					else
					{
						$(this).attr('empty','false');
					}
				});
		});
	}


	$this.Remove = function System$$Remove(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/Remove',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListView();
			});
		}
	}

	$this.RemoveTaskWorker = function System$$RemoveTaskWorker(id, user_id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		//alert(user_id);
		if(remove)
		{
			$.post('worker/RemoveTaskWorker',
			{
				id : id,
				user_id : user_id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.WorkerListView();
			});
		}
	}

	$this.ListView = function System$$ListView()
	{
		var id = $("#project_id").val();
		$.post(controller + '/ListViewEstimates',
		{
			id : id
		},
		function(data)
		{
			//alert(data);
			var result = eval('('+data+')');
			var list = result.data;
			$("#project_name").text(result.project_name);

			$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl.tmpl(list);
			$tbody.html($tmplResult);

			$table.dataTable();
			var user_type = $("#user_type").val();
			if(user_type != 1)
			{
				$(".admin-control").hide();
			}
			else
				$(".admin-control").show();

		});
	}

	$this.WorkerListView = function System$$WorkerListView()
	{
		var id = $("#task_id").val();
		//alert(id);
		$.post('../worker/ListViewByTask',
		{
			id : id
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $("#task-worker-tmpl").tmpl(list);
			$("#task-worker-result").html($tmplResult);

			$table.dataTable();
		});
	}

	$this.MaterialList = function System$$MaterialList()
	{
		$.post('../Material/ListView',
		{
			
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			//$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl_material.tmpl(list);
			$tbody_material.html($tmplResult);

			//$table.dataTable();
		});
	}

	$this.EquipmentList = function System$$EquipmentList()
	{
		$.post('../Equipment/ListView',
		{
			
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			//$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl_equipment.tmpl(list);
			$tbody_equipment.html($tmplResult);

			//$table.dataTable();
		});
	}

	$this.ManpowerList = function System$$ManpowerList()
	{
		$.post('../Manpower/ListView',
		{
			
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			//$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl_manpower.tmpl(list);
			$tbody_manpower.html($tmplResult);

			//$table.dataTable();
		});
	}

	$this.AddWorker = function System$$AddWorker()
	{
		$.post('../worker/SaveTaskWorker',
		{
			task_id : $("#task_id").val(),
			user_id : $("#worker_id").val()
		},
		function(data)
		{
			$this.WorkerListView();
		});
	}

	$this.ViewTaskCost = function System$$ViewTaskCost()
	{
		var id = $("#task_id").val();
		$.post(controller + '/GetTaskModules',
		{
			id : id,
			status : 1
		},
		function(data)
		{
			var result = eval("("+data+")");
			var mat = result.materials;
			var equip = result.equipments;
			var man = result.manpower;

			var tmplMat = $tmpl_material_cost.tmpl(mat);
			var tmplEquip = $tmpl_equipment_cost.tmpl(equip);
			var tmplMan = $tmpl_manpower_cost.tmpl(man);

			//Apply template result
			$tbody_material_cost.html(tmplMat);
			$tbody_equipment_cost.html(tmplEquip);
			$tbody_manpower_cost.html(tmplMan);
			//get total then append to table as total cost

			var material_total = 0;
			var equipment_total = 0;
			var manpower_total = 0;
			$('[name="material-cost"]').each(function()
			{
				material_total =  material_total + parseFloat($(this).val());
			});
			$('[name="equipment-cost"]').each(function()
			{
				equipment_total = equipment_total + parseFloat($(this).val());
			});
			$('[name="manpower-cost"]').each(function()
			{
				manpower_total = manpower_total + parseFloat($(this).val());
			});

			var task_total = material_total + equipment_total + manpower_total;
			material_total = material_total.toFixed(2);
			equipment_total = equipment_total.toFixed(2);
			manpower_total = manpower_total.toFixed(2);
			task_total = task_total.toLocaleString();

			$tbody_material_cost.append('<tr style="background-color: #999;border-top:1px solid grey;"><td><b>Total</b></td><td></td><td><b>'
										+ material_total.toLocaleString() +'</b></td></tr>');
			$tbody_equipment_cost.append('<tr style="background-color: #999;border-top:1px solid grey;"><td><b>Total</b></td><td></td><td></td><td><b>'
										+ equipment_total.toLocaleString() +'</b></td></tr>');
			$tbody_manpower_cost.append('<tr style="background-color: #999;border-top:1px solid grey;"><td><b>Total</b></td><td></td><td></td><td><b>'
										+ manpower_total.toLocaleString() +'</b></td></tr>');
			$("#task-summary").empty().append('<tr><td>Materials cost</td><td>'+
										material_total.toLocaleString() +
										'</td></tr><tr><td>Equipments cost</td><td>'
										+ equipment_total.toLocaleString() +
										'</td></tr><tr><td>Manpower cost</td><td>'
										+ manpower_total.toLocaleString() +
										'</td></tr><tr style="background-color: #999;border-top:1px solid grey;"><td><b>Task total cost</b></td><td><b>'
										+ task_total +'</b></td></tr>');
		});
	}

	$this.Save = function System$$Save()
	{
		// task details
		var task_id = $task_id.val();
		var task_name = $task_name.val();
		var task_start_date = $task_start_date.val();
		var task_end_date = $task_end_date.val();
		var project_id = $("#project_id").val();

		var strMat= "";
		var strEquip = "";
		var strMan = "";
		var materials = $('[cat="material"]');
		var equipments = $('[cat="equipment"]');
		var manpower = $('[cat="manpower"]');
		// get all input value of materials 
		// that is not equal to 0

		if(!$this.Validation())
		{
			return;
		}

		materials.each(function()
		{
			var mat_id = $(this).attr('id');
			var Mval =  $(this).val();

			strMat += "tm_quantity=" + Mval +
						",material_id=" + mat_id +
						":";
		});
		// get all input value of equipments 
		// that is not equal to 0
		equipments.each(function()
		{
			var equip_id = $(this).attr('id');
			var Eval =  $(this).val();
			var qty = $(this).attr('qty');

			if(qty != '' && qty != null)
			{
				var n = $(this).val();
				strEquip += "te_days=" + n + ":";
			}
			else
			{
				strEquip += "te_quantity=" + Eval +
							",equipment_id=" + equip_id + "," ;
			}

		});
		// get all input value of manpower 
		// that is not equal to 0
		manpower.each(function()
		{
			var man_id = $(this).attr('id');
			var Maval =  $(this).val();
			var qty = $(this).attr('qty');

			if(qty != '' && qty != null)
			{
				var n = $(this).val();
				strMan += "tmp_days=" + n + ":";
			}
			else
			{
				strMan += "tmp_quantity=" + Maval +
							",manpower_id=" + man_id + ",";
			}	

		});

		strMat = strMat.slice(0,-1)
		strEquip = strEquip.slice(0,-1);
		strMan = strMan.slice(0,-1);
		//return;
		
		// save data using post request
		$.post(controller + '/Save',
		{
			task_id : task_id,
			project_id : project_id,
			task_name : task_name,
			task_start_date : task_start_date,
			task_end_date : task_end_date,
			strMat : strMat,
			strEquip : strEquip,
			strMan : strMan
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);
			if(result.error == true)
			{return;}

			closeModal('task-modal');
			$this.ListView();
		});
	}

	$this.Update = function System$$Update(id)
	{
		// task details
		var task_id = $task_id.val();
		var task_name = $task_name.val();
		var task_start_date = $task_start_date.val();
		var task_end_date = $task_end_date.val();
		var project_id = $("#project_id").val();

		var strMat= "";
		var strEquip = "";
		var strMan = "";
		var materials = $('[cat="material"]');
		var equipments = $('[cat="equipment"]');
		var manpower = $('[cat="manpower"]');

		if(!$this.Validation())
		{
			return;
		}
		
		// get all input value of materials 
		// that is not equal to 0
		materials.each(function()
		{
			var mat_id = $(this).attr('id');
			var Mval =  $(this).val();
			strMat += "tm_quantity=" + Mval +
							",material_id=" + mat_id +
							",task_id=" + task_id + ":";
		});
		// get all input value of equipments 
		// that is not equal to 0
		equipments.each(function()
		{
			var equip_id = $(this).attr('id');
			var Eval =  $(this).val();
			var qty = $(this).attr('qty');

			if(qty != '' && qty != null)
			{
				var n = $(this).val();
				strEquip += "te_days=" + n + ":"
			}
			else
			{
				strEquip += "te_quantity=" + Eval +
							",equipment_id=" + equip_id +
							",task_id=" + task_id + ",";
			}

		});
		// get all input value of manpower 
		// that is not equal to 0
		manpower.each(function()
		{
			var man_id = $(this).attr('id');
			var Maval =  $(this).val();
			var qty = $(this).attr('qty');

			if(qty != '' && qty != null)
			{
				var n = $(this).val();
				strMan += "tmp_days=" + n + ":"
			}
			else
			{
				strMan += "tmp_quantity=" + Maval +
							",manpower_id=" + man_id +
							",task_id=" + task_id + ",";
			}

		});

		strMat = strMat.slice(0,-1)
		strEquip = strEquip.slice(0,-1);
		strMan = strMan.slice(0,-1);
		//return;
		//alert(strMat);
		//alert(strEquip);
		//alert(strMan);
		
		// save data using post request
		$.post(controller + '/Edit',
		{
			task_id : task_id,
			project_id : project_id,
			task_name : task_name,
			task_start_date : task_start_date,
			task_end_date : task_end_date,
			strMat : strMat,
			strEquip : strEquip,
			strMan : strMan
		},
		function(data)
		{
			alert(data);
			var result = eval('('+data+')');
			alert(result.message);

			closeModal('task-modal');
			$this.ListView();
		});

	}

	$this.Done = function System$$Done()
	{
		$.post(controller + '/Done',
		{
			id : $task_id.val()
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);

			window.location.href = "";
		});
	}

	$this.Start = function System$$Start()
	{
		$.post(controller + '/Start',
		{
			id : $task_id.val()
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);
			window.location.href = "";
		});
	}

	$this.Fields = function System$$Fields()
	{
		var $temp = $();

		$temp = $temp.add($task_id);
		$temp = $temp.add($task_name);
		$temp = $temp.add($task_start_date);
		$temp = $temp.add($task_end_date);
		var materials = $('[cat="material"]');
		var equipments = $('[cat="equipment"]');
		var manpower = $('[cat="manpower"]');

		/*materials.each(function()
		{
			$temp = $temp.add($("#"+ $(this).attr(id)));
		});
		equipments.each(function()
		{
			$temp = $temp.add($("#"+ $(this).attr(id)));
		});
		manpower.each(function()
		{
			$temp = $temp.add($("#"+ $(this).attr(id)));
		});
		*/

		$this.$fields = $temp;

	}

	$this.Validation = function System$$Validate()
	{
		var retVal = true;

		Validate($tip, $task_name, "Name is required.");
		Validate($tip, $task_start_date, "Start date is required.");
		Validate($tip, $task_end_date, "End date is required.");

		$this.ClearError();

		return retVal;
	}

	$this.Clear = function System$$Clear()
	{
		$this.ClearError();

		$this.$fields.val("");

		$("#add-task-form :input").val("");
	}

	$this.EnableReadonly = function System$$EnableReadonly()
	{
		$this.$fields.attr('disabled', true);
		$task_btns.attr('disabled', true);
		$rand_btn.attr('disabled', true);
		$("#add-task-form :input").attr('disabled', true);
	}

	$this.DisableReadonly = function System$$DisableReadonly()
	{
		$this.$fields.attr('disabled', false);
		$task_btns.attr('disabled', false);
		$rand_btn.attr('disabled', false);
		$("#add-task-form :input").attr('disabled', false);
	}

	$this.ClearError = function System$$ClearError()
	{
		$this.$fields.removeClass("system-error");

		$tip.html("");
	}
}