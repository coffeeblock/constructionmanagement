var material;
$(function()
{
	material = new System.Material();

	material.ListView();
	material.Fields();

	//Add material modal
	$("#add-material-btn").click(function()
	{	
		material.DisableReadonly();
		material.Clear();
		$("#material_name").removeAttr('disabled');
		openModal('material-modal');
	});

		//Save Info
	$("#material-btns").click(function()
	{
		var id = $("#material_id").val();
		if(id != "")
			material.Update();
		else
			material.Save();
	});

	setInterval(function(){$('[status="1"]').hide()},200);

});

function Initialize(id, status)
{
	material.DisableReadonly();
	material.Initialize(id, status);
}

function Remove(id)
{
	material.Remove(id);
}

function PrintMaterialList()
{
	window.open(window.location.origin + '/print/print_material_list.php', '_blank');
}

