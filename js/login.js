if( typeof (System) === "undefined"){
	System = {};
}

System.Login = function System$Login() {
	var $this = this;
	var $username = $("#username");
	var $password = $("#password");
	var $tip = $("#login-tip");

	$this.VerifyLogin = function System$$VerifyLogin()
	{
		var username = $username.val();
		var password = $password.val();

		if(!$this.Validation())
		{
			return;
		}

		$.post('user/loginProcess',
		{
			username : username,
			password : password
		},
		function(data)
		{
			//alert(data);
			window.location.href = "";
		});

	}

	$this.Validation = function System$$Validate()
	{
		var retVal = true;

		$this.ClearError();

		Validate($tip, $username, "Username is required.");
		Validate($tip, $password, "Password is required.");

		return retVal;
	}


	$this.ClearError = function System$$ClearError()
	{
		$username.removeClass("system-error");
		$password.removeClass("system-error");
		$tip.html("");
	}
}