if( typeof (System) === "undefined"){
	System = {};
}

System.Worker = function System$Worker() {
	var $this = this;
	var controller = "worker";

	var $table = $("#worker_table");
	var $tmpl = $("#worker-list-tmpl");
	var $tbody = $("#worker-list-result");
	var $worker_id = $("#worker_id");
	var $worker_firstname = $("#worker_firstname");
	var $worker_lastname = $("#worker_lastname");
	var $worker_rate = $("#worker_rate");
	var $worker_username = $("#worker_username");
	var $worker_password = $("#worker_password");
	var $worker_age = $("#worker_age");
	var $worker_gender = $("#worker_gender");
	var $worker_contact_number = $("#worker_contact_number");
	var $worker_address = $("#worker_address");
	var $worker_btns = $("#worker-btns");
	var $tip = $("#add-worker-tip");


	$this.Load = function System$$Load()
	{
		$this.Fields();
	}

	$this.Initialize = function System$$Initialize(id)
	{
		$this.Clear();

		$.post(controller + '/Initialize',
		{
			id : id
		},
		function(data)
		{
			var result = eval("("+data+")");
			var list = result.data;

			$worker_id.val(list.user_id);
			$worker_firstname.val(list.user_firstname);
			$worker_lastname.val(list.user_lastname);
			$worker_age.val(list.user_age);
			$worker_gender.val(list.user_gender);
			$worker_rate.val(list.user_rate);
			$worker_contact_number.val(list.user_contact_number);
			$worker_address.val(list.user_address);

			openModal('worker-modal');
		});
	}


	$this.Remove = function System$$Remove(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/Remove',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListView();
			});
		}
	}

	$this.ListView = function System$$ListView()
	{
		$.post('worker/ListView',
		{
			//null
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl.tmpl(list);
			$tbody.html($tmplResult);

			$table.dataTable();
		});
	}

	$this.Save = function System$$Save()
	{

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Save',
		{
			user_firstname : $worker_firstname.val(),
			user_lastname : $worker_lastname.val(),
			user_username : $worker_username.val(),
			user_rate : $worker_rate.val(),
			user_password_text : $worker_password.val(),
			user_password_hash : '',
			user_age : $worker_age.val(),
			user_gender : $worker_gender.val(),
			user_contact_number : $worker_contact_number.val(),
			user_address : $worker_address.val(),
			user_type : 5,
			status : 0
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);

			closeModal('engineer-modal');
			$this.ListView();
		});
	}

	$this.Update = function System$$Update(id)
	{
		var id = $worker_id.val();
		var name = $worker_name.val();
		var rate = $worker_rate_day.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Edit',
		{
			user_id : $worker_id.val(),
			user_firstname : $worker_firstname.val(),
			user_lastname : $worker_lastname.val(),
			user_username : $worker_username.val(),
			user_rate : $worker_rate.val(),
			user_password_text : $worker_password.val(),
			user_password_hash : '',
			user_age : $worker_age.val(),
			user_gender : $worker_gender.val(),
			user_contact_number : $worker_contact_number.val(),
			user_address : $worker_address.val(),
			user_type : 4,
			status : 0
		},
		function(data)
		{
			var result = eval("("+data+")");
			alert(result.message);

			closeModal('worker-modal');
			$this.ListView();
		});

	}

	$this.Fields = function System$$Fields()
	{
		var $temp = $();

		$temp = $temp.add($worker_id);
		$temp = $temp.add($worker_firstname);
		$temp = $temp.add($worker_lastname);
		$temp = $temp.add($worker_rate);
		$temp = $temp.add($worker_age);
		$temp = $temp.add($worker_gender);
		$temp = $temp.add($worker_contact_number);
		$temp = $temp.add($worker_address);
		$this.$fields = $temp;

	}

	$this.Validation = function System$$Validate()
	{
		var retVal = true;

		Validate($tip, $worker_firstname, "Firstname is required.");
		Validate($tip, $worker_lastname, "Lastname is required.");
		Validate($tip, $worker_rate, "Rate is required.");
		Validate($tip, $worker_age, "Age is required.");
		Validate($tip, $worker_gender, "Gender is required.");
		Validate($tip, $worker_contact_number, "Contact number is required.");
		Validate($tip, $worker_address, "Address is required.");
		$this.ClearError();

		return retVal;
	}


	$this.Clear = function System$$Clear()
	{
		$this.ClearError();

		$this.$fields.val("");
	}

	$this.EnableReadonly = function System$$EnableReadonly()
	{
		$this.$fields.prop('readonly', true);
		$worker_btns.attr('disabled', true);
		//$rand_btn.attr('disabled', true);
	}

	$this.DisableReadonly = function System$$DisableReadonly()
	{
		$this.$fields.prop('readonly', false);
		$worker_btns.attr('disabled', false);
		//$rand_btn.attr('disabled', false);
	}

	$this.ClearError = function System$$ClearError()
	{
		$this.$fields.removeClass("system-error");

		$tip.html("");
	}
}