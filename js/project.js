if( typeof (System) === "undefined"){
	System = {};
}

System.Project = function System$Project() {
	var $this = this;
	var controller = "project";

	//elements
	var $table = $("#project_table");
	var $tbody = $("#project-list-result");
	var $tbody_total_cost = $("#project-total-cost");
	var $tbody_estimated = $("#estimated-cost-result");
	var $tmpl = $("#project-list-tmpl");
	var $tmpl_total_cost = $("#project-total-cost-tmpl");
	var $tmpl_estimated = $("#estimated-cost-tmpl");
	var $tmpl_estimated_materials = $("#estimated-materials-tmpl");
	var $tmpl_estimated_equipments = $("#estimated-equipments-tmpl");
	var $tmpl_estimated_manpower = $("#estimated-manpower-tmpl");
	var $tip = $("#add-project-tip");
	var $rand_btn = $("#rand-project-pass-btn");
	var $project_btns = $("#project-btns");
	var $admin_control = $(".admin-control");
	var $fields;

	var $project_id = $("#project_id");
	var $project_name = $("#project_name");
	var $project_location = $("#project_location");
	var $project_start_date = $("#p_start_date");
	var $project_end_date = $("#p_end_date");
	var $project_dim_length = $("#project_dim_length");
	var	$project_dim_height = $("#project_dim_height");
	var $project_dim_width = $("#project_dim_width");
	var $project_dim_lanes = $("#project_dim_lanes");
	var $project_engineer =  $("#project_engineer");
	var $project_client = $("#project_client");
	var $project_modal_dialog = $("#project-modal .modal-dialog");
	var $project_modal_upper_table = $("#project-modal .modal-dialog .upper-table");
	//estimates volume/area
	var $excavation = $("#excavation");
	var $embankment = $("#embankment");
	var $sub_grade = $("#sub-grade");
	var $sub_base = $("#sub-base");
	var $base_course = $("#base-course");

	$this.Load = function System$$Load()
	{
		var user_type = $("#user_type").val();
			if(user_type != 1)
				$("button.admin-control").hide();
			else
				$("button.admin-control").show();

		$this.Fields();
	}

	$this.Initialize = function System$$Initialize(id, callback)
	{
		$this.Clear();	

		$.post(controller + '/Initialize',
		{
			id : id
		},
		function(data)
		{
			var result = eval("("+data+")");
			var list = result.data;
			var item = result.items;

			$project_id.val(list.project_id);
			$project_name.val(list.project_name);
			$project_location.val(list.project_location);
			$project_start_date.val(list.project_start_date);
			$project_end_date.val(list.project_end_date);
			$project_dim_length.val(list.project_dim_length);
			$project_dim_height.val(list.project_dim_height);
			$project_dim_width.val(list.project_dim_width);
			$project_dim_lanes.val(list.project_dim_lanes);
			$project_engineer.val(list.project_engineer);
			$project_client.val(list.project_client);

			for(var i=0; i<item.length; i++)
			{
				//alert(item[i]['task_description']);
				name = item[i]['task_description'];

				switch(name)
				{
					case 'Excavation':
						$excavation.val(item[i]['quantity']);
						break;
					case 'Embankment':
						$embankment.val(item[i]['quantity']);
						break;
					case 'Sub-grade':
						$sub_grade.val(item[i]['quantity']);
						break;
					case 'Sub-base':
						$sub_base.val(item[i]['quantity']);
						break;
					case 'Base course':
						$base_course.val(item[i]['quantity']);
						break;
				}
			}

			if(callback != null && typeof(callback) == "function")
			{
				callback(true);
			}
		});
	}

	$this.Estimated = function System$$Estimated()
	{
		var id = $("#project_id").val();
		$.post('task/GeneratedByProject',
		{
			id : id
		},
		function(data)
		{
			//alert(data);
			var result = eval("("+data+")");
			var list = result.data;
			//generated task template
			var $tmplResult = $tmpl_estimated.tmpl(list);
			$tbody_estimated.html($tmplResult);
			
			//generated task estimates
			for(var i = 0; i < list.length; i++)
			{
				var task_id = list[i]['task_id'];
				//alert(task_id);
				$.post('task/GetTaskModules',
				{
					id : list[i]['task_id'],
					status : 1
				},
				function(data)
				{
					//alert(data);
					var total_materials = 0;
					var total_equipments = 0;
					var total_manpower = 0;
					var result = eval("("+data+")");
					var id = result.id;
					var mat = result.materials;
					var equip = result.equipments;
					var man = result.manpower;
					//total
					for(var i = 0; i<mat.length; i++)
					{
						total_materials = total_materials + parseFloat(mat[i]['tm_total']);
						//alert(total_materials);
						//alert(parseFloat(mat[i]['tm_total']));
					}
					for(var i = 0; i<equip.length; i++)
					{
						total_equipments = total_equipments + parseFloat(equip[i]['te_total']);
					}
					for(var i = 0; i<man.length; i++)
					{
						total_manpower = total_manpower +parseFloat(man[i]['tmp_total']);
					}
					//generated task modules tamplate
					var $tmplResult1 = $tmpl_estimated_materials.tmpl(mat);

					$("#estimated-materials-"+id+"-result").html($tmplResult1);
					$("#estimated-materials-"+id+"-result").append('<tr style="background-color: #999;border-top:1px solid grey;"><td><b>Total</b><td><td></td></td><td></td><td><b>'
										+ total_materials.toLocaleString() +'</b></td></tr>');

					var $tmplResult2 = $tmpl_estimated_equipments.tmpl(equip);
					$("#estimated-equipments-"+id+"-result").html($tmplResult2);
					$("#estimated-equipments-"+id+"-result").append('<tr style="background-color: #999;border-top:1px solid grey;"><td><b>Total</b></td><td></td><td></td><td></td><td><b>'
										+ total_equipments.toLocaleString() +'</b></td></tr>');

					var $tmplResult3 = $tmpl_estimated_manpower.tmpl(man);
					$("#estimated-manpower-"+id+"-result").html($tmplResult3);
					$("#estimated-manpower-"+id+"-result").append('<tr style="background-color: #999;border-top:1px solid grey;"><td><b>Total</b></td><td></td><td></td><td></td><td><b>'
										+ total_manpower.toLocaleString() +'</b></td></tr>');

					var total = total_materials + total_equipments + total_manpower;
					$("#total-"+id).text(total.toFixed(2));
				});
			}
		});

		setTimeout(
			function()
			{
				//get total estimated cost
				var estimated_total = 0;
				$('[total="task-sub-total"]').each(function()
				{
					var n = parseFloat($(this).text());
					estimated_total += isNaN(n) ? 0 : n;
					//alert(estimated_total);
				});
				
				$("#estimated-total-cost").empty().append('<tr style="background-color: #999;border-top:1px solid grey;"><td><b>Total cost</b><span style="float:right;margin-right:5%;"><b>'
											+estimated_total.toFixed(2).toLocaleString()+'</b></span></td></tr>');
			},1000
		);

	}


	$this.Remove = function System$$Remove(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/Remove',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListView();
			});
		}
	}

	$this.ListView = function System$$ListView()
	{
		var user_type = $("#user_type").val();
		var sub;

		if(user_type != 1)
			sub = '/ListById';
		else
			sub = '/ListView';

		$.post(controller +sub,
		{
			//null
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$table.dataTable().fnDestroy();
			//populate template
			var $tmplResult = $tmpl.tmpl(list);
			$tbody.html($tmplResult);

			$table.dataTable({
				"order": [[ 2, "desc" ]]
			});
			var user_type = $("#user_type").val();
			if(user_type != 1)
			{
				$(".admin-control").hide();
			}
			else
			{
				$(".admin-control").show();
			}
		});
	}

	$this.ListEquipmentDue = function System$$ListEquipmentDue()
	{
		var project_id = $("#project_id").val();

		$.post('equipment/ListEquipmentDue',
		{
			id : project_id
		},
		function(data)
		{
			alert(data);
			var result = eval('('+data+')');
			var total = 0;
			var list = result.data;

			for(var i = 0 ; i<list.length; i++)
			{
				total = total + parseFloat(list[i]['amount_due']);
			}

			$("#project_equipment_due_cost").val(total);
			//populate template
			var $tmplResult = $("#equipment-due-tmpl").tmpl(list);
			$("#equipment-due-result").html($tmplResult);
			$("#equipment-due-summary").empty().append('<tr style="border:1px solid grey;"><td width="30%"><b>Total</b></td><td width="20%"></td><td colspan="2" width="30%"><b style="text-align:left;">'
												+ total.toFixed(2).toLocaleString() +'</b></td></tr>');
		});
	}

	$this.Save = function System$$Save()
	{
		var project_name = $project_name.val();
		var project_location = $project_location.val();
		
		var project_dim_height = $project_dim_height.val();
		var project_dim_length = $project_dim_length.val();
		var project_dim_width = $project_dim_width.val();
		var project_dim_lanes = $project_dim_lanes.val();
		var project_engineer = $project_engineer.val();
		var project_client = $project_client.val();
		var excavation = $excavation.val();
		var embankment = $embankment.val();
		var sub_grade = $sub_grade.val();
		var sub_base = $sub_base.val();
		var base_course = $base_course.val();

		if(!$this.Validation())
		{
			return;
		}

		$.post(controller + '/Save',
		{
			project_name : project_name,
			project_location : project_location,
			project_start_date : $project_start_date.val(),
			//project_end_date : $project_end_date.val(),
			project_dim_length : project_dim_length,
			project_dim_height : project_dim_height,
			project_dim_width : project_dim_width,
			project_dim_lanes : project_dim_lanes,
			engineer_id : project_engineer,
			client_id : project_client,
			excavation : excavation,
			embankment : embankment,
			sub_grade : sub_grade,
			sub_base : sub_base,
			base_course : base_course,
			status : 0
		},
		function(data)
		{
			alert(data);
			var result = eval('('+data+')');
			alert(result.message);

			closeModal('project-modal');
			$this.ListView();
		});
	}

	$this.Update = function System$$Update(id)
	{
		var id = $project_id.val();
		var name = $project_name.val();
		var location = $project_location.val();
		var start_date = $project_start_date.val();
		//var end_date = $project_end_date.val();
		var dim_length = $project_dim_length.val();
		var dim_height = $project_dim_height.val();
		var dim_width = $project_dim_width.val();
		var dim_lanes = $project_dim_lanes.val()
		var project_engineer = $project_engineer.val();
		var project_client = $project_client.val();

		var excavation = $excavation.val();
		var embankment = $embankment.val();
		var sub_grade = $sub_grade.val();
		var sub_base = $sub_base.val();
		var base_course = $base_course.val();


		if(!$this.Validation())
		{
			return;
		}

		//$this.GenerateFixedMaterials(id, dim_length, dim_width, dim_height);

		
		$.post(controller + '/Edit',
		{
			project_id : id,
			project_name : name,
			project_location : location,
			project_start_date : start_date,
			//project_end_date : end_date,
			project_dim_length : dim_length,
			project_dim_height : dim_height,
			project_dim_width : dim_width,
			project_dim_lanes : dim_lanes,
			project_engineer_id : project_engineer,
			project_client_id : project_client,
			excavation : excavation,
			embankment : embankment,
			sub_grade : sub_grade,
			sub_base : sub_base,
			base_course : base_course,
			status : 0
		},
		function(data)
		{
			var result = eval("("+data+")");
			alert(result.message);

			closeModal('project-modal');
			$this.ListView();
		});

	}

	$this.ViewTotal = function System$$ViewTotal(id)
	{
		$.post(controller + '/TotalCost',
		{
			id : id,
			status : 0
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;
			var days = 0;
			var done = 0;
			var percentage = 0;

			var $tmplResult = $tmpl_total_cost.tmpl(list);
			$tbody_total_cost.html($tmplResult);

			//total project cost
			var i = 0;
			var total = 0;
			for(i = 0; i<list.length; i++)
			{
				total = total + parseFloat(list[i]['total_cost']);

				if(list[i].status == 2)
				{
					done = done + parseInt(list[i]['total_days']);
					//alert(done);
				}
					days = days + parseFloat(list[i]['total_days']);
			}

			percentage = (done/days)*100;
			$("#project_total_task_cost").val(total);
			$("#project-percentage").text(percentage.toFixed() + '%');
			$("#project-summary").empty().append('<tr style="border:1px solid grey;"><td width="30%"><b>Total</b></td><td width="20%"><b style="text-align:left;">'+days+'</b></td><b><td width="30%"><b style="text-align:left;">'
												+ total.toFixed(2).toLocaleString() +'</b></td><td></td></tr>');


		});
	}

	$this.Fields = function System$$Fields()
	{
		var $temp = $();

		$temp = $temp.add($project_id);
		$temp = $temp.add($project_name);
		$temp = $temp.add($project_location);
		$temp = $temp.add($project_start_date);
		//$temp = $temp.add($project_end_date);
		$temp = $temp.add($project_dim_length);
		$temp = $temp.add($project_dim_height);
		$temp = $temp.add($project_dim_width);
		$temp = $temp.add($project_dim_lanes);
		$temp = $temp.add($project_engineer);
		$temp = $temp.add($project_client);
		$temp = $temp.add($excavation);
		$temp = $temp.add($embankment);
		$temp = $temp.add($sub_grade);
		$temp = $temp.add($sub_base);
		$temp = $temp.add($base_course);


		$this.$fields = $temp;

	}

	$this.Validation = function System$$Validate()
	{
		var retVal = true;

		Validate($tip, $project_name, "Name is required.");
		Validate($tip, $project_location, "Location is required.");
		Validate($tip, $project_start_date, "Start date is required.");
		//Validate($tip, $project_end_date, "End date is required.");
		Validate($tip, $project_dim_length, "Length is required.");
		Validate($tip, $project_dim_height, "Heigh is required.");
		Validate($tip, $project_dim_width, "Width is required.");
		Validate($tip, $project_dim_lanes, "Lanes is required.");
		Validate($tip, $project_engineer, "Project engineer is required.");
		Validate($tip, $project_client, "Project client is required.");

		$this.ClearError();

		return retVal;
	}

	$this.Clear = function System$$Clear()
	{
		$this.ClearError();

		$this.$fields.val("");
	}

	$this.EnableReadonly = function System$$EnableReadonly()
	{
		$this.$fields.attr('disabled', true);
		$project_engineer.attr('disabled', true);
		$project_client.attr('disabled', true);
		$project_btns.attr('disabled', true);
		$rand_btn.attr('disabled', true);
	}

	$this.DisableReadonly = function System$$DisableReadonly()
	{
		$this.$fields.removeAttr('disabled');
		$project_engineer.attr('disabled', false);
		$project_client.attr('disabled', false);
		$project_btns.attr('disabled', false);
		$rand_btn.attr('disabled', false);
	}

	$this.ClearError = function System$$ClearError()
	{
		$this.$fields.removeClass("system-error");

		$tip.html("");
	}

	$this.MaximizeModal = function System$$MaximizeModal()
	{
		$project_modal_dialog.addClass('modal-expand');
		$project_modal_upper_table.addClass('minimize-tbl');
		$(".left").css('width','50%');
		$("#project-total").css('display','inline-block');
		$("#project-modal .modal-inner").css('width','1100px');
	}

	$this.MinimizeModal = function System$$MinimizeModal()
	{
		$project_modal_dialog.removeClass('modal-expand');
		$project_modal_upper_table.removeClass('minimize-tbl');
		$(".left").css('width','100%');
		$("#project-total").css('display','inline-block');
		$("#project-modal .modal-inner").css('width','650px');
	}

	Number.prototype.zeroPad = function()
	{
		return ('0'+this).slice(-2);
	}
}