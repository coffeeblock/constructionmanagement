if( typeof (System) === "undefined"){
	System = {};
}

System.Manpower = function System$Manpower() {
	var $this = this;
	var controller = "manpower";

	//elements
	var $table = $("#manpower_table");
	var $tbody = $("#manpower-list-result");
	var $tmpl = $("#manpower-list-tmpl");
	var $tip = $("#add-manpower-tip");
	var $rand_btn = $("#rand-manpower-pass-btn");
	var $manpower_btns = $("#manpower-btns");
	var $fields;

	var $manpower_id = $("#manpower_id");
	var $manpower_name = $("#manpower_name");
	var $manpower_rate_day = $("#manpower_rate_day");

	//worker
	var $worker_table = $("#worker_table");
	var $worker_tmpl = $("#worker-list-tmpl");
	var $worker_tbody = $("#worker-list-result");
	var $worker_firstname = $("#worker_firstname");
	var $worker_lastname = $("#worker_lastname");
	var $worker_rate = $("#worker_rate");
	var $worker_username = $("#worker_username");
	var $worker_password = $("#worker_password");
	var $worker_age = $("#worker_age");
	var $worker_gender = $("#worker_gender");
	var $worker_contact_number = $("#worker_contact_number");
	var $worker_address = $("#worker_address");
	var $tip_worker = $("#add-worker-tip");


	$this.Load = function System$$Load()
	{
		$this.Fields();
	}

	$this.Initialize = function System$$Initialize(id,status)
	{
		$this.Clear();

		if(status == 1)
			$("#manpower_name").attr('disabled','disabled');
		else
			$("#manpower_name").removeAttr('disabled');

		$.post(controller + '/Initialize',
		{
			id : id
		},
		function(data)
		{
			var result = eval("("+data+")");
			var list = result.data;

			$manpower_id.val(list.manpower_id);
			$manpower_name.val(list.manpower_name);
			$manpower_rate_day.val(list.manpower_rate_day);

			openModal('manpower-modal');
		});
	}


	$this.Remove = function System$$Remove(id)
	{
		var remove = confirm("Are you sure you want to delete this?");
		if(remove)
		{
			$.post(controller + '/Remove',
			{
				id : id
			},
			function(data)
			{
				var result = eval("("+data+")");
				alert(result.message);
				$this.ListView();
			});
		}
	}

	$this.ListView = function System$$ListView()
	{
		$.post(controller + '/ListView',
		{
			//null
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $tmpl.tmpl(list);
			$tbody.html($tmplResult);

			$table.dataTable();
			$('[status="1"]').hide();
		});
	}

	$this.ListViewWorker = function System$$ListView()
	{
		$.post('manpower/ListWorkers',
		{
			//null
		},
		function(data)
		{
			var result = eval('('+data+')');
			var list = result.data;

			$worker_table.dataTable().fnDestroy();
			// populate template
			var $tmplResult = $worker_tmpl.tmpl(list);
			$worker_tbody.html($tmplResult);

			$worker_table.dataTable();
		});
	}

	$this.Save = function System$$Save()
	{
		var manpower_name = $manpower_name.val();
		var manpower_rate_day = $manpower_rate_day.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Save',
		{
			manpower_name : manpower_name,
			manpower_rate_day : manpower_rate_day
		},
		function(data)
		{
			var result = eval('('+data+')');
			alert(result.message);

			closeModal('manpower-modal');
			$this.ListView();
		});
	}

	$this.Update = function System$$Update(id)
	{
		var id = $manpower_id.val();
		var name = $manpower_name.val();
		var rate = $manpower_rate_day.val();

		if(!$this.Validation())
		{
			return;
		}
		$.post(controller + '/Edit',
		{
			manpower_id : id,
			manpower_name : name,
			manpower_rate_day : rate
		},
		function(data)
		{
			var result = eval("("+data+")");
			alert(result.message);

			closeModal('manpower-modal');
			$this.ListView();
		});

	}

	$this.Fields = function System$$Fields()
	{
		var $temp = $();

		$temp = $temp.add($manpower_id);
		$temp = $temp.add($manpower_name);
		$temp = $temp.add($manpower_rate_day);

		$this.$fields = $temp;

	}

	$this.Validation = function System$$Validate()
	{
		var retVal = true;

		Validate($tip, $manpower_name, "Name is required.");
		Validate($tip, $manpower_rate_day, "rate is required.");

		$this.ClearError();

		return retVal;
	}

	$this.Clear = function System$$Clear()
	{
		$this.ClearError();

		$this.$fields.val("");
	}

	$this.EnableReadonly = function System$$EnableReadonly()
	{
		$this.$fields.prop('readonly', true);
		$manpower_btns.attr('disabled', true);
		$rand_btn.attr('disabled', true);
	}

	$this.DisableReadonly = function System$$DisableReadonly()
	{
		$this.$fields.prop('readonly', false);
		$manpower_btns.attr('disabled', false);
		$rand_btn.attr('disabled', false);
	}

	$this.ClearError = function System$$ClearError()
	{
		$this.$fields.removeClass("system-error");

		$tip.html("");
	}
}