var equipment;
$(function()
{
	equipment = new System.Equipment();

	equipment.ListView();
	equipment.ListViewLogs();
	equipment.Fields();
	
	GetPopulateDropdown("project/OptionList", "project_id", {}, function(data){});
	//GetPopulateDropdown("worker/ListAllByProject", "worker_id", {id : $("#project_id").val()}, function(data){});

	$("#project_id").change(function()
	{
		GetPopulateDropdown("worker/ListAllByProject", "worker_id", {id : $("#project_id").val()}, function(data){});
		GetPopulateDropdown("equipment/ListByProject", "equipment_log_id", {id : $("#project_id").val()}, function(data){});
	});

	//Add equipment modal
	$("#add-equipment-btn").click(function()
	{	
		equipment.DisableReadonly();
		equipment.Clear();

		$("#equipment_name").removeAttr('disabled');
		openModal('equipment-modal');
	});

		//Save Info
	$("#equipment-btns").click(function()
	{
		var id = $("#equipment_id").val();
		if(id != "")
			equipment.Update();
		else
			equipment.Save();
	});

	$("#add-equipment-log-btn").click(function()
	{
		openModal('equipment-log-modal');
	});

	$("#equipment-log-btns").click(function()
	{
		equipment.SaveLog();
	});

	setInterval(function(){$('[status="1"]').hide()},200);

});

function Initialize(id, status)
{
	equipment.DisableReadonly();
	equipment.Initialize(id, status);
}

function InitializeLog(id,project_id)
{
	alert(project_id);
	GetPopulateDropdown("equipment/ListByProject", "equipment_log_id", {id : project_id}, function(data){
		GetPopulateDropdown("worker/ListAllByProject", "worker_id", {id : project_id}, function(data){
			equipment.InitializeLog(id, function(data)
				{
					openModal('equipment-log-modal');
				});

		});
	});
}

function Remove(id)
{
	equipment.Remove(id);
}

function RemoveLog(id)
{
	equipment.RemoveLog(id);
}

function PrintEquipmentList()
{
	window.open(window.location.origin + '/print/print_equipment_list.php', '_blank');
}