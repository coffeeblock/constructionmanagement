var task;
$(function()
{
	task = new System.Task();

	var id = $("#project_id").val();
	var type = "";


	task.Load();
	task.ListView();
	task.MaterialList();
	task.EquipmentList();
	task.ManpowerList();
	task.Fields();

	GetPopulateDropdown("../project/ProjectDropdown", "task_project_id", {}, function(data){});

	$("#done-btns").hide();
	$("#start-btns").hide();
	$("#task-returned-materials").hide();
	$("#task-workers").hide();
	
	
	$("#task-returned-subheader").hide();
	$("#task-worker-subheader").hide();

	
	//alert(window.project_id);

	//Add task modal
	$("#view-estimate-btn").click(function()
	{	
		$("#task-sub-header").hide();
		$("#task-details").hide();
		$("#task-header").text('Add Task');
		$("#task-sub-total").hide();
		task.DisableReadonly();
		task.Clear();
		openModal('estimates-modal');
	});

		//Save Info
	$("#task-btns").click(function()
	{
		var id = $("#task_id").val();
		alert(id);
		if(id != "")
			task.Update(id);
		else
			task.Save();
	});

	//generate password
	$("#rand-task-pass-btn").click(function()
	{
		GeneratePassword('task_password');
	});

	$("#task-header").click(function()
	{
		$("#task-details").show();
		$("#task-sub-total").hide();
		$("#task-workers").hide();
	});

	$("#task-sub-header").click(function()
	{
		$("#task-details").hide();
		$("#task-sub-total").show();
		$("#task-workers").hide();
		task.ViewTaskCost();
	});

	$("#task-worker-subheader").click(function()
	{
		$("#task-details").hide();
		$("#task-sub-total").hide();
		$("#task-workers").show();
		task.WorkerListView();
	});

	$("#done-btns").click(function()
	{
		task.Done();
	});

	$("#start-btns").click(function()
	{
		task.Start();
	});

	$("#add-task-worker").click(function()
	{
		task.AddWorker();
	});

	$("#manpower_id").change(function()
	{
		type = $("#manpower_id").val();
		//alert(type);
	});

	$("#task_worker_name").autocomplete({
        source: function (request, response) {
			
            $.post("../worker/KeyValueListWorkerKey", {
                'keyword': request.term,
                'type' : type
            },
            function (data) {
            		//alert(data);
                      var result = eval("("+data+")");
                      response($.map(result.data, function (item) {
                          return {
                              label: item.value,
                              value: item.value,
                              id: item.key
                          }
                      }));
                  });
        },
        minLength: 0,
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function (event, ui) {
			  $("#task_worker_name").val(ui.item.value);
			  $("#worker_id").val(ui.item.id);
			  return false;
        }
    }).keypress(function (event, ui) {
		//$("#supplier_name").val("");
    }).focus(function (){
		  //$(this).data("autocomplete").search($(this).val());
    });

    // Gantt CHART

    //task.ViewGanttChart();

    $(".gantt").gantt({
				source: [{
					name: "Sprint 0",
					desc: "Analysis",
					values: [{
						from: "/Date(1320192000000)/",
						to: "/Date(1322401600000)/",
						label: "Requirement Gathering", 
						customClass: "ganttRed"
					}]
				},{
					name: " ",
					desc: "Scoping",
					values: [{
						from: "/Date(1322611200000)/",
						to: "/Date(1323302400000)/",
						label: "Scoping", 
						customClass: "ganttRed"
					}]
				},{
					name: "Sprint 1",
					desc: "Development",
					values: [{
						from: "/Date(1323802400000)/",
						to: "/Date(1325685200000)/",
						label: "Development", 
						customClass: "ganttGreen"
					}]
				},{
					name: " ",
					desc: "Showcasing",
					values: [{
						from: "/Date(1325685200000)/",
						to: "/Date(1325695200000)/",
						label: "Showcasing", 
						customClass: "ganttBlue"
					}]
				},{
					name: "Sprint 2",
					desc: "Development",
					values: [{
						from: "/Date(1326785200000)/",
						to: "/Date(1325785200000)/",
						label: "Development", 
						customClass: "ganttGreen"
					}]
				},{
					name: " ",
					desc: "Showcasing",
					values: [{
						from: "/Date(1328785200000)/",
						to: "/Date(1328905200000)/",
						label: "Showcasing", 
						customClass: "ganttBlue"
					}]
				},{
					name: "Release Stage",
					desc: "Training",
					values: [{
						from: "/Date(1330011200000)/",
						to: "/Date(1336611200000)/",
						label: "Training", 
						customClass: "ganttOrange"
					}]
				},{
					name: " ",
					desc: "Deployment",
					values: [{
						from: "/Date(1336611200000)/",
						to: "/Date(1338711200000)/",
						label: "Deployment", 
						customClass: "ganttOrange"
					}]
				},{
					name: " ",
					desc: "Warranty Period",
					values: [{
						from: "/Date(1336611200000)/",
						to: "/Date(1349711200000)/",
						label: "Warranty Period", 
						customClass: "ganttOrange"
					}]
				}],
				navigate: "scroll",
				scale: "weeks",
				maxScale: "months",
				minScale: "days",
				itemsPerPage: 10,
				onItemClick: function(data) {
					alert("Item clicked - show some details");
				},
				onAddClick: function(dt, rowId) {
					alert("Empty space clicked - add an item!");
				},
				onRender: function() {
					if (window.console && typeof console.log === "function") {
						console.log("chart rendered");
					}
				}
			});


});

function Initialize(id)
{
	$("#task-header").text('Task Details');
	$("#task-sub-header").hide();
	$("#task-details").show();
	task.DisableReadonly();
	task.Initialize(id);
	GetPopulateDropdown("../manpower/ListByTask", "manpower_id", {id : id}, function(data){});

	$("#task-sub-total").hide();
}

function Remove(id)
{
	task.Remove(id);
}

function RemoveTaskWorker(id, user_id)
{
	task.RemoveTaskWorker(id, user_id);
}

function View(id)
{
	$("#task-header").text('Task Details');
	$("#task-header").show();
	$("#task-sub-header").show();
	$("#task-sub-header").text('Total Cost');
	$("#task-details").show();
	task.EnableReadonly();
	task.Initialize(id);
	$("#task-workers").hide();
	$("#task-sub-total").hide();

}

function PrintTaskList()
{
	var id = $("#project_id").val();
	window.open(window.location.origin + '/print/print_task_list.php?id='+id , '_blank');
}