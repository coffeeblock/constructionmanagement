var manpower;
$(function()
{
	manpower = new System.Manpower();

	manpower.ListView();
	manpower.Fields();

	//Add manpower modal
	$("#add-manpower-btn").click(function()
	{	
		manpower.DisableReadonly();
		manpower.Clear();
		openModal('manpower-modal');
	});

	//Add worker
	$("#add-worker-btn").click(function()
	{
		openModal('worker-modal');

	});

		//Save Info
	$("#manpower-btns").click(function()
	{
		manpower.Save();
	});

});

function Initialize(id, status)
{
	manpower.DisableReadonly();
	manpower.Initialize(id, status);
}

function Remove(id)
{
	manpower.Remove(id);
}

function PrintManpowerList()
{
	window.open(window.location.origin + '/print/print_manpower_list.php', '_blank');
}
